#include "EditorInputInterpretor.h"
#include "Editor.h"
#include "EditorHUD.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "Cursor.h"
#include <fstream>

//For Keyboard
void EditorInputInterpretor::interpretInput(std::string input)
{
	Editor* pEditor = dynamic_cast<Editor*>(gpGame);
	bool updateHUD = false;

	//Alphabet
	if (input == "L")
	{
		if (pEditor != NULL)
		{
			pEditor->loadEnviornment(pEditor->getFileName());
			pEditor->getCursor()->setCurGridSize(pEditor->getMap()->getGrid()->getSquareSize());
			std::cout << "Map(s) loaded!\n";
		}
	}

	else if (input == "S")
	{
		if (pEditor != NULL)
		{
			pEditor->saveMaps();
			std::cout << "Map(s) saved!\n";
		}
	}

	else if (input == "H")
	{
		pEditor->getHUD()->toggleHelp();
	}

	//Alternative
	else if (input == "ESCAPE")
	{
		gpGame->markForExit();
	}

	else if (input == "KEY62") //PLUS
	{
		pEditor->switchNextMap();
	}

	else if (input == "KEY61") //MINUS
	{
		pEditor->switchPrevMap();
	}

	else if (input == "F1")
	{
		pEditor->getHUD()->toggleHelp();
	}

	else if (input == "DOWN")
	{
		updateHUD = true;
		if (mIndex == 0)
		{
			mIndex = NUM_SPAWN_TYPES;
		}

		else
		{
			mIndex--;
		}
		std::cout << mIndex << std::endl;
	}

	else if (input == "UP")
	{
		updateHUD = true;
		if (mIndex == NUM_SPAWN_TYPES)
		{
			mIndex = 0;
		}

		else
		{
			mIndex++;
		}
		std::cout << mIndex << std::endl;
	}

	else if (input == "LEFT")
	{
		if (pEditor->getMap()->getPelletPopulationVal() > 0)
		{
			pEditor->getMap()->setPelletPopulationVal(pEditor->getMap()->getPelletPopulationVal() - 10);
			pEditor->getMap()->regeneratePellets();
		}
	}

	else if (input == "RIGHT")
	{
		if (pEditor->getMap()->getPelletPopulationVal() < 100)
		{
			pEditor->getMap()->setPelletPopulationVal(pEditor->getMap()->getPelletPopulationVal() + 10);
			pEditor->getMap()->regeneratePellets();
		}
	}
	if (updateHUD)
	{
		std::string curItem = "";
		Sprite* curSprite = NULL;
		switch (mIndex)
		{
		case 0: 
			curItem = "Wall Solid";
			curSprite = gpGame->getSpriteManager()->getSprite(WALL_SPRITE_ID);
			break;
		case 1:
			curItem = "Portal / Doorway";
			curSprite = gpGame->getSpriteManager()->getSprite(PORTAL_SPRITE_ID);
			break;
		case 2:
			curItem = "Power Pellet";
			curSprite = gpGame->getSpriteManager()->getSprite(POWER_PELLET_SPRITE_ID);
			break;
		case 3:
			curItem = "Grue Spawn";
			curSprite = gpGame->getSpriteManager()->getSprite(GRUE_SPAWN_SPRITE_ID);
			break;
		case 4:
			curItem = "Player Spawn";
			curSprite = gpGame->getSpriteManager()->getSprite(PLAYER_SPAWN_SPRITE_ID);
			break;
		default:
			curItem = "Error!";
			curSprite = NULL;
			break;
		}
		pEditor->getHUD()->setCurItem(curItem);
		pEditor->getHUD()->setCurItemSprite(curSprite);
	}
}

//For Mouse
void EditorInputInterpretor::interpretInput(Vector2D mousePos, MouseClick click)
{
	Editor* pEditor = static_cast<Editor*>(gpGame);
	Grid* pGrid = static_cast<Editor*>(gpGame)->getMap()->getGrid();

	Vector2D transposedMousePos = transposeMousePos(mousePos);
	mMousePos = transposedMousePos;
	pEditor->getCursor()->setTilePos(Vector2D(int(mMousePos.getX() / pGrid->getSquareSize()), int(mMousePos.getY() / pGrid->getSquareSize())));

	if (click == LEFT_DOWN)
	{
		if (!mPortalClickedState)
		{
			if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) == CLEAR_VALUE)
			{
				int tileId = pGrid->getSquareIndexFromPixelXY(transposedMousePos.getX(), transposedMousePos.getY());
				switch (mIndex)
				{
					// ADD A TILE
				case TILE:
				{
					pEditor->addBlock(transposedMousePos);
					if (pGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
					{
						pEditor->getMap()->addWallItem(new Wall(WALL_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}
					else
					{
						pEditor->getMap()->addWallItem(new Wall(WALL_SMALL_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}
					break;
				}

					// ADD A PORTAL
				case PORTAL:
				{
					Portal* p = inquireNewPortalData(transposedMousePos);
					if (p != NULL)
						pEditor->addPortal(p);
					break;
				}

					// ADD A POWER PELLET
				case POWER_PELLET:
				{
					pEditor->addPowerPellet(transposedMousePos);
					if (pGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
					{
						pEditor->getMap()->addPowerPelletItem(new PowerPellet(POWER_PELLET_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}

					else
					{
						pEditor->getMap()->addPowerPelletItem(new PowerPellet(POWER_PELLET_SMALL_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}
					break;
				}

					// ADD A GRUE SPAWN
				case GRUE_SPAWN:
				{
					pEditor->addGrueSpawn(transposedMousePos);
					if (pGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
					{
						pEditor->getMap()->addGrueSpawn(new GrueSpawn(GRUE_SPAWN_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}

					else
					{

						pEditor->getMap()->addGrueSpawn(new GrueSpawn(GRUE_SPAWN_SMALL_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
					}
					break;
				}

					// ADD A PLAYER SPAWN
				case PLAYER_SPAWN:
				{
					pEditor->addPlayerSpawn(transposedMousePos);
					if (pEditor->getMap()->getNumPlayerSpawns() == 0)
					{
						if (pGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
						{
							pEditor->getMap()->addPlayerSpawn(new PlayerSpawn(PLAYER_SPAWN_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
						}

						else
						{

							pEditor->getMap()->addPlayerSpawn(new PlayerSpawn(PLAYER_SPAWN_SMALL_SPRITE_ID, tileId, pGrid->getOriginOfSquare(tileId), pEditor->getMap()->getMapName()), tileId);
						}
					}
					break;
				}
				default:
					break;
				}
			}

			else if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) >= PORTAL_VALUE 
				&& pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) <= PORTAL_DISC)
			{
				pEditor->handlePortalClick(transposedMousePos);
				mPortalClickedState = true;
			}
		}
	}

	else if (click == LEFT_UP && mPortalClickedState)
		mPortalClickedState = false;

	else if (click == RIGHT_DOWN)
	{
		if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) != CLEAR_VALUE)
			pEditor->clearBlock(transposedMousePos);
	}

	else if (click == NO_CLICK)
	{
		pEditor->getMap()->setSelectedTileIndex(
			pGrid->getSquareIndexFromPixelXY(mMousePos.getX(),
			mMousePos.getY()));
	}
}

//The backbone for creating new portals along with new maps
Portal* EditorInputInterpretor::inquireNewPortalData(Vector2D pos)
{
	std::string name;
	std::string linkerID;
	Editor* pEditor = static_cast<Editor*>(gpGame);
	Map* mMap = static_cast<Editor*>(gpGame)->getMap();
	Grid* mGrid = static_cast<Editor*>(gpGame)->getMap()->getGrid();
	int tileIndex = mGrid->getSquareIndexFromPixelXY(pos.getX(), pos.getY());

	//Display header
	clearScreen();
	std::cout << "Creating a new Portal at (id = " + std::to_string(tileIndex) + ")" << std::endl;
	std::cout << "->Type \"-c\" at any time to cancel the process." << std::endl;
	std::cout << "------------------------------------------------" << std::endl;

	//Ask for map to link to
	std::cout << "Linked Map FILE (eg. dungeon.txt): ";
	std::getline (std::cin, name);
	space2underscore(name); //get rid of all spaces and replace them with underscores

	//Cancels the operation
	if (name == "-c")
	{
		clearScreen();
		std::cout << "Returning to editor" << std::endl;
		pEditor->clearBlock(pos);
		return NULL;
	}

	//Ask for the teleporters ID
	std::cout << "Teleporter ID for this map Connection:";
	std::getline(std::cin, linkerID);
	space2underscore(linkerID); //get rid of all spaces and replace them with underscores

	//Cancels the operation
	if (linkerID == "-c")
	{
		clearScreen();
		std::cout << "Returning to editor" << std::endl;
		pEditor->clearBlock(pos);
		return NULL;
	}

	clearScreen();

	//Create a new portal with the specified data
	Portal* portal = NULL;
	if (mGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
	{
		portal = new Portal(mMap->getMapName(), name, linkerID, tileIndex, PORTAL_SPRITE_ID, mGrid->getULCornerOfSquare(tileIndex) + Vector2D(mGrid->getSquareSize() / 2, mGrid->getSquareSize() / 2));
	}

	else
	{
		portal = new Portal(mMap->getMapName(), name, linkerID, tileIndex, PORTAL_SMALL_SPRITE_ID, mGrid->getULCornerOfSquare(tileIndex) + Vector2D(mGrid->getSquareSize() / 2, mGrid->getSquareSize() / 2));
	}

	//Find number of portals with the same teleporter ID
	if (mMap->checkForIdenticalPortals(portal) >= 1) //if there is more than one portal with the same ID, cancel the operation
	{
		if (mMap->checkNumMapPortalConnections(portal) >= 2)
		{
			clearScreen();
			std::cout << "Portal of this type already exists!" << std::endl;
			std::cout << "Returning to editor" << std::endl;
			pEditor->clearBlock(pos);
			delete portal;
			return NULL;
		}
		clearScreen();
		std::cout << "Portal of this type already exists!" << std::endl;
		std::cout << "Returning to editor" << std::endl;
		pEditor->clearBlock(pos);
		delete portal;
		return NULL;
	}
	return portal;
}

std::string EditorInputInterpretor::space2underscore(std::string text)
{
	for (std::string::iterator it = text.begin(); it != text.end(); ++it) {
		if (*it == ' ') {
			*it = '_';
		}
	}
	return text;
}