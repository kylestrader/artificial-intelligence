#pragma once

#include "InputInterpretor.h"

enum AddType
{
	TILE = 0,
	PORTAL = 1,
	POWER_PELLET = 2,
	GRUE_SPAWN = 3,
	PLAYER_SPAWN = 4
};

const int NUM_SPAWN_TYPES = 4;

class EditorInputInterpretor :public InputInterpretor
{
public:
	//constructor/destructor
	EditorInputInterpretor(): mIndex(0){};
	~EditorInputInterpretor(){};

	//virtualized functions
	void interpretInput(std::string inputChar);
	void interpretInput(Vector2D mousePos, MouseClick click);

private:
	Portal* inquireNewPortalData(Vector2D pos);
	inline void clearScreen() { system("cls"); };
	std::string space2underscore(std::string text);

	Vector2D mMousePos;
	bool mPortalClickedState;
	AddType mSelected;
	int mIndex;
};