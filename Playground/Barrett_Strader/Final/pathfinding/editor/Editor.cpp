#include <fstream>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro.h>

#include "Editor.h"
#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "GraphicsBufferManager.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Vector2D.h"
#include "InputManager.h"
#include "Cursor.h"
#include "EditorHUD.h"
#include "EditorInputInterpretor.h"

using namespace std;

Editor::Editor()
:Game()
, mpCurMap(NULL)
, mpHUD(NULL)
, mpCursor(NULL)
,mCurMapIndex(0)
{
}

Editor::~Editor()
{
	cleanup();
}

bool Editor::init()
{
	bool retVal = Game::init();
	if( retVal == false )
		return false;

	//Setting up maps vector
	mpCurMap = new Map();
	mpCurMap->initialize(mpGraphicsSystem->getHeight(), mpGraphicsSystem->getHeight(), GRID_SQUARE_SIZE_OVERWORLD, OVERWORLD_NAME);
	mMaps[mpCurMap->getMapName()] = mpCurMap;

	//setup overlay
	mpHUD = new EditorHUD(mpGraphicsSystem->getHeight(), 0, 256, mpGraphicsSystem->getHeight());
	mpCursor = new Cursor(Vector2D(-1, -1), GRID_SQUARE_SIZE_OVERWORLD);

	//setup interpretor
	mpInputInterpretor = new EditorInputInterpretor();
	mpMasterTimer->start();

	return true;
}

void Editor::cleanup()
{
	delete mpInputInterpretor;
	mpInputInterpretor = NULL;

	//delete hud elements
	delete mpCursor;
	mpCursor = NULL;

	delete mpHUD;
	mpHUD = NULL;

	cleanupMaps();
}

void Editor::cleanupMaps()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end();)
	{
		delete iter->second;
		iter = mMaps.erase(iter);
	}
}

void Editor::beginLoop()
{
	Game::beginLoop();
	al_draw_filled_rectangle(0, 0, 0, 0, al_map_rgba(0.3f, 0.3f, 0.3f, 1.0f));
}

void Editor::processLoop()
{
	Game::processLoop();
	drawEditor();
	mpHUD->draw(*(mpGraphicsSystem->getBackBuffer()));
	mpCursor->draw(*(mpGraphicsSystem->getBackBuffer()));
}

bool Editor::endLoop()
{
	return Game::endLoop();
}

//ONLY TO BE CALLED IN PROCESS LOOP
void Editor::drawEditor()
{
	//copy to back buffer
	mpCurMap->draw(*(mpGraphicsSystem->getBackBuffer()));
}

void Editor::addBlock(Vector2D pos)
{
	mpCurMap->getGrid()->setValueAtPixelXY(pos.getX(), pos.getY(), BLOCKING_VALUE);
	mpCurMap->getGridVisualizer()->setModified();
}

void Editor::addPowerPellet(Vector2D pos)
{
	mpCurMap->getGrid()->setValueAtPixelXY(pos.getX(), pos.getY(), POWER_PELLET_VALUE);
	mpCurMap->getGridVisualizer()->setModified();
}

void Editor::addGrueSpawn(Vector2D pos)
{
	mpCurMap->getGrid()->setValueAtPixelXY(pos.getX(), pos.getY(), GRUE_SPAWN_VALUE);
	mpCurMap->getGridVisualizer()->setModified();
}

void Editor::addPlayerSpawn(Vector2D pos)
{
	mpCurMap->getGrid()->setValueAtPixelXY(pos.getX(), pos.getY(), PLAYER_SPAWN_VALUE);
	mpCurMap->getGridVisualizer()->setModified();
}

void Editor::clearBlock(Vector2D pos)
{
	if (mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) >= PORTAL_VALUE 
		&& mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) <= PORTAL_DISC)
	{
		mpCurMap->removePortal(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY()));
		refreshPortals();
	}

	if (mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) == BLOCKING_VALUE)
		mpCurMap->removeWallItem(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY()));

	if (mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) == POWER_PELLET_VALUE)
		mpCurMap->removePowerPelletItem(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY()));

	if (mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) == GRUE_SPAWN_VALUE)
		mpCurMap->removeGrueSpawn(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY()));

	if (mpCurMap->getGrid()->getValueAtIndex(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY())) == PLAYER_SPAWN_VALUE)
		mpCurMap->removePlayerSpawn(mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY()));

	mpCurMap->getGrid()->setValueAtPixelXY(pos.getX(), pos.getY(), CLEAR_VALUE);
	mpCurMap->getGridVisualizer()->setModified();
}

void Editor::addPortal(Portal* portal)
{
	Grid* mGrid = mpCurMap->getGrid();

	mpCurMap->addPortal(portal);
	mGrid->setValueAtIndex(portal->getTileID(), PORTAL_VALUE);
	mpCurMap->getGridVisualizer()->setModified();	
	
	if (!doesMapExist(portal->getToMapName()))
		addMap(synthesizeNewMapData(portal->getToMapName()));

	refreshPortals();
}

void Editor::saveMaps()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		std::ofstream theStream(std::string(MAPS_DIRECTORY + iter->second->getMapName()).c_str());
		iter->second->save(theStream);
		theStream.close();
	}

	refreshPortals();
}

void Editor::loadEnviornment(std::string name)
{
	cleanupMaps();
	loadMap(name);

	if (mMaps.size() > 0)
		switchMap(0);
	else
	{
		mpCurMap = new Map();
		mpCurMap->initialize(gpGame->getGraphicsSystem()->getHeight(), gpGame->getGraphicsSystem()->getHeight(), GRID_SQUARE_SIZE_OVERWORLD, OVERWORLD_NAME);
		mMaps[mpCurMap->getMapName()] = mpCurMap;
		mCurMapIndex = 0;
	}
	mpCurMap->getGridVisualizer()->setModified();

	refreshPortals();
}

//recursive function that loads a map and then proceeds to load all maps that it is connected to (portal connections)
void Editor::loadMap(std::string name)
{
	std::vector<std::string> maps2Load;
	std::ifstream theStream(std::string(MAPS_DIRECTORY + name).c_str());
	if (theStream.is_open())
	{
		Map* tmpMap = new Map();

		int gridSize = GRID_SQUARE_SIZE_DUNGEON;

		if (name == OVERWORLD_NAME)
			gridSize = GRID_SQUARE_SIZE_OVERWORLD;

		//setup templated map to load stream into
		tmpMap->initialize(gpGame->getGraphicsSystem()->getHeight(), gpGame->getGraphicsSystem()->getHeight(), gridSize, name);

		if (theStream.is_open() && !doesMapExist(tmpMap))
		{
			maps2Load = tmpMap->load(theStream, MAPS_DIRECTORY); //stores a list of the maps that need to be loaded (derived from the connections tmpMap has with other maps)
			mMaps[tmpMap->getMapName()] = tmpMap;

			for each (std::string s in maps2Load) //load in all maps in the list of maps to load
			{
				loadMap(s);
			}
		}
		else
			delete tmpMap;
	}
	theStream.close();
}

bool Editor::doesMapExist(std::string name)
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		if (iter->second->getMapName() == name)
			return true;
	}
	return false;
}

//Does not check to see if map adresses are the same (runs name comparison)
bool Editor::doesMapExist(Map* _map)
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		if (iter->second->getMapName() == _map->getMapName())
			return true;
	}
	return false;
}

void Editor::switchNextMap()
{
	mCurMapIndex = (mCurMapIndex + 1) % mMaps.size();
	switchMap(mCurMapIndex);
}

void Editor::switchPrevMap()
{
	if (mCurMapIndex == 0)
		mCurMapIndex = mMaps.size() - 1;
	else
		mCurMapIndex--;

	switchMap(mCurMapIndex);
}

void Editor::addMap(Map* map)
{
	mMaps[map->getMapName()] = map;
	switchMap(map->getMapName());
	refreshPortals();
}

int Editor::switchMap(std::string name)
{
	int index = 0;
	for (std::map<std::string, Map*>::iterator it = mMaps.begin(); it != mMaps.end();)
	{
		if (it->second->getMapName() == name)
		{
			mpCurMap = it->second;
			mCurMapIndex = index;
			mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());
			return 0;
		}
		else
			++it;

		index++;
	}
	system("cls");
	std::cout << "ERROR: Could not find map \"" + name + "\" in the working directory!" << std::endl;
	std::cout << endl;
	std::cout << "Would you like to re-create this map? (y/n): ";
	std::string ans;
	std::getline(std::cin, ans);
	if (ans == "y")
	{
		system("cls");
		std::cout << "Generating map..." << std::endl;
		addMap(synthesizeNewMapData(name));
		system("cls");
	}

	else
	{
		system("cls");
		std::cout << "Returning to editor" << std::endl;
	}

	mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());
	return -1;
}

int Editor::switchMap(int index)
{
	int size = mMaps.size();
	if (index < size)
	{
		std::map<std::string, Map*>::iterator iter;
		iter = mMaps.begin();

		int compIndex = 0;
		for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
		{
			if (compIndex == index)
				break;
			else
				compIndex++;
		}
		mpCurMap = iter->second;
		mCurMapIndex = index;
		mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());
		return 0;
	}
	return -1;
}

void Editor::handlePortalClick(Vector2D pos)
{
	int id = mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY());
	if (id > 0)
	{
		Portal* p = mpCurMap->getPortal(id);
		switchMap(p->getToMapName());
	}
}

Map* Editor::synthesizeNewMapData(std::string name)
{
	Map* tmpMap = new Map();
	tmpMap->initialize(mpGraphicsSystem->getHeight(), mpGraphicsSystem->getHeight(), GRID_SQUARE_SIZE_DUNGEON, name);
	return tmpMap;
}

void Editor::refreshPortals()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		iter->second->refreshPortalConnections(mMaps);
	}
}
