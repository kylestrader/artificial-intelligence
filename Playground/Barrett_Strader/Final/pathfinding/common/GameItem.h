#pragma once

#include "Game.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Vector2D.h"

class GameItem: public Trackable
{
public:
	GameItem(IDType type, int tileId, Vector2D origin, std::string mapName)
		:mSpriteID(type)
		, mTileID(tileId)
		, mOrigin(origin)
		, mMapName(mapName){
		mpSprite = gpGame->getSpriteManager()->getSprite(type);
	};

	virtual ~GameItem(){};

	virtual void draw(GraphicsBuffer* pBuffer) = 0;
	virtual void update(float time) = 0;

	inline int getTileId() { return mTileID; };
	inline std::string getMapName(){ return mMapName; };



protected:
	Sprite* mpSprite;
	IDType mSpriteID;
	int mTileID;
	Vector2D mOrigin;
	std::string mMapName;
};

