#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro.h>

#include "HUD.h"
#include "Game.h"
#include "GraphicsSystem.h"
#include "SpriteManager.h"

HUD::HUD(int x, int y, int w, int h)
{
	mX_Pos = x;
	mY_Pos = y;
	mWidth = w;
	mHeight = h;
	mpFont = al_load_ttf_font(std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 14, ALLEGRO_ALIGN_CENTER);
	mpFontUpper = al_load_ttf_font(std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 28, ALLEGRO_ALIGN_CENTER);
	mHelp = true;
	mCurItem = "Wall Block";
	mCurItemSprite = gpGame->getSpriteManager()->getSprite(WALL_SPRITE_ID);
}

HUD::~HUD()
{
	al_destroy_font(mpFont);
	al_destroy_font(mpFontUpper);
}

void HUD::draw(GraphicsBuffer& gb)
{

	ALLEGRO_BITMAP* pOldTarget = GraphicsSystem::switchTargetBitmap(gb.getBitmap());

	al_draw_filled_rectangle(mX_Pos, mY_Pos, mX_Pos + mWidth, mY_Pos + mHeight, al_map_rgba(0.3f, 0.3f, 0.3f, 0.01f));

	if (mHelp)
	{
		al_draw_filled_rectangle(mX_Pos + 10, mY_Pos + 120, mX_Pos + mWidth - 10, mY_Pos + mHeight - 10, al_map_rgba(0, 0, 0, 25));
		al_draw_text(mpFontUpper, al_map_rgb(255, 75, 75), mX_Pos + 30 + mWidth / 2, 310, ALLEGRO_ALIGN_CENTER, std::string("Controls:").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 360, ALLEGRO_ALIGN_LEFT, std::string("L: load dungeons").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 380, ALLEGRO_ALIGN_LEFT, std::string("S: Save dungeons").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 400, ALLEGRO_ALIGN_LEFT, std::string("ESC: Quit").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 420, ALLEGRO_ALIGN_LEFT, std::string("+: View next map").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 440, ALLEGRO_ALIGN_LEFT, std::string("-: View previous map").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 460, ALLEGRO_ALIGN_LEFT, std::string("LMB: Place wall/traverse").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 480, ALLEGRO_ALIGN_LEFT, std::string("MMB: Place portal").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 500, ALLEGRO_ALIGN_LEFT, std::string("RMB: Remove wall/portal").c_str());
	}

		al_draw_text(mpFontUpper, al_map_rgb(255, 75, 75), mX_Pos + 30 + mWidth / 2, 30, ALLEGRO_ALIGN_CENTER, std::string("Tools:").c_str());
		al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, 80, ALLEGRO_ALIGN_LEFT, std::string("Current Selected Item: " + mCurItem).c_str());
		mCurItemSprite->draw(gb, mX_Pos + 70, 160, 0.0f);
		

	GraphicsSystem::switchTargetBitmap(pOldTarget);
}