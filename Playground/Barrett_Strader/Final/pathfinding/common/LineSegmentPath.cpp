#include "LineSegmentPath.h"

void LineSegmentPath::draw()
{
	int size = mPath.size();
	for (int i = 0; i < size; i++)
	{
		LineSegment segment = mPath.at(i);
		segment.draw();

		if (mAccessorize)
		{
			Vector2D normToTip = segment.getToPoint() - segment.getFromPoint();
			normToTip.normalize();

			Vector2D p1 = segment.getFromPoint() + normToTip * ACCESSORY_SIZE / 2;
			Vector2D rotatedNorm = Vector2D(normToTip.getY() * -1, normToTip.getX());
			Vector2D p2 = segment.getFromPoint() + ((normToTip * ACCESSORY_SIZE / 2) * -1) + (rotatedNorm * ACCESSORY_SIZE / 2);
			rotatedNorm *= -1;
			Vector2D p3 = segment.getFromPoint() + ((normToTip * ACCESSORY_SIZE / 2) * -1) + (rotatedNorm * ACCESSORY_SIZE / 2);

			al_draw_filled_triangle(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), al_map_rgb(255, 255, 255));

			if (i == mPath.size() - 1)
			{
				al_draw_filled_circle(segment.getToPoint().getX(), segment.getToPoint().getY(), ACCESSORY_SIZE / 2, al_map_rgb(255, 255, 255));
			}
		}
	}
}

void LineSegmentPath::toggleAccessorize(bool acc)
{
	mAccessorize = acc;
}
