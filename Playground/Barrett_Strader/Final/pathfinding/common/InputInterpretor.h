#pragma once

#include "Game.h"
#include "Vector2D.h"
#include "Portal.h"
#include <string>
#include <Map.h>

enum MouseClick
{
	NO_CLICK = 0,
	LEFT_DOWN = 1,
	RIGHT_DOWN = 2,
	MIDDLE_DOWN = 3,
	LEFT_UP = 4,
	RIGHT_UP = 5,
	MIDDLE_UP = 6
};

class InputInterpretor :public Trackable
{
public:
	InputInterpretor();
	virtual ~InputInterpretor(){};

	//virtualized functions
	virtual void interpretInput(std::string inputChar) = 0;
	virtual void interpretInput(Vector2D mousePos, MouseClick click) = 0;

protected:
	Vector2D transposeMousePos(Vector2D pos);

	Vector2D mMousePos;
	bool mPortalClickedState;
};