#include "SendKeyActionMessage.h"
#include "InputInterpretor.h"
#include "Game.h"

void SendKeyActionMessage::process()
{
	gpGame->getInputInterpretor()->interpretInput(mAction);
}