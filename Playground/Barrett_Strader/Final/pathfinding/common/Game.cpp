#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <PerformanceTracker.h>

#include <sstream>

#include "Game.h"
#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "GraphicsBufferManager.h"
#include "GameMessageManager.h"
#include "InputManager.h"
#include "InputInterpretor.h"
#include "Cursor.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Timer.h"
#include "Defines.h"

Game* gpGame = NULL;

Game::Game()
	:mpGraphicsSystem(NULL)
	,mpGraphicsBufferManager(NULL)
	,mpMessageManager(NULL)
	,mpInputManager(NULL)
	,mpInputInterpretor(NULL)
	,mpSpriteManager(NULL)
	,mpLoopTimer(NULL)
	,mpMasterTimer(NULL)
	,mShouldExit(false)
	,mLoopTargetTime(LOOP_TARGET_TIME)
#ifdef _DEBUG
	,debugMode(true)
#else
	,debugMode(false)
#endif
	,drawDebugText(true)
{
}

Game::~Game()
{
	cleanup();
}

bool Game::init()
{
	srand(time(0));//seed random number generator

	mShouldExit = false;

	//create Timers
	mpLoopTimer = new Timer;
	mpMasterTimer = new Timer;

	//startup allegro
	if(!al_init()) 
	{
		fprintf(stderr, "failed to initialize allegro!\n");
		return false;
	}

	//initialize allegro stuff
	if (!initializeAllegro())
	{
		return false;
	}

	//create and init GraphicsSystem
	mpGraphicsSystem = new GraphicsSystem();
	bool goodGraphics = mpGraphicsSystem->init(SCREEN_WIDTH, SCREEN_HEIGHT);
	if(!goodGraphics) 
	{
		fprintf(stderr, "failed to initialize GraphicsSystem object!\n");
		return false;
	}


	mpGraphicsBufferManager = new GraphicsBufferManager();
	mpSpriteManager = new SpriteManager();
	mpMessageManager = new GameMessageManager();
	mpInputManager = new InputManager();
	mpInputManager->initialize();

	//load buffers
	mpGraphicsBufferManager->loadBuffer(BACKGROUND_ID, IMAGES_DIRECTORY + "wallpaper.bmp");
	mpGraphicsBufferManager->loadBuffer(WALL_ID, IMAGES_DIRECTORY + "wall.bmp");
	mpGraphicsBufferManager->loadBuffer(WALL_SMALL_ID, IMAGES_DIRECTORY + "wall_small.bmp");
	mpGraphicsBufferManager->loadBuffer(PLAYER_BG_ID, IMAGES_DIRECTORY + "player.png");
	mpGraphicsBufferManager->loadBuffer(PLAYER_SMALL_BG_ID, IMAGES_DIRECTORY + "player_small.png");
	mpGraphicsBufferManager->loadBuffer(ENEMY_BG_ID, IMAGES_DIRECTORY + "minion.png");
	mpGraphicsBufferManager->loadBuffer(ENEMY_SMALL_BG_ID, IMAGES_DIRECTORY + "minion_small.png");
	mpGraphicsBufferManager->loadBuffer(PELLET_BG_ID, IMAGES_DIRECTORY + "pellet.png");
	mpGraphicsBufferManager->loadBuffer(PELLET_SMALL_BG_ID, IMAGES_DIRECTORY + "pellet_small.png");
	mpGraphicsBufferManager->loadBuffer(POWER_PELLET_BG_ID, IMAGES_DIRECTORY + "power_pellet.png");
	mpGraphicsBufferManager->loadBuffer(POWER_PELLET_SMALL_BG_ID, IMAGES_DIRECTORY + "power_pellet_small.png");
	mpGraphicsBufferManager->loadBuffer(PORTAL_BG_ID, IMAGES_DIRECTORY + "portal.png");
	mpGraphicsBufferManager->loadBuffer(PORTAL_SMALL_BG_ID, IMAGES_DIRECTORY + "portal_small.png");
	mpGraphicsBufferManager->loadBuffer(GRUE_SPAWN_BG_ID, IMAGES_DIRECTORY + "grue_spawn.png");
	mpGraphicsBufferManager->loadBuffer(GRUE_SPAWN_SMALL_BG_ID, IMAGES_DIRECTORY + "grue_spawn_small.png");
	mpGraphicsBufferManager->loadBuffer(PLAYER_SPAWN_BG_ID, IMAGES_DIRECTORY + "player_spawn.png");
	mpGraphicsBufferManager->loadBuffer(PLAYER_SPAWN_SMALL_BG_ID, IMAGES_DIRECTORY + "player_spawn_small.png");
	mpGraphicsBufferManager->loadBuffer(PLAYER_POWERPELLET_BG_ID, IMAGES_DIRECTORY + "Player_powerup.png");
	mpGraphicsBufferManager->loadBuffer(PLAYER_POWERPELLET_SMALL_BG_ID, IMAGES_DIRECTORY + "Player_powerup_small.png");

	mpGraphicsBufferManager->loadBuffer(MINION_SEEK_BG_ID, IMAGES_DIRECTORY + "Minion_seek.png");
	mpGraphicsBufferManager->loadBuffer(MINION_SEEK_SMALL_BG_ID, IMAGES_DIRECTORY + "Minion_seek_small.png");
	mpGraphicsBufferManager->loadBuffer(MINION_FLEE_BG_ID, IMAGES_DIRECTORY + "Minion_flee.png");
	mpGraphicsBufferManager->loadBuffer(MINION_FLEE_SMALL_BG_ID, IMAGES_DIRECTORY + "Minion_flee_small.png");
	mpGraphicsBufferManager->loadBuffer(MINION_INVESTIGATE_BG_ID, IMAGES_DIRECTORY + "Minion_investigate.png");
	mpGraphicsBufferManager->loadBuffer(MINION_INVESTIGATE_SMALL_BG_ID, IMAGES_DIRECTORY + "Minion_investigate_small.png");


#pragma region load

	//setup sprites
	//background
	GraphicsBuffer* pBackGroundBuffer = mpGraphicsBufferManager->getBuffer(BACKGROUND_ID);
	if (pBackGroundBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(
			BACKGROUND_SPRITE_ID
			, pBackGroundBuffer
			, 0
			, 0
			, pBackGroundBuffer->getWidth()
			, pBackGroundBuffer->getHeight());
	}


	auto initSprite = [this](const IDType BG, const IDType SPRITE) {
		GraphicsBuffer* buffer = mpGraphicsBufferManager->getBuffer(BG);
		if (buffer != NULL)
		{
			mpSpriteManager->createAndManageSprite(SPRITE, buffer, 0, 0, buffer->getWidth(), buffer->getHeight());
		}
	};

	initSprite(WALL_ID, WALL_SPRITE_ID);
	initSprite(WALL_SMALL_ID, WALL_SMALL_SPRITE_ID);
	initSprite(PLAYER_BG_ID, PLAYER_SPRITE_ID);
	initSprite(PLAYER_POWERPELLET_SMALL_BG_ID, PLAYER_POWERPELLET_SMALL_SPRITE_ID);

	initSprite(MINION_SEEK_BG_ID, MINION_SEEK_SPRITE_ID);
	initSprite(MINION_SEEK_SMALL_BG_ID, MINION_SEEK_SMALL_SPRITE_ID);
	initSprite(MINION_FLEE_BG_ID, MINION_FLEE_SPRITE_ID);
	initSprite(MINION_FLEE_SMALL_BG_ID, MINION_FLEE_SMALL_SPRITE_ID);
	initSprite(MINION_INVESTIGATE_BG_ID, MINION_INVESTIGATE_SPRITE_ID);
	initSprite(MINION_INVESTIGATE_SMALL_BG_ID, MINION_INVESTIGATE_SMALL_SPRITE_ID);


	//player small
	GraphicsBuffer* pPlayerSmallBuffer = mpGraphicsBufferManager->getBuffer(PLAYER_SMALL_BG_ID);
	if (pPlayerSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PLAYER_SMALL_SPRITE_ID, pPlayerSmallBuffer, 0, 0, pPlayerSmallBuffer->getWidth(), pPlayerSmallBuffer->getHeight());
	}

	//enemy
	GraphicsBuffer* pEnemyBuffer = mpGraphicsBufferManager->getBuffer(ENEMY_BG_ID);
	if (pEnemyBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(ENEMY_SPRITE_ID, pEnemyBuffer, 0, 0, pEnemyBuffer->getWidth(), pEnemyBuffer->getHeight());
	}

	//enemy
	GraphicsBuffer* pEnemySmallBuffer = mpGraphicsBufferManager->getBuffer(ENEMY_SMALL_BG_ID);
	if (pEnemySmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(ENEMY_SMALL_SPRITE_ID, pEnemySmallBuffer, 0, 0, pEnemySmallBuffer->getWidth(), pEnemySmallBuffer->getHeight());
	}

	//pellet
	GraphicsBuffer* pPelletBuffer = mpGraphicsBufferManager->getBuffer(PELLET_BG_ID);
	if (pPelletBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PELLET_SPRITE_ID, pPelletBuffer, 0, 0, pPelletBuffer->getWidth(), pPelletBuffer->getHeight());
	}

	//pellet small
	GraphicsBuffer* pPelletSmallBuffer = mpGraphicsBufferManager->getBuffer(PELLET_SMALL_BG_ID);
	if (pPelletSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PELLET_SMALL_SPRITE_ID, pPelletSmallBuffer, 0, 0, pPelletSmallBuffer->getWidth(), pPelletSmallBuffer->getHeight());
	}

	//power pellet
	GraphicsBuffer* pPowerPelletBuffer = mpGraphicsBufferManager->getBuffer(POWER_PELLET_BG_ID);
	if (pPowerPelletBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(POWER_PELLET_SPRITE_ID, pPowerPelletBuffer, 0, 0, pPowerPelletBuffer->getWidth(), pPowerPelletBuffer->getHeight());
	}

	//power pellet
	GraphicsBuffer* pPowerPelletSmallBuffer = mpGraphicsBufferManager->getBuffer(POWER_PELLET_SMALL_BG_ID);
	if (pPowerPelletSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(POWER_PELLET_SMALL_SPRITE_ID, pPowerPelletSmallBuffer, 0, 0, pPowerPelletSmallBuffer->getWidth(), pPowerPelletSmallBuffer->getHeight());
	}

	//portal
	GraphicsBuffer* pPowerPortalBuffer = mpGraphicsBufferManager->getBuffer(PORTAL_BG_ID);
	if (pPowerPortalBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PORTAL_SPRITE_ID, pPowerPortalBuffer, 0, 0, pPowerPortalBuffer->getWidth(), pPowerPortalBuffer->getHeight());
	}

	//portal small
	GraphicsBuffer* pPortalSmallBuffer = mpGraphicsBufferManager->getBuffer(PORTAL_SMALL_BG_ID);
	if (pPortalSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PORTAL_SMALL_SPRITE_ID, pPortalSmallBuffer, 0, 0, pPortalSmallBuffer->getWidth(), pPortalSmallBuffer->getHeight());
	}

	//grue spawn
	GraphicsBuffer* pGrueSpawnBuffer = mpGraphicsBufferManager->getBuffer(GRUE_SPAWN_BG_ID);
	if (pGrueSpawnBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(GRUE_SPAWN_SPRITE_ID, pGrueSpawnBuffer, 0, 0, pGrueSpawnBuffer->getWidth(), pGrueSpawnBuffer->getHeight());
	}

	//grue spawn small
	GraphicsBuffer* pGrueSpawnSmallBuffer = mpGraphicsBufferManager->getBuffer(GRUE_SPAWN_SMALL_BG_ID);
	if (pGrueSpawnSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(GRUE_SPAWN_SMALL_SPRITE_ID, pGrueSpawnSmallBuffer, 0, 0, pGrueSpawnSmallBuffer->getWidth(), pGrueSpawnSmallBuffer->getHeight());
	}

	//player spawn
	GraphicsBuffer* pPlayerSpawnBuffer = mpGraphicsBufferManager->getBuffer(PLAYER_SPAWN_BG_ID);
	if (pPlayerSpawnBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PLAYER_SPAWN_SPRITE_ID, pPlayerSpawnBuffer, 0, 0, pPlayerSpawnBuffer->getWidth(), pPlayerSpawnBuffer->getHeight());
	}

	//player spawn small
	GraphicsBuffer* pPlayerSpawnSmallBuffer = mpGraphicsBufferManager->getBuffer(PLAYER_SPAWN_SMALL_BG_ID);
	if (pPlayerSpawnSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PLAYER_SPAWN_SMALL_SPRITE_ID, pPlayerSpawnSmallBuffer, 0, 0, pPlayerSpawnSmallBuffer->getWidth(), pPlayerSpawnSmallBuffer->getHeight());
	}

	//player powerpellet sprite
	GraphicsBuffer* pPlayerPowerpelletBuffer = mpGraphicsBufferManager->getBuffer(PLAYER_POWERPELLET_BG_ID);
	if (pPlayerPowerpelletBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PLAYER_POWERPELLET_SPRITE_ID, pPlayerPowerpelletBuffer , 0, 0, pPlayerPowerpelletBuffer->getWidth(), pPlayerPowerpelletBuffer->getHeight());
	}

	//player powerpellet small sprite
	/*
	GraphicsBuffer* pPlayerPowerpelletSmallBuffer = mpGraphicsBufferManager->getBuffer(PLAYER_POWERPELLET_SMALL_BG_ID);
	if (pPlayerPowerpelletSmallBuffer != NULL)
	{
		mpSpriteManager->createAndManageSprite(PLAYER_POWERPELLET_SMALL_SPRITE_ID, pPlayerPowerpelletSmallBuffer, 0, 0, pPlayerPowerpelletSmallBuffer->getWidth(), pPlayerPowerpelletSmallBuffer->getHeight());
	}
	*/

#pragma endregion


	return true;
}

bool Game::initializeAllegro()
{
	//load image loader addon
	if (!al_init_image_addon())
	{
		fprintf(stderr, "image addon failed to load!\n");
		return false;
	}

	//install audio stuff
	if (!al_install_audio())
	{
		fprintf(stderr, "failed to initialize sound!\n");
		return false;
	}

	if (!al_init_acodec_addon())
	{
		fprintf(stderr, "failed to initialize audio codecs!\n");
		return false;
	}

	if (!al_reserve_samples(1))
	{
		fprintf(stderr, "failed to reserve samples!\n");
		return false;
	}

	//should be somewhere else!
	al_init_font_addon();
	if (!al_init_ttf_addon())
	{
		printf("ttf font addon not initted properly!\n");
		return false;
	}

	if (!al_init_primitives_addon())
	{
		printf("Primitives addon not added!\n");
		return false;
	}

	return true;
}

void Game::cleanup()
{
	//delete the timers
	delete mpLoopTimer;
	mpLoopTimer = NULL;

	delete mpMasterTimer;
	mpMasterTimer = NULL;

	//delete the graphics system
	delete mpGraphicsSystem;
	mpGraphicsSystem = NULL;

	delete mpGraphicsBufferManager;
	mpGraphicsBufferManager = NULL;

	//delete managers

	delete mpMessageManager;
	mpMessageManager = NULL;

	delete mpInputManager;
	mpInputManager = NULL;

	delete mpSpriteManager;
	mpSpriteManager = NULL;

	//shutdown components
	al_uninstall_audio();
	al_shutdown_image_addon();
	al_shutdown_font_addon();
	al_shutdown_ttf_addon();
	al_shutdown_primitives_addon();
}

void Game::beginLoop()
{
	mpLoopTimer->start();
	//draw background
	Sprite* pBackgroundSprite = mpSpriteManager->getSprite(BACKGROUND_SPRITE_ID);
	pBackgroundSprite->draw(*(mpGraphicsSystem->getBackBuffer()), SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
}

void Game::processLoop()
{
	mpInputManager->update();
	mpMessageManager->processMessagesForThisframe();
}

bool Game::endLoop()
{
	mpGraphicsSystem->swap();
	mpLoopTimer->sleepUntilElapsed(LOOP_TARGET_TIME - mpLoopTimer->getElapsedTime());
	mpLoopTimer->stop();
	return mShouldExit;
}

float genRandomBinomial()
{
	return genRandomFloat() - genRandomFloat();
}

float genRandomFloat()
{
	float r = (float)rand()/(float)RAND_MAX;
	return r;
}

float mapRotationToRange( float rotation, float low, float high )
{
	while( rotation < low )
	{
		rotation += ( 2.0f * PI );
	}

	while( rotation > high )
	{
		rotation -= ( 2.0f * PI );
	}
	return rotation;
}
