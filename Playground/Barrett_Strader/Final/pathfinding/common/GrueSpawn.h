#pragma once

#include "GameItem.h"

class GrueSpawn : public GameItem
{
public:
	GrueSpawn(IDType type, int tileId, Vector2D origin, std::string mapName)
		: GameItem(type, tileId, origin, mapName){};

	~GrueSpawn(){};

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);

	void setActive(bool tmp){ mActive = tmp; };
	bool isActive(){ return mActive; };


private:
	bool mActive;
};

