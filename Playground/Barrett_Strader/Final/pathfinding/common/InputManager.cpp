#include <Allegro5/keyboard.h>
#include <Allegro5/keycodes.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <iostream>

#include "InputManager.h"
#include "Game.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "GameMessage.h"
#include "GameMessageManager.h"
#include "GraphicsSystem.h"
#include "SendKeyActionMessage.h"
#include "SendMouseActionMessage.h"

/*InputManager

Kyle Strader
Champlain College
2014
*/

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
	if (mpKeyboardEventQueue != NULL)
		al_destroy_event_queue(mpKeyboardEventQueue);	

	if (mpMouseEventQueue != NULL)
		al_destroy_event_queue(mpMouseEventQueue);

	al_uninstall_keyboard();
	al_uninstall_mouse();
}

int InputManager::initialize()
{
	srand(time(NULL));
	//install mouse
	if (!al_install_mouse())
	{
		printf("Mouse not installed!\n");
		return -1;
	}

	//install keyboard
	if (!al_install_keyboard())
	{
		printf("Keyboard not installed!\n");
		return -1;
	}

	//setup event queue for keyboard input
	mpKeyboardEventQueue = al_create_event_queue();
	al_register_event_source(mpKeyboardEventQueue, al_get_keyboard_event_source());

	//setup event queue for mouse input
	mpMouseEventQueue = al_create_event_queue();
	al_register_event_source(mpMouseEventQueue, al_get_mouse_event_source());

	return 0;
}

int InputManager::update()
{
	checkKeyboard();
	checkMouse(); 
	fireMessages();
	return 0;
}

int InputManager::checkKeyboard()
{
	//run through every keyboard event
	while (!al_event_queue_is_empty(mpKeyboardEventQueue))
	{
		al_get_next_event(mpKeyboardEventQueue, &mpKeyEvent);
		if (mpKeyEvent.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			mKeyPressStack.push(al_keycode_to_name(mpKeyEvent.keyboard.keycode));
		}
	}

	return 0;
}

int InputManager::checkMouse()
{
	//setup mouse state
	ALLEGRO_MOUSE_STATE mouseState;
	al_get_mouse_state(&mouseState);

	bool mouseDown = false;

	if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mMouseClickStack.push(std::pair<int, Vector2D>(1, Vector2D(mouseState.x, mouseState.y)));
		bool mouseDown = true;
	}
	else
		mMouseClickStack.push(std::pair<int, Vector2D>(4, Vector2D(mouseState.x, mouseState.y)));

	if (al_mouse_button_down(&mouseState, 2)) //right mouse click
	{
		mMouseClickStack.push(std::pair<int, Vector2D>(2, Vector2D(mouseState.x, mouseState.y)));
		bool mouseDown = true;
	}
	else
		mMouseClickStack.push(std::pair<int, Vector2D>(5, Vector2D(mouseState.x, mouseState.y)));

	if (al_mouse_button_down(&mouseState, 3)) //middle mouse click
	{
		mMouseClickStack.push(std::pair<int, Vector2D>(3, Vector2D(mouseState.x, mouseState.y)));
		bool mouseDown = true;
	}
	else
		mMouseClickStack.push(std::pair<int, Vector2D>(6, Vector2D(mouseState.x, mouseState.y)));

	if (!mouseDown)
		mMouseClickStack.push(std::pair<int, Vector2D>(0, Vector2D(mouseState.x, mouseState.y)));

	return 0;
}

int InputManager::fireMessages()
{
	//Send keyboard messages
	while (!mKeyPressStack.empty())
	{
		GameMessage* pMessage = new SendKeyActionMessage(mKeyPressStack.top());
		mKeyPressStack.pop();
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	//Send mouse messages
	while (!mMouseClickStack.empty())
	{
		std::pair<int, Vector2D> tmpPair = mMouseClickStack.top();
		mMouseClickStack.pop();
		GameMessage* pMessage = new SendMouseActionMessage(MouseClick(tmpPair.first), tmpPair.second);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	return 0;
}





