#pragma once

#include <string>
#include <Trackable.h>

#include "GameItem.h"

class Portal:public GameItem
{
public:
	//constructor/destructor
	Portal(std::string fromMapName, std::string toMapName, std::string _linkerID, int _tileID, IDType type, Vector2D origin);
	~Portal(){};

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);

	//getters
	inline const std::string getToMapName(){ return mToMapName; };
	inline const std::string getFromMapName(){ return mFromMapName; };
	inline const std::string getLinkerID(){ return mLinkerID; };
	inline const int getTileID(){ return mTileID; };

private:
	//private variables

	std::string mToMapName;
	std::string mFromMapName;
	std::string mLinkerID;
	int mTileID;
};