#pragma once

#include <vector>

#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "Sprite.h"

class HUD
{
public:
	//constructors/destructors
	HUD(int x, int y, int w, int h);
	~HUD();

	//utils
	void draw(GraphicsBuffer& gb);
	void toggleHelp(){ mHelp = !mHelp; };
	void setCurItem(std::string tmp){ mCurItem = tmp; };
	void setCurItemSprite(Sprite* tmp){ mCurItemSprite = tmp; };

private:
	int mX_Pos;
	int mY_Pos;
	int mWidth;
	int mHeight;

	ALLEGRO_FONT* mpFont;
	ALLEGRO_FONT* mpFontUpper;

	bool mHelp;

	std::vector<std::string> mHudItems;
	std::vector<std::string> mHelperItems;
	std::string mCurItem;
	Sprite* mCurItemSprite;
};