#pragma once

#include "GameItem.h"

class Pellet: public GameItem
{
public:
	Pellet(IDType type, int tileId, Vector2D origin, std::string mapName)
		: GameItem(type, tileId, origin, mapName){};

	~Pellet(){};

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);

	void setActive(bool tmp){ mActive = tmp; };
	bool isActive(){ return mActive; };

private:
	bool mActive;
};

