#include "LineSegment.h"

void LineSegment::draw()
{
	al_draw_line(mFrom.getX(), mFrom.getY(), mTo.getX(), mTo.getY(), al_map_rgb(255, 255, 255), 1);
}