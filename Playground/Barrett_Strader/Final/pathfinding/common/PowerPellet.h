#pragma once

#include "GameItem.h"

class PowerPellet: public GameItem
{
public:
	PowerPellet(IDType type, int tileId, Vector2D origin, std::string mapName)
		: GameItem(type, tileId, origin, mapName){};

	~PowerPellet(){};

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);

	void setActive(bool tmp){ mActive = tmp;  };
	void consume() { mActive = false; mRespawnTimer = MAX_RESPAWN_SEC; }
	bool isActive(){ return mActive; };

private:
	bool mActive;
	double mRespawnTimer;
	static const double MAX_RESPAWN_SEC;
};

