#pragma once

#include "GameItem.h"

class Wall : public GameItem
{
public:
	Wall(IDType type, int tileId, Vector2D origin, std::string mapName)
		: GameItem(type, tileId, origin, mapName){};

	~Wall(){};

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);
};