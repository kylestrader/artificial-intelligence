#include "PowerPellet.h"

const double PowerPellet::MAX_RESPAWN_SEC = 60;

void PowerPellet::update(float time)
{
	if (!mActive)
	{
		mRespawnTimer -= time;
		if (mRespawnTimer <= 0)
			mActive = true;
	}
}

void PowerPellet::draw(GraphicsBuffer* pBuffer)
{
	if (mActive)
	{
		mpSprite->draw(*pBuffer
			, mOrigin.getX()
			, mOrigin.getY()
			, 0.0f);
	}
}
