#pragma once

#include <allegro5/allegro_primitives.h>

#include "Game.h"
#include "Vector2D.h"

const int ACCESSORY_SIZE = 8;

class LineSegment : public Trackable
{
public:
	//constructors/destructors
	LineSegment(Vector2D from, Vector2D to)
		: mFrom(from)
		, mTo(to){};

	~LineSegment(){};

	//getters/setters
	Vector2D getFromPoint(){ return mFrom; };
	Vector2D getToPoint(){ return mTo; };

	//utils
	void draw();

private:
	Vector2D mFrom;
	Vector2D mTo;
};