#include "Sprite.h"
#include "GraphicsBuffer.h"
#include "GraphicsSystem.h"

Sprite::Sprite( GraphicsBuffer* pBuffer, float sx, float sy, float sw, float sh )
	:mpBitmap(NULL)
	,mWidth(sw)
	,mHeight(sh)
{
	if( pBuffer->isValid() )
	{
		mpBitmap = al_create_sub_bitmap( pBuffer->getBitmap(), sx, sy, sw, sh );
	}
}

Sprite::~Sprite()
{
	//nothing to do right now as the bitmap is always a subBitmap
}

void Sprite::draw( GraphicsBuffer& dest, float dx, float dy, float rotationInRadians, int flags )
{
	//set new target
	ALLEGRO_BITMAP* pOldTarget = GraphicsSystem::switchTargetBitmap(dest.getBitmap());
	if (rotationInRadians != 0.0f)
	{
		if (mpBitmap != NULL && dest.getBitmap() != NULL)
		{
			float centerX = al_get_bitmap_width(mpBitmap) / 2;
			float centerY = al_get_bitmap_height(mpBitmap) / 2;
			al_draw_rotated_bitmap(mpBitmap, centerX, centerY, dx, dy, rotationInRadians, flags);
		}
	}

	else
	{
		float centerX = al_get_bitmap_width(mpBitmap) / 2;
		float centerY = al_get_bitmap_height(mpBitmap) / 2;
		al_draw_bitmap(mpBitmap, dx - al_get_bitmap_width(mpBitmap) / 2, dy - al_get_bitmap_height(mpBitmap) / 2, flags);
	}
	GraphicsSystem::switchTargetBitmap(pOldTarget);
}