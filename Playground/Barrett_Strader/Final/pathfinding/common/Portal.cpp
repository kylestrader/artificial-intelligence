#include "Portal.h"

Portal::Portal(std::string fromMapName, std::string toMapName, std::string _linkerID, int _tileID, IDType type, Vector2D origin)
: GameItem(type, _tileID, origin, fromMapName)
,mFromMapName(fromMapName)
,mToMapName(toMapName)
,mLinkerID(_linkerID)
,mTileID(_tileID)
{
}

void Portal::update(float time)
{

}

void Portal::draw(GraphicsBuffer* pBuffer)
{
	mpSprite->draw(*pBuffer
		, mOrigin.getX()
		, mOrigin.getY()
		, 0.0f);
}