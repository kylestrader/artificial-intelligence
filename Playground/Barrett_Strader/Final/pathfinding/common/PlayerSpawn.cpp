#include "PlayerSpawn.h"

void PlayerSpawn::update(float time)
{

}

void PlayerSpawn::draw(GraphicsBuffer* pBuffer)
{
	if (mActive)
	{
		mpSprite->draw(*pBuffer
			, mOrigin.getX()
			, mOrigin.getY()
			, 0.0f);
	}
}
