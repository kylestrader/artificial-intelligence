#pragma once

#include <string>
#include <fstream>

#include "Game.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "Portal.h"
#include "Trackable.h"
#include "GameItem.h"
#include "Pellet.h"
#include "Powerpellet.h"
#include "Wall.h"
#include "GrueSpawn.h"
#include "PlayerSpawn.h"

const int HELPER_OBJECT_MARGIN = 4;
const int HELPER_OBJECT_BUFF = 4;

class Map: public Trackable
{
public:
	//constructor/destructor
	Map();
	~Map();

	//utils
	void initialize(int width, int height, int squareSize, std::string name);
	void update(float time);
	void draw(GraphicsBuffer& gb);
	inline void addPortal(Portal* portal) { mPortalDictionary.push_back(portal); };
	void removePortal(Portal* portal);
	void removePortal(int tileID);
	int checkForIdenticalPortals(Portal* portal);
	int checkNumMapPortalConnections(Portal* portal);
	void save(std::ofstream& theStream);
	std::vector<std::string> load(std::ifstream& theStream, std::string mapsDirectory);
	void refreshPortalConnections(std::map<std::string, Map*>& maps);
	inline std::vector < Portal* > getPortalDictionary() {return mPortalDictionary; };
	inline void addWallItem(Wall* item, int tileId){ mWalls[tileId] = item; };
	inline void addPelletItem(Pellet* item, int tileId){ mPellets[tileId] = item; };
	inline void addPowerPelletItem(PowerPellet* item, int tileId){ mPowerPellets[tileId] = item; };
	inline void addGrueSpawn(GrueSpawn* item, int tileId){ mGrueSpawns[tileId] = item; };
	inline void addPlayerSpawn(PlayerSpawn* item, int tileId){ mPlayerSpawns[tileId] = item; };
	inline void removeWallItem(int tileId){ if (mWalls.find(tileId) != mWalls.end()){ delete mWalls.find(tileId)->second; mWalls.erase(mWalls.find(tileId)); } };
	inline void removePelletItem(int tileId){ if (mPellets.find(tileId) != mPellets.end()){ delete mPellets.find(tileId)->second; mPellets.erase(mPellets.find(tileId)); } };
	inline void removePowerPelletItem(int tileId){ if (mPowerPellets.find(tileId) != mPowerPellets.end()){ delete mPowerPellets.find(tileId)->second; mPowerPellets.erase(mPowerPellets.find(tileId)); } };
	inline void removeGrueSpawn(int tileId){ if (mGrueSpawns.find(tileId) != mGrueSpawns.end()){ delete mGrueSpawns.find(tileId)->second; } mGrueSpawns.erase(mGrueSpawns.find(tileId)); };
	inline void removePlayerSpawn(int tileId){ if (mPlayerSpawns.find(tileId) != mPlayerSpawns.end()){ delete mPlayerSpawns.find(tileId)->second; mPlayerSpawns.erase(mPlayerSpawns.find(tileId)); } };
	inline int getNumPlayerSpawns(){ return mPlayerSpawns.size(); };
	Portal* getInMapPortalConnection(Portal* portal);
	int getPelletPopulationVal(){ return mPelletPopulationVal; };
	void setPelletPopulationVal(int tmp){ mPelletPopulationVal = tmp; };

	std::map<int, PlayerSpawn*> getPlayerSpawns() { return mPlayerSpawns; }

	std::map<int, GrueSpawn*> getGrueSpawns(){ return mGrueSpawns; };

	void regeneratePellets();

	//getters
	inline Grid* getGrid() { return mpGrid; };
	inline GridVisualizer* getGridVisualizer() { return mpGridVisualizer; };
	inline std::string getMapName() { return mMapName; };
	Portal* getPortal(int tileID);
	std::vector<Portal*> getPortals(std::string toMapName, std::string linkerID);
	Pellet* getPelletAtIndex(int index) { return (mPellets.count(index) && mPellets[index]->isActive()) ? mPellets[index] : NULL; }
	PowerPellet* getPowerPelletAtIndex(int index) { return (mPowerPellets.count(index) && mPowerPellets[index]->isActive()) ? mPowerPellets[index] : NULL; }

	//setters
	void setSelectedTileIndex(int index){ selectedTileIndex = index; };

private:
	//utils
	void cleanupPortals();
	void cleanupWalls();
	void cleanupPellets();
	void cleanupPowerPellets();
	void cleanupGrueSpawns();
	void cleanupPlayerSpawns();

	//private variables
	int selectedTileIndex;
	int mPelletPopulationVal;
	Grid* mpGrid;
	GridVisualizer* mpGridVisualizer;
	std::string mMapName;
	std::vector<Portal*> mPortalDictionary;
	std::map<int, Wall*> mWalls;
	std::map<int, Pellet*> mPellets;
	std::map<int, PowerPellet*> mPowerPellets;
	std::map<int, GrueSpawn*> mGrueSpawns;
	std::map<int, PlayerSpawn*> mPlayerSpawns;

	ALLEGRO_FONT* mpFont;
};