#pragma once

#include <string>

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
const int GRID_WIDTH = 960;
const int GRID_HEIGHT = 960;

const std::string OVERWORLD_NAME = "OverWorld.txt";

const int GRID_SQUARE_SIZE_OVERWORLD = 20;
const int GRID_SQUARE_SIZE_DUNGEON = 40;

const int PELLET_SPAWN_RATE = 30;