#pragma once

#include "Node.h"
#include <string>

class NodeF_CostHeap
{
public:
	NodeF_CostHeap(int maxSize, std::string mapName);
	~NodeF_CostHeap();

	//utils
	void insert(Node* node);
	Node* top();
	Node* pop();

private:
	void remove();

	Node** mpOpenList;
	int mOpenListSize;
	std::string mMapName;
};