#include "GrueManager.h"
#include "Game.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

GrueManager::GrueManager()
{
	mpTotalUnits = 0;
	mpAccumUnits = 0;
}

GrueManager::~GrueManager()
{
	unitCleanUp();
}

void GrueManager::addUnit(Grue* unit)
{
	mGrues.push_back(unit);
}

void GrueManager::draw(GraphicsBuffer* pBuffer)
{

	for each (Grue* g in mGrues)
	{
		g->draw(pBuffer);
	}
}

Grue* GrueManager::getUnit(int index)
{
	return mGrues.at(index);
}

void GrueManager::removeUnit(int index)
{
	if (index >= 0 || (unsigned)index < mGrues.size() - 1)
	{
		delete mGrues.at(index);
		mGrues.erase(mGrues.begin() + index);
	}
}

void GrueManager::unitCleanUp()
{
	for each (Grue* g in mGrues)
	{
		delete g;
	
	}
	mGrues.clear();
}

void GrueManager::update(float time)
{
	for each (Grue* g in mGrues)
		g->update(time);
}

