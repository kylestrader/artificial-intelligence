#include "NodeF_CostHeap.h"

NodeF_CostHeap::NodeF_CostHeap(int maxSize, std::string mapName)
{
	mpOpenList = new Node*[maxSize + 1]; 
	mOpenListSize = 0;
	mMapName = mapName;
}

void NodeF_CostHeap::insert(Node* node)
{
	mOpenListSize++;
	//std::cout << mOpenListSize << std::endl;
	mpOpenList[mOpenListSize] = node;

	int m = mOpenListSize;
	while (m != 1)
	{
		if (mpOpenList[m]->getF_Score() <= mpOpenList[m / 2]->getF_Score())
		{
			Node* tmp = mpOpenList[m / 2];
			mpOpenList[m / 2] = mpOpenList[m];
			mpOpenList[m] = tmp;
			m /= 2;
		}

		else
			break;
	}
}

void NodeF_CostHeap::remove()
{
	if (mOpenListSize == 0)
		return;

	mpOpenList[1] = mpOpenList[mOpenListSize];
	mOpenListSize--;
	//std::cout << mOpenListSize << std::endl;
	int u = 1;
	int v = u;
	while (true)
	{
		u = v;
		if (((2 * u) + 1) <= mOpenListSize)
		{
			if (mpOpenList[u]->getF_Score() >= mpOpenList[2 * u]->getF_Score())
				v = 2 * u;
			if (mpOpenList[v]->getF_Score() >= mpOpenList[(2 * u) + 1]->getF_Score())
				v = (2 * u) + 1;
		}

		else if (2 * u <= mOpenListSize)
		{
			if (mpOpenList[u]->getF_Score() >= mpOpenList[2 * u]->getF_Score())
				v = 2 * u;
		}

		if (u != v)
		{
			Node* tmp = mpOpenList[u];
			mpOpenList[u] = mpOpenList[v];
			mpOpenList[v] = tmp;
		}

		else
			break;
	}
}

Node* NodeF_CostHeap::top()
{
	if (mOpenListSize == 0)
		return NULL;

	return mpOpenList[1];
}

Node* NodeF_CostHeap::pop()
{
	if (mOpenListSize == 0)
		return NULL;

	Node* tmp = top();
	remove();
	//std::cout << mOpenListSize << std::endl;
	return tmp;
}