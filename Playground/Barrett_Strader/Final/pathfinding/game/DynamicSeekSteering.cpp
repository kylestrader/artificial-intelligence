#include "DynamicSeekSteering.h"
#include "KinematicUnit.h"

DynamicSeekSteering::DynamicSeekSteering(KinematicUnit *pMover, Vector2D pTarget, bool shouldFlee)
:mpMover(pMover)
,mpTarget(pTarget)
,mShouldFlee(shouldFlee)
{
	mApplyDirectly = false;
}

Steering* DynamicSeekSteering::getSteering()
{
	if( !mShouldFlee )
	{
		mLinear = mpTarget - mpMover->getPosition();
	}
	else
	{
		mLinear = mpMover->getPosition() - mpTarget;
	}

	if (!(mLinear.getX() == 0 && mLinear.getY() == 0))
	{
		mLinear.normalize();
		mLinear *= mpMover->getMaxAcceleration();
	}
	mAngular = 0;
	return this;
}