#include "GameAppInputInterpretor.h"
#include "GameApp.h"
#include "GameHUD.h"
#include "Cursor.h"
#include "GridVisualizer.h"
#include "GridGraph.h"
#include "Grid.h"
#include "GameMessageManager.h"
#include "GameMessage.h"
#include "PathToMessage.h"
#include "ToggleDrawVisitedMessage.h"
#include "ChangePlayerDirectionMessage.h"
#include "Player.h"

//for keyboard
void GameAppInputInterpretor::interpretInput(std::string input)
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);

	if (input == "D")
		gpGame->setDebug(!gpGame->isDebug());

	if (input == "T" && gpGame->isDebug())
		gpGame->setDrawDbgTxt(!gpGame->shouldDrawDbgTxt());

	if (input == "I")
		static_cast<GameApp*>(gpGame)->getPlayer()->toggleInvincibility();

	//alphabet
	if (input == "C")
	{
		if (mLeftClicked && mRightClicked)
		{
			GameMessage* pMessage = new ToggleDrawVisitedMessage(mpLastLeftClickedNode, mpLastRightClickedNode);
			gpGame->getGameMessageManager()->addMessage(pMessage, 0);
		}
	}

	if (input == "H")
	{
		pGameApp->getHUD()->toggleHelp();
	}

	if (input == "UP")
	{
		GameMessage* pMessage = new ChangePlayerDirectionMessage(UP);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	if (input == "RIGHT")
	{
		GameMessage* pMessage = new ChangePlayerDirectionMessage(RIGHT);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	if (input == "DOWN")
	{
		GameMessage* pMessage = new ChangePlayerDirectionMessage(DOWN);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	if (input == "LEFT")
	{
		GameMessage* pMessage = new ChangePlayerDirectionMessage(LEFT);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}

	//alternative
	else if (input == "ESCAPE")
	{
		gpGame->markForExit();
	}

	else if (input == "F1")
	{
		pGameApp->getHUD()->toggleHelp();
	}
}

//for mouse
void GameAppInputInterpretor::interpretInput(Vector2D mousePos, MouseClick click)
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);
	Grid* pGrid = pGameApp->getCurMap()->getGrid();

	mMousePos = transposeMousePos(mousePos);

	pGameApp->getCursor()->setTilePos(Vector2D(int(mMousePos.getX() / pGrid->getSquareSize()), int(mMousePos.getY() / pGrid->getSquareSize())));

	//check if cursor is in valid position or not
	if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == BLOCKING_VALUE ||
		pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == PORTAL_DISC ||
		pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == PORTAL_MAP_DISC)
	{
		pGameApp->getCursor()->setWarning(true);
	}

	/*
	if (click == LEFT_DOWN && !mPortalClickedState)
	{
		if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == CLEAR_VALUE)
		{
			Node* tmpNode;
			int index = pGrid->getSquareIndexFromPixelXY(mMousePos.getX(), mMousePos.getY());
			tmpNode = pGameApp->getCurGridGraph()->getNode(index);
			mpLastLeftClickedNode = tmpNode;
			if (!mLeftClicked)
				mLeftClicked = true;
			sendPathToMessage();
		}

		else if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == PORTAL_VALUE)
		{
			pGameApp->handlePortalClick(mMousePos);
			mPortalClickedState = true;
		}
	}

	else if (click == LEFT_UP && mPortalClickedState)
		mPortalClickedState = false;

	else if (click == RIGHT_DOWN)
	{
		if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == CLEAR_VALUE)
		{
			Node* tmpNode;
			int index = pGrid->getSquareIndexFromPixelXY(mMousePos.getX(), mMousePos.getY());
			tmpNode = pGameApp->getCurGridGraph()->getNode(index);
			mpLastRightClickedNode = tmpNode;
			if (!mRightClicked)
				mRightClicked = true;
			sendPathToMessage();
		}
	}
	*/

	if (click == NO_CLICK)
	{
		pGameApp->getCurMap()->setSelectedTileIndex(
			pGrid->getSquareIndexFromPixelXY(mMousePos.getX(),
			mMousePos.getY()));
	}
}

void GameAppInputInterpretor::sendPathToMessage()
{
	if (mLeftClicked && mRightClicked)
	{
		GameMessage* pMessage = new PathToMessage(mpLastLeftClickedNode, mpLastRightClickedNode);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}
}