#pragma once

#include "Player.h"
#include "GameMessage.h"

class ChangePlayerDirectionMessage:public GameMessage
{
public:
	ChangePlayerDirectionMessage(Direction direc)
		:mDirec(direc)
		,GameMessage(CHANGE_PLAYER_DIRECTION_MESSAGE){};

	~ChangePlayerDirectionMessage(){};

	void process();

private:
	Direction mDirec;
};

