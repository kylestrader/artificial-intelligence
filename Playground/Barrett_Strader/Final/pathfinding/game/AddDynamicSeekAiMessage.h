#pragma once

#include "GameMessage.h"
#include "Vector2d.h"
#include <vector>

class Vector2D;

class AddDynamicSeekAiMessage : public GameMessage
{
public:
	AddDynamicSeekAiMessage(const std::vector<Vector2D> path, std::string mapName);
	~AddDynamicSeekAiMessage();

	void process();

private:
	std::vector<Vector2D> mPath;
	std::string mMapName;
};