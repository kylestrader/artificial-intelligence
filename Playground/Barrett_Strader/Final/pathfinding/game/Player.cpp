#include "Player.h"

#include "Map.h"
#include "Game.h"
#include "GameApp.h"
#include "GridGraph.h"
#include "SwitchMapMessage.h"
#include "GameMessage.h"
#include "GameMessageManager.h"
#include "Sprite.h"
#include "Node.h"
#include "GraphicsBuffer.h"

#include <sstream>

const double Player::MAX_INVINC_SEC = 10;
const double Player::SEC_BETWEEN_BLINKS = 0.15;

Player::Player(std::string respawnMapName, int tileId, Sprite* dungeonSprite, Sprite* overWorldSprite, Sprite* powerupSprite, Sprite* dungeonPowerupSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration)
:GameUnit(dungeonSprite, overWorldSprite, position, orientation, velocity, rotationVel, mapName, maxDungeonVelocity, maxOverworldVelocity, maxDungeonAcceleration, maxOverworldAcceleration)
{
	this->respawnMapName = respawnMapName;
	prevTeleport = Vector2D(-1, -1);
	spawnTileId = tileId;
	mpPowerupSprite = powerupSprite;
	mpDungeonPowerupSprite = dungeonPowerupSprite;
	mPlayerDirec = LEFT;
	mPlayerDirecUpdate = LEFT;
}


Player::~Player()
{
}

void Player::update(float time)
{
	mpUnit->update(time);

#pragma region portalcheck
	Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
	std::vector<Portal*> portals = map->getPortalDictionary();

	int unitIndex = map->getGrid()->getSquareIndexFromPixelXY(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

	for each (Portal* p in portals)
	{
		if ((Vector2D::distance(prevTeleport, mpUnit->getPosition()) > map->getGrid()->getSquareSize()) ||
			(prevTeleport.getX() == -1 && prevTeleport.getY() == -1))
		{
			prevTeleport = Vector2D(-1, -1);
			if ((Vector2D::distanceSq(map->getGrid()->getOriginOfSquare(p->getTileID()), mpUnit->getPosition()) <= 10))
			{
				GameMessage* pMessage = new SwitchMapMessage(p->getToMapName());
				gpGame->getGameMessageManager()->addMessage(pMessage, 0);
				mMapName = p->getToMapName();
				Map* toMap = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
				std::vector<Portal*> connectedPortals = toMap->getPortals(p->getFromMapName(), p->getLinkerID());
				if (connectedPortals.size() <= 2)
				{
					if (p != connectedPortals.at(0))
					{
						mpUnit->setPosition(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID()));
						mpUnit->dynamicSeek(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID()));
					}
					else
					{
						mpUnit->setPosition(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(1)->getTileID()));
						mpUnit->dynamicSeek(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(1)->getTileID()));
					}

					if (toMap->getGrid()->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
					{
						if (mpCurSprite == mpDungeonSprite || mpCurSprite == mpOverworldSprite)
							mpCurSprite = mpDungeonSprite;
						else
							mpCurSprite = mpDungeonPowerupSprite;
						mpUnit->setMaxVelocity(mMaxDungeonVelocity);
						mpUnit->setMaxAcceleration(mMaxDungeonAcceleration);
					}

					else
					{
						if (mpCurSprite == mpDungeonSprite || mpCurSprite == mpOverworldSprite)
							mpCurSprite = mpOverworldSprite;
						else
							mpCurSprite = mpPowerupSprite;
						mpUnit->setMaxVelocity(mMaxOverworldVelocity);
						mpUnit->setMaxAcceleration(mMaxOverworldAcceleration);
					}

					prevTeleport = mpUnit->getPosition();
				}
			}
		}
	}
#pragma endregion

#pragma region pathing
	//update pathing

	Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

	// Drop invinc time
	if (mInvincible)
	{
		mInvincTimer -= (time);
		if (mInvincTimer < 3)
		{
			// BLINK!
			mBlinkTimer -= time;
			if (mBlinkTimer <= 0)
			{
				mVisible = !mVisible;
				mBlinkTimer = SEC_BETWEEN_BLINKS;
			}

			if (mInvincTimer <= 0)
			{
				mInvincible = false;
				mVisible = true;
				Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
				mpCurSprite = map->getGrid()->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON ? mpDungeonSprite : mpOverworldSprite;
			}
		}
	}

	if (Vector2D::distanceSq(pathfindingPosition, mpUnit->getTarget()) <= 10)
	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		int tileIndex = grid->getSquareIndexFromPixelXY(pathfindingPosition.getX(), pathfindingPosition.getY());

		// Check for power pellets
		if (grid->getValueAtIndex(tileIndex) == POWER_PELLET_VALUE)
		{
			Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
			if (map != NULL)
			{
				PowerPellet* pp = map->getPowerPelletAtIndex(tileIndex);
				if (pp && pp->isActive())
				{
					pp->consume();
					score += 1000;
					mInvincible = true;
					mInvincTimer = MAX_INVINC_SEC;
					mBlinkTimer = SEC_BETWEEN_BLINKS;
					Grid* grid = static_cast<GameApp*>(gpGame)->getMapByName(mMapName)->getGrid();

					if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
					{
						mpCurSprite = mpDungeonPowerupSprite;
					}
					else
					{
						mpCurSprite = mpPowerupSprite;
					}
				}
			}
		}

		// Check for pellets
		{
			Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
			if (map != NULL)
			{
				Pellet* pellet = map->getPelletAtIndex(tileIndex);
				if (pellet && pellet->isActive())
				{
					pellet->setActive(false);
					score += 100;
				}
			}
		}

		switch (mPlayerDirecUpdate)
		{
		case UP:
			if (grid->getValueAtIndex(tileIndex - grid->getGridWidth()) != 1)
			{
				mPlayerDirec = mPlayerDirecUpdate;
				mpUnit->setPaused(false);
			}
			break;
		case RIGHT:
			if (grid->getValueAtIndex(tileIndex + 1) != 1)
			{
				mPlayerDirec = mPlayerDirecUpdate;
				mpUnit->setPaused(false);
			}
			break;
		case DOWN:
			if (grid->getValueAtIndex(tileIndex + grid->getGridWidth()) != 1)
			{
				mPlayerDirec = mPlayerDirecUpdate;
				mpUnit->setPaused(false);
			}
			break;
		case LEFT:
			if (grid->getValueAtIndex(tileIndex - 1) != 1)
			{
				mPlayerDirec = mPlayerDirecUpdate;
				mpUnit->setPaused(false);
			}
			break;
		case NONE:default:
			break;
		}

		switch (mPlayerDirec)
		{
		case UP:
			if (grid->getValueAtIndex(tileIndex - grid->getGridWidth()) != 1)
			{
				mpUnit->dynamicSeek(grid->getOriginOfSquare(tileIndex - grid->getGridWidth()));
			}
			else
				mpUnit->setPaused(true);
			break;
		case RIGHT:
			if (grid->getValueAtIndex(tileIndex + 1) != 1)
			{
				mpUnit->dynamicSeek(grid->getOriginOfSquare(tileIndex + 1));
			}
			else
				mpUnit->setPaused(true);
			break;
		case DOWN:
			if (grid->getValueAtIndex(tileIndex + grid->getGridWidth()) != 1)
			{
				mpUnit->dynamicSeek(grid->getOriginOfSquare(tileIndex + grid->getGridWidth()));
			}
			else
				mpUnit->setPaused(true);
			break;
		case LEFT:
			if (grid->getValueAtIndex(tileIndex - 1) != 1)
			{
				mpUnit->dynamicSeek(grid->getOriginOfSquare(tileIndex - 1));
			}
			else
				mpUnit->setPaused(true);
			break;
		case NONE:default:
			break;
		}
	}
#pragma endregion

}

void Player::draw(GraphicsBuffer* pBuffer)
{
	if (!mVisible)
		return;

	if (mMapName == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
	{
		mpCurSprite->draw(*pBuffer, mpUnit->getPosition().getX(), mpUnit->getPosition().getY(), mpUnit->getOrientation());

		{
			Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

			GameApp* pGame = static_cast<GameApp*>(gpGame);
			GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
			Grid* grid = gridGraph->getGrid();
			int tileIndex = grid->getSquareIndexFromPixelXY(pathfindingPosition.getX(), pathfindingPosition.getY());

			stringstream ss;
			ss << "Player tile: #";
			ss << tileIndex;

			gpGame->getGraphicsSystem()->writeDbgText(ss.str().c_str(), pathfindingPosition.getX(), pathfindingPosition.getY());
		}
	}
}

void Player::kill()
{
	mMapName = respawnMapName;
	Map* map = static_cast<GameApp*>(gpGame)->getMapByName(respawnMapName);
	map->regeneratePellets();
	Vector2D spawnPoint = map->getGrid()->getULCornerOfSquare(spawnTileId);
	mpUnit->setPosition(spawnPoint);
	mpUnit->dynamicSeek(spawnPoint);

	if (map->getGrid()->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
	{
		mpCurSprite = mpDungeonSprite;
		mpUnit->setMaxVelocity(mMaxDungeonVelocity);
		mpUnit->setMaxAcceleration(mMaxDungeonAcceleration);
	}
	else
	{
		mpCurSprite = mpOverworldSprite;
		mpUnit->setMaxVelocity(mMaxOverworldVelocity);
		mpUnit->setMaxAcceleration(mMaxOverworldAcceleration);
	}

	static_cast<GameApp*>(gpGame)->switchMap(respawnMapName);
	
	mInvincible = true;
	mInvincTimer = 3;
	mBlinkTimer = SEC_BETWEEN_BLINKS;
}

void Player::toggleInvincibility()
{
	mInvincible = !mInvincible;
	mInvincTimer = 100000;
	Grid* grid = static_cast<GameApp*>(gpGame)->getMapByName(mMapName)->getGrid();

	if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
	{
		if (mInvincible)
			mpCurSprite = mpDungeonPowerupSprite;
		else
			mpCurSprite = mpDungeonSprite;
	}
	else
	{
		if (mInvincible)
			mpCurSprite = mpPowerupSprite;
		else
			mpCurSprite = mpOverworldSprite;
	}
}