#include "A_StarPathfinder.h"
#include "Path.h"
#include "Connection.h"
#include "GridGraph.h"
#include "Grid.h"
#include "Game.h"
#include "GameApp.h"
#include "Vector2D.h"
#include <Cursor.h>
#include <PerformanceTracker.h>
#include <algorithm>

A_StarPathfinder::A_StarPathfinder(Graph* pGraph)
	:GridPathfinder(dynamic_cast<GridGraph*>(pGraph))
{
#ifdef VISUALIZE_PATH
	mpPath = NULL;
#endif
	mPathColor = al_map_rgba(0, 150, 10, 40);
	mVisitedColor = al_map_rgba(100, 128, 100, 255);
	mStartColor = al_map_rgba(60, 150, 70, 255);
	mStopColor = al_map_rgba(60, 70, 150, 255);
}

A_StarPathfinder::~A_StarPathfinder()
{
#ifdef VISUALIZE_PATH
	delete mpPath;
#endif
}

Path* A_StarPathfinder::findPath(Node* pFrom, Node* pTo)
{
	gpPerformanceTracker->clearTracker("path");
	gpPerformanceTracker->startTracking("path");

	//Uses A* search Algorithm
	// :http://en.wikipedia.org/wiki/A*_search_algorithm

	if (mpPath != NULL)
	{
		delete mpPath;
		mpPath = NULL;
	}

	if (mVisitedNodes.size() > 0) mVisitedNodes.clear();

	std::map <std::string, std::map<int, Node*>> closedSetMap;

	std::map <std::string, NodeF_CostHeap*> openSetMap;

	std::map <std::string, std::map<int, Node*>> cameFromMap;

	std::map <std::string, std::map<int, bool>> isInOpenList;
	std::map <std::string, std::map<int, bool>> isInClosedList;

	Node* startNode = pFrom;
	Node* endNode = pTo;
	isInOpenList[startNode->getMapName()][startNode->getId()] = true;
	startNode->setG_Score(0);
	startNode->setF_Score(startNode->getG_Score() + estimateHeuristicCost(startNode, endNode));

	if (openSetMap.find(startNode->getMapName()) == openSetMap.end())
	{
		int maxNodes = static_cast<GameApp*>(gpGame)->getGridGraphByMap(startNode->getMapName())->getNumNodes();
		openSetMap[startNode->getMapName()] = new NodeF_CostHeap(maxNodes, startNode->getMapName());
		openSetMap[startNode->getMapName()]->insert(startNode);
	}

	while (openSetMapValidity(&openSetMap))
	{
		//check to make sure there is atleast one open set

		Node* currentNode = findAndTakeLowestFScore(&openSetMap);

#ifdef VISUALIZE_PATH
		mVisitedNodes.push_back(currentNode);
#endif
		if (currentNode == endNode)
		{
			mpPath = reconstructPath(&cameFromMap, endNode);
			gpPerformanceTracker->stopTracking("path");
			mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
			return mpPath;
		}
		closedSetMap[currentNode->getMapName()][currentNode->getId()] = currentNode;
		isInOpenList[startNode->getMapName()][startNode->getId()] = false;
		isInClosedList[startNode->getMapName()][startNode->getId()] = true;

		std::vector<Connection*> connections = static_cast<GameApp*>(gpGame)->getGridGraphByMap(currentNode->getMapName())->getConnections(currentNode->getId());
		int numConnections = connections.size();

		for (int i = 0; i < numConnections; i++)
		{
			Connection* currentConnection = connections[i];
			Node* neighbor = currentConnection->getToNode();

			if (openSetMap.find(neighbor->getMapName()) == openSetMap.end())
			{
				int maxNodes = static_cast<GameApp*>(gpGame)->getGridGraphByMap(neighbor->getMapName())->getNumNodes();
				openSetMap[neighbor->getMapName()] = new NodeF_CostHeap(maxNodes, neighbor->getMapName());
			}

			if (closedSetMap[neighbor->getMapName()].find(neighbor->getId()) != closedSetMap[neighbor->getMapName()].end())
				continue;

			int tentative_g_score = currentNode->getG_Score() + 1;

			bool exists = false;
			if (isInOpenList[neighbor->getMapName()].find(neighbor->getId()) != isInOpenList[neighbor->getMapName()].end())
				if (isInOpenList[neighbor->getMapName()][neighbor->getId()] == true)
					exists = true;

			if (!exists || tentative_g_score < neighbor->getG_Score())
			{
				cameFromMap[neighbor->getMapName()][neighbor->getId()] = currentNode;
				neighbor->setG_Score(tentative_g_score);
				neighbor->setF_Score(neighbor->getG_Score() + estimateHeuristicCost(neighbor, endNode));

				openSetMap[neighbor->getMapName()]->insert(neighbor);
				isInOpenList[neighbor->getMapName()][neighbor->getId()] = true;
				isInClosedList[neighbor->getMapName()][neighbor->getId()] = false;
			}
		}
	}
	gpPerformanceTracker->stopTracking("path");
	mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
	static_cast<GameApp*>(gpGame)->getCursor()->setWarning(true);

	return NULL;
}

int A_StarPathfinder::estimateHeuristicCost(Node* currentNode, Node* targetNode)
{
	if (currentNode->getMapName() == targetNode->getMapName())
	{
		//uses Manhattan heuristic
		//updated so that the heuristic looks at the distance on a tile basis (as opposed to a pixel basis)
		GridGraph* gridGraph = dynamic_cast <GridGraph*>(mpGraph);

		Vector2D currentPos = gridGraph->getGrid()->getULCornerOfSquare(currentNode->getId());
		Vector2D targetPos = gridGraph->getGrid()->getULCornerOfSquare(targetNode->getId());

		int toX = static_cast<int>(targetPos.getX() / gridGraph->getGrid()->getSquareSize());
		int toY = static_cast<int>(targetPos.getY() / gridGraph->getGrid()->getSquareSize());
		int fromX = static_cast<int>(currentPos.getX() / gridGraph->getGrid()->getSquareSize());
		int fromY = static_cast<int>(currentPos.getY() / gridGraph->getGrid()->getSquareSize());
		int graphDistX = static_cast<int>(abs(toX - fromX));
		int graphDistY = static_cast<int>(abs(toY - fromY));

		return graphDistX + graphDistY;
		//return int(Vector2D::distance(UL1, UL2));
	}

	else
	{
		return 1;
	}
}

Node* A_StarPathfinder::findAndTakeLowestFScore(std::map <std::string, NodeF_CostHeap*>* openSetMap)
{
	//gpPerformanceTracker->clearTracker("process");
	//gpPerformanceTracker->startTracking("process");
	//std::cout << gpPerformanceTracker->getElapsedTime("process") << std::endl;
	Node* smallestNode = NULL;
	std::string mapName;
	int smallestF_Score = INT_MAX;

	std::map <std::string, NodeF_CostHeap*>::iterator mapIter = openSetMap->begin();
	for (; mapIter != openSetMap->end(); mapIter++)
	{
		Node* smallest = mapIter->second->top();
		if (smallest != NULL) {
			if (smallest->getF_Score() < smallestF_Score)
			{
				smallestNode = mapIter->second->top();
				mapName = mapIter->first;
				smallestF_Score = mapIter->second->top()->getF_Score();
			}
		}
	}

	if (smallestNode != NULL)
	{
		Node* topNode = openSetMap->find(mapName)->second->pop();
		return topNode;
	}

	else
	{
		return NULL;
	}
}

Node* A_StarPathfinder::findLowestFScore(std::map <std::string, NodeF_CostHeap*>* openSetMap)
{
	//gpPerformanceTracker->clearTracker("process");
	//gpPerformanceTracker->startTracking("process");
	//std::cout << gpPerformanceTracker->getElapsedTime("process") << std::endl;
	Node* smallestNode = NULL;
	std::string mapName;
	int smallestF_Score = INT_MAX;

	std::map <std::string, NodeF_CostHeap*>::iterator mapIter = openSetMap->begin();
	for (; mapIter != openSetMap->end(); mapIter++)
	{
		Node* smallest = mapIter->second->top();
		if (smallest->getF_Score() < smallestF_Score)
		{
			smallestNode = mapIter->second->top();
			mapName = mapIter->first;
			smallestF_Score = mapIter->second->top()->getF_Score();
		}
	}

	return smallestNode;
}

Path* A_StarPathfinder::reconstructPath(map <std::string, map<int, Node*>>* cameFromMap, Node* currentNode)
{
	Path* pPath = NULL;
	if (cameFromMap->size() == 0)
	{
		return NULL;
	}
	
	if (cameFromMap->find(currentNode->getMapName())->second.find(currentNode->getId()) != cameFromMap->find(currentNode->getMapName())->second.end())
	{
		Node* node = cameFromMap->find(currentNode->getMapName())->second.find(currentNode->getId())->second;

		pPath = reconstructPath(cameFromMap, node);
		pPath->addNode(currentNode);
	}

	else
	{
		pPath = new Path();
		pPath->addNode(currentNode);
	}

	return pPath;
}

bool A_StarPathfinder::openSetMapValidity(std::map <std::string, NodeF_CostHeap*>* openSetMap)
{
	std::map <std::string, NodeF_CostHeap*>::iterator mapIter = openSetMap->begin();
	for (; mapIter != openSetMap->end(); mapIter++)
	{
		if (mapIter->second->top() != NULL)
		{
			return true;
		}
	}
	return false;
}