#pragma once

#include "Graph.h"

class Grid;

class GridGraph:public Graph
{
public:
	GridGraph( Grid* pGrid, std::string mapName);
	virtual ~GridGraph();

	inline void setGrid( Grid* pGrid ) { mpGrid = pGrid; };
	inline Grid* getGrid(){ return mpGrid; };
	void addPortalConnection(int tileID, Node* node);
	void init();

private:
	Grid* mpGrid;
	std::string mMapName;
};