#pragma once

#include "KinematicUnit.h"

class GameUnit: public Trackable
{
public:
	GameUnit(Sprite *dungeonSprite, Sprite* overWorldSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration);
	virtual ~GameUnit(){ if (mpUnit) delete mpUnit; };

	virtual void update(float time) = 0;
	virtual void draw(GraphicsBuffer* pBuffer) = 0;

	inline std::string getMapName() { return mMapName; };
	inline Vector2D getPosition() { return mpUnit->getPosition(); };

protected:
	KinematicUnit* mpUnit;
	Sprite* mpDungeonSprite;
	Sprite* mpOverworldSprite;
	Sprite* mpCurSprite;
	int mMaxDungeonVelocity;
	int mMaxOverworldVelocity;
	int mMaxDungeonAcceleration;
	int mMaxOverworldAcceleration;
	std::string mMapName;
};