#include "GameUnit.h"
#include "GameApp.h"
#include "Game.h"
#include "Grid.h"
#include "GridGraph.h"

GameUnit::GameUnit(Sprite *dungeonSprite, Sprite* overWorldSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration)
{
	int squareSize = static_cast<GameApp*>(gpGame)->getGridGraphByMap(mapName)->getGrid()->getSquareSize();
	Vector2D vectorTransform = Vector2D(position.getX() * squareSize, position.getY() * squareSize);

	mpDungeonSprite = dungeonSprite;
	mpOverworldSprite = overWorldSprite;

	if (squareSize == GRID_SQUARE_SIZE_DUNGEON)
	{
		mpUnit = new KinematicUnit(vectorTransform, orientation, velocity, rotationVel, mapName, maxDungeonVelocity, maxDungeonAcceleration);
		mpCurSprite = dungeonSprite;
	}
	
	else
	{
		mpUnit = new KinematicUnit(vectorTransform, orientation, velocity, rotationVel, mapName, maxOverworldVelocity, maxOverworldAcceleration);
		mpCurSprite = overWorldSprite;
	}
	
	mpUnit->dynamicSeek(vectorTransform);

	mMaxDungeonVelocity = maxDungeonVelocity;
	mMaxOverworldVelocity = maxOverworldVelocity;
	mMaxDungeonAcceleration = maxDungeonAcceleration;
	mMaxOverworldAcceleration = maxOverworldAcceleration;
	mMapName = mapName;
}