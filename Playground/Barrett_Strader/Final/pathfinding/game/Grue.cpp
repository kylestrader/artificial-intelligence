#include "Grue.h"

#include "GameApp.h"
#include "Map.h"
#include "Portal.h"
#include "Game.h"
#include "GameMessage.h"
#include "SwitchMapMessage.h"
#include "GameMessageManager.h"
#include "GridGraph.h"
#include "Sprite.h"
#include "Player.h"
#include "InputManager.h"
#include "GameAppInputInterpretor.h"
#include "GraphicsBuffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sstream>

Grue::Grue(Sprite* dungeonSprite, Sprite* overWorldSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration, std::string respawnMap, int respawnTileId)
:GameUnit(dungeonSprite, overWorldSprite, position, orientation, velocity, rotationVel, mapName, maxDungeonVelocity, maxOverworldVelocity, maxDungeonAcceleration, maxOverworldAcceleration)
,prevTeleport(-1, -1)
{
	mRespawnMapName = respawnMap;
	mRespawnTileId = respawnTileId;
	mIsActive = true;
	mIsAtRespawnPoint = false;

	mDirection = LEFT;
	mState = WANDER;
	pathfinder = new A_StarPathfinder(static_cast<GameApp*>(gpGame)->getGridGraphByMap(mapName));

	mpRespawnTimer = new Timer();

	yOffset = 0;

	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		mpPlayer = pGame->getPlayer();
	}

}


Grue::~Grue()
{
	if (pathfinder)
	{
		delete pathfinder;
		pathfinder = NULL;
	}

	if (mpRespawnTimer)
	{
		delete mpRespawnTimer;
		mpRespawnTimer = NULL;
	}
}

void Grue::update(float time)
{
	Player* p = static_cast<GameApp*>(gpGame)->getPlayer();
	Grid* g = static_cast<GameApp*>(gpGame)->getGridGraphByMap(mMapName)->getGrid();
	Grid* respawnGrid = static_cast<GameApp*>(gpGame)->getGridGraphByMap(mRespawnMapName)->getGrid();
	int playerIndex = g->getSquareIndexFromPixelXY(p->getPosition().getX(), p->getPosition().getY());
	int thisIndex = g->getSquareIndexFromPixelXY(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

	if (gpGame->isDebug())
	{
		ALLEGRO_KEYBOARD_STATE keys;
		al_get_keyboard_state(&keys);
		if (al_key_down(&keys, ALLEGRO_KEY_SPACE))
		{
			if (!mKeydownSpace)
			{
				mKeydownSpace = true;
				mPause = !mPause;
			}
		}
		else
		{
			mKeydownSpace = false;
		}
		if (al_key_down(&keys, ALLEGRO_KEY_N))
		{
			if (!mKeydownN)
			{
				mKeydownN = true;
				mStepFrame = true;
				mStepped = false;
				mPause = true;
			}
		}
		else
		{
			mKeydownN = false;
		}
		if (al_key_down(&keys, ALLEGRO_KEY_V))
		{
			if (!mKeydownV)
			{
				mKeydownV = true;
				mDrawVisualization = !mDrawVisualization;
			}
		}
		else
		{
			mKeydownV = false;
		}

		if (mStepFrame)
		{
			if (!mStepped)
			{
				mStepped = true;
				mPause = false;
			}
			else
			{
				mPause = true;
				mStepFrame = false;
				mStepped = false;
			}
		}

		if (mPause)
			return;
	}
	if (mIsActive && !mIsAtRespawnPoint)
	{
		if (p->getMapName() == mMapName //check to see if grue is on the same map as the player
			&& playerIndex == thisIndex) //check to see if the player and the grue are on the same index
		{
			if (p->isInvincible())
			{
				mIsActive = false;
				mpUnit->setPosition(respawnGrid->getOriginOfSquare(mRespawnTileId));
				mpUnit->dynamicSeek(mpUnit->getPosition());

				mMapName = mRespawnMapName;

				if (respawnGrid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
				{
					mpUnit->setMaxVelocity(mMaxDungeonVelocity);
					mpUnit->setMaxAcceleration(mMaxDungeonAcceleration);
				}

				else
				{
					mpUnit->setMaxVelocity(mMaxOverworldVelocity);
					mpUnit->setMaxAcceleration(mMaxOverworldAcceleration);
				}
				mIsAtRespawnPoint = true;
				mpRespawnTimer->start();
			}

			else
			{
				//kill player;
				p->kill();
			}
		}

		else
		{
			mpUnit->update(time);

			Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

			Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
			std::vector<Portal*> portals = map->getPortalDictionary();

			int unitIndex = map->getGrid()->getSquareIndexFromPixelXY(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

			for each (Portal* p in portals)
			{
				if ((Vector2D::distance(prevTeleport, mpUnit->getPosition()) > map->getGrid()->getSquareSize()) ||
					(prevTeleport.getX() == -1 && prevTeleport.getY() == -1))
				{
					prevTeleport = Vector2D(-1, -1);
					if ((Vector2D::distanceSq(map->getGrid()->getOriginOfSquare(p->getTileID()), mpUnit->getPosition()) <= 10))
					{
						mMapName = p->getToMapName();
						Map* toMap = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
						if (pathfinder)
							delete pathfinder;
						pathfinder = new A_StarPathfinder(static_cast<GameApp*>(gpGame)->getGridGraphByMap(mMapName));
						std::vector<Portal*> connectedPortals = toMap->getPortals(p->getFromMapName(), p->getLinkerID());
						if (connectedPortals.size() <= 2)
						{
							if (p != connectedPortals.at(0))
							{
								mpUnit->setPosition(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID()));
								mpUnit->dynamicSeek(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID()));
							}
							else
							{
								mpUnit->setPosition(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(1)->getTileID()));
								mpUnit->dynamicSeek(toMap->getGrid()->getOriginOfSquare(connectedPortals.at(1)->getTileID()));
							}

							if (toMap->getGrid()->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
							{
								mpCurSprite = mpDungeonSprite;
								mpUnit->setMaxVelocity(mMaxDungeonVelocity);
								mpUnit->setMaxAcceleration(mMaxDungeonAcceleration);
							}

							else
							{
								mpCurSprite = mpOverworldSprite;
								mpUnit->setMaxVelocity(mMaxOverworldVelocity);
								mpUnit->setMaxAcceleration(mMaxOverworldAcceleration);
							}

							prevTeleport = mpUnit->getPosition();
						}
						break;
					}
				}
			}

			// Pathing

			// FSM
			switch (mState)
			{
			case WANDER:
				doWander();
				if (!canSeePlayer())
				{
					mSeesPlayer = false;
				}
				else if (isPlayerInvincible())
					mState = FLEE;
				else
				{
					mSeesPlayer = true;
					mState = SEEK;
				}
				break;
			case SEEK:
				doSeek();
				if (!canSeePlayer())
				{
					mState = INVESTIGATE;
					enterInvestigate();
					mSeesPlayer = false;
				}
				else if (isPlayerInvincible())
					mState = FLEE;
				break;
			case INVESTIGATE:
				doInvestigate();
				if (canSeePlayer())
				{
					mSeesPlayer = true;
					if (isPlayerInvincible())
						mState = FLEE;
					else
						mState = SEEK;
				}
				else
				{
					mSeesPlayer = false;
					if (mPathToPlayer.empty())
					{
						mState = WANDER;
					}
				}
				break;
			case FLEE:
				doFlee();
				if (canSeePlayer())
				{
					mSeesPlayer = true;
					if (!isPlayerInvincible())
					{
						mState = SEEK;
					}
				}
				else
				{
					mSeesPlayer = false;
					mState = WANDER;
				}
				break;
			default:
				break;
			}

			if (canSeePlayer())
				playerOldPos = mpPlayer->getPosition();
		}
	}

	else if (mIsAtRespawnPoint)
	{
		double time = mpRespawnTimer->getElapsedTime();
		if (time >= GRUE_RESPAWN_TIME)
		{
			mIsActive = true;
			mIsAtRespawnPoint = false;
			mpRespawnTimer->stop();
		}
	}
}

void Grue::draw(GraphicsBuffer* pBuffer)
{
	if (!mIsActive)
		return;

	GameApp* pGame = static_cast<GameApp*>(gpGame);
	if (mMapName == pGame->getCurMap()->getMapName())
	{
		mpCurSprite->draw(*pBuffer, mpUnit->getPosition().getX(), mpUnit->getPosition().getY(), mpUnit->getOrientation());
#ifdef VISUALIZE_PATH
		if (gpGame->isDebug() && mDrawVisualization)
		{
			Grid* pGrid = pGame->getCurGridGraph()->getGrid();
			pathfinder->setPathColor(RED_COLOR);
			pathfinder->setShouldDrawVisited(true);
			pathfinder->setVisitedColor(ORANGE_COLOR);
			pathfinder->setStartColor(GREEN_COLOR);
			pathfinder->setStopColor(GREEN_COLOR);
			pathfinder->drawVisualization(pGrid, pBuffer);
		}
#endif VISUALIZE_PATH
		//mpSprite->draw(*pBuffer, unit->getPosition().getX(), unit->getPosition().getY(), unit->getOrientation(), mScale);
		mpCurSprite->draw(*pBuffer, mpUnit->getPosition().getX(), mpUnit->getPosition().getY(), mpUnit->getOrientation());

		Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

		yOffset = 0;

		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();

		if (gpGame->isDebug())
		{
			writeDbgStr("Grue tile: #%d Dist:%f", grid->getSquareIndexFromPixelXY(pathfindingPosition.getX(), pathfindingPosition.getY()), distFromTargetTile);
			writeDbgStr("Turning: %s", dirToA(mTurnDirection));
			writeDbgStr("Target index: %d", grid->getSquareIndexFromPixelXY(mpUnit->getTarget().getX(), mpUnit->getTarget().getY()));
			writeDbgStr(stateToA(mState));
			if (mSeesPlayer)
				writeDbgStr("SEES PLAYER");
		}
	}
}

void Grue::doWander()
{
	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
			mpCurSprite = mpDungeonSprite;
		else
			mpCurSprite = mpOverworldSprite;
	}

	if (mTurnDirection != NONE)
		assert(mTurnDirection == mDirection);

	pathfinder->initPath();

	Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

	if (reachedTarget())
	{

		distFromTargetTile = Vector2D::distance(pathfindingPosition, mpUnit->getTarget());
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		int tileIndex = grid->getSquareIndexFromPixelXY(pathfindingPosition.getX(), pathfindingPosition.getY());
		mpUnit->setPaused(false);

		std::vector<Direction> connections;

		const int upIndex = (tileIndex - grid->getGridWidth()),
				  rightIndex = (tileIndex + 1),
				  leftIndex = (tileIndex - 1),
				  downIndex = (tileIndex + grid->getGridWidth());

		int targetIndex = -1;

		if (canMoveToIndex(upIndex) && mDirection != DOWN)
			connections.push_back(UP);
		if (canMoveToIndex(rightIndex) && mDirection != LEFT)
			connections.push_back(RIGHT);
		if (canMoveToIndex(downIndex) && mDirection != UP)
			connections.push_back(DOWN);
		if (canMoveToIndex(leftIndex) && mDirection != RIGHT)
			connections.push_back(LEFT);

		if (connections.size() > 0)
		{
			if (mMoveLeftWhenAble)
			{
				bool hasLeft = false;
				{
					for (auto d : connections)
					{
						if (d == LEFT)
							hasLeft = true;
					}
				}
				if (hasLeft)
					mTurnDirection = LEFT;
				else
					mTurnDirection = connections.at((rand() % connections.size()));
			}
			else
				mTurnDirection = connections.at((rand() % connections.size()));

			mDirection = mTurnDirection;
		}

		switch (mDirection)
		{
		case UP:
			targetIndex = upIndex;
			break;
		case RIGHT:
			targetIndex = rightIndex;
			break;
		case DOWN:
			targetIndex = downIndex;
			break;
		case LEFT:
			targetIndex = leftIndex;
			break;
		case NONE:default:
			break;
		}

		if (grid->getValueAtIndex(targetIndex) != BLOCKING_VALUE)
		{
			mpUnit->setPaused(false);
			mpUnit->dynamicSeek(grid->getOriginOfSquare(targetIndex));
		}
	}
}

void Grue::doSeek()
{
	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_SEEK_SPRITE_ID);
		else
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_SEEK_SMALL_SPRITE_ID);
	}
	if (reachedTarget())
	{
		if (canSeePlayer())
		{
			GameApp* pGame = static_cast<GameApp*>(gpGame);
			GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
			Grid* grid = gridGraph->getGrid();
			int playerIndex = grid->getSquareIndexFromPixelXY(mpPlayer->getPosition().getX(), mpPlayer->getPosition().getY());
			int myIndex = grid->getSquareIndexFromPixelXY(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

			Node* pFrom = gridGraph->getNode(myIndex);
			Node* pTo = gridGraph->getNode(playerIndex);

			Path* path = pathfinder->findPath(pFrom, pTo);

			if (path)
			{
				auto vecPath = pGame->translatePath(path);

				while (!mPathToPlayer.empty())
				{
					mPathToPlayer.pop();
				}

				for (Vector2D v : vecPath)
				{
					mPathToPlayer.emplace(v.getX(), v.getY());
				}

				mPathToPlayer.pop();

				if (mPathToPlayer.size() >= 1)
				{
					Vector2D target = mPathToPlayer.front();
					mPathToPlayer.pop();
					mpUnit->dynamicSeek(target);
				}
			}
		}
	}
}

bool Grue::canSeePlayer()
{
	if (mMapName.compare(mpPlayer->getMapName()) != 0)
		return false;

	// TODO: Change this to grid distance instead of pixel distance
	if (static_cast<GameApp*>(gpGame)->getMapByName(mMapName)->getGrid()->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
		return Vector2D::distanceSq(mpUnit->getPosition(), mpPlayer->getPosition()) < 4 * VIEW_DISTANCE * VIEW_DISTANCE;
	else
		return Vector2D::distanceSq(mpUnit->getPosition(), mpPlayer->getPosition()) < VIEW_DISTANCE * VIEW_DISTANCE;

}

bool Grue::isPlayerInvincible()
{
	//return true;
	return mpPlayer->isInvincible();
}

void Grue::enterInvestigate()
{
	GameApp* pGame = static_cast<GameApp*>(gpGame);
	GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
	Grid* grid = gridGraph->getGrid();
	int playerIndex = grid->getSquareIndexFromPixelXY(playerOldPos.getX(), playerOldPos.getY());
	int myIndex = grid->getSquareIndexFromPixelXY(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());

	Node* pFrom = gridGraph->getNode(myIndex);
	Node* pTo = gridGraph->getNode(playerIndex);

	Path* path = pathfinder->findPath(pFrom, pTo);

	if (path)
	{
		auto vecPath = pGame->translatePath(path);

		while (!mPathToPlayer.empty())
		{
			mPathToPlayer.pop();
		}

		for (Vector2D v : vecPath)
		{
			mPathToPlayer.emplace(v.getX(), v.getY());
		}

		mPathToPlayer.pop();

		if (mPathToPlayer.size() >= 1)
		{
			Vector2D target = mPathToPlayer.front();
			mPathToPlayer.pop();
			mpUnit->dynamicSeek(target);
		}
	}
}

void Grue::doInvestigate()
{
	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_INVESTIGATE_SPRITE_ID);
		else
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_INVESTIGATE_SMALL_SPRITE_ID);
	}

	if (reachedTarget())
	{
		if (!mPathToPlayer.empty())
		{
			Vector2D target = mPathToPlayer.front();
			mPathToPlayer.pop();
			mpUnit->dynamicSeek(target);
		}
	}
}

void Grue::doFlee()
{
	{
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		if (grid->getSquareSize() == GRID_SQUARE_SIZE_DUNGEON)
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_FLEE_SPRITE_ID);
		else
			mpCurSprite = gpGame->getSpriteManager()->getSprite(MINION_FLEE_SMALL_SPRITE_ID);
	}
	pathfinder->initPath();

	if (reachedTarget())
	{
		Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());
		distFromTargetTile = Vector2D::distance(pathfindingPosition, mpUnit->getTarget());
		GameApp* pGame = static_cast<GameApp*>(gpGame);
		GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
		Grid* grid = gridGraph->getGrid();
		int tileIndex = grid->getSquareIndexFromPixelXY(pathfindingPosition.getX(), pathfindingPosition.getY());
		mpUnit->setPaused(false);

		std::vector<Direction> connections;

		const int upIndex = (tileIndex - grid->getGridWidth()),
			rightIndex = (tileIndex + 1),
			leftIndex = (tileIndex - 1),
			downIndex = (tileIndex + grid->getGridWidth());

		if (canMoveToIndex(upIndex) && mDirection != DOWN)
			connections.push_back(UP);
		if (canMoveToIndex(rightIndex) && mDirection != LEFT)
			connections.push_back(RIGHT);
		if (canMoveToIndex(downIndex) && mDirection != UP)
			connections.push_back(DOWN);
		if (canMoveToIndex(leftIndex) && mDirection != RIGHT)
			connections.push_back(LEFT);

		int farthestIndex = -1;

		for (Direction c : connections)
		{
			int currIndex = -1;

			switch (c)
			{
			case UP:
				currIndex = upIndex;
				break;
			case DOWN:
				currIndex = downIndex;
				break;
			case LEFT:
				currIndex = leftIndex;
				break;
			case RIGHT:
				currIndex = rightIndex;
				break;
			default:
				break;
			}

			if (currIndex != -1)
			{
				float currDistToPlayer = Vector2D::distance(grid->getOriginOfSquare(currIndex), mpPlayer->getPosition());
				float farthestDistToPlayer = Vector2D::distance(grid->getOriginOfSquare(farthestIndex), mpPlayer->getPosition());
				if (farthestIndex == -1)
					farthestIndex = currIndex;
				else if (currDistToPlayer > farthestDistToPlayer)
					farthestIndex = currIndex;
			}
		}

		if (farthestIndex != -1)
		{
			if (farthestIndex == upIndex)
				mTurnDirection = UP;
			else if (farthestIndex == downIndex)
				mTurnDirection = DOWN;
			else if (farthestIndex == rightIndex)
				mTurnDirection = RIGHT;
			else if (farthestIndex == leftIndex)
				mTurnDirection = LEFT;

			mDirection = mTurnDirection;
			mpUnit->dynamicSeek(grid->getOriginOfSquare(farthestIndex));
		}
	}
}

bool Grue::canMoveToIndex(int index)
{
	GameApp* pGame = static_cast<GameApp*>(gpGame);
	GridGraph* gridGraph = pGame->getGridGraphByMap(mMapName);
	Grid* grid = gridGraph->getGrid();

	const int indexValue = grid->getValueAtIndex(index);

	if (prevTeleport.getX() == -1 && prevTeleport.getY() == -1)
		return indexValue != BLOCKING_VALUE;
	else
		return indexValue != BLOCKING_VALUE && indexValue != PORTAL_VALUE;
}

bool Grue::reachedTarget()
{
	Vector2D pathfindingPosition = Vector2D(mpUnit->getPosition().getX(), mpUnit->getPosition().getY());
	distFromTargetTile = Vector2D::distance(pathfindingPosition, mpUnit->getTarget());
	return Vector2D::distanceSq(pathfindingPosition, mpUnit->getTarget()) <= 10;
}

void Grue::writeDbgStr(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	std::stringstream ss;

	int len = vsnprintf(0, 0, format, args);

	char* target_string = new char[++len];

	vsnprintf(target_string, len, format, args);

	gpGame->getGraphicsSystem()->writeDbgText(target_string, mpUnit->getPosition().getX(), mpUnit->getPosition().getY() + yOffset);
	yOffset += 12;

	delete[] target_string;
	va_end(args);
}