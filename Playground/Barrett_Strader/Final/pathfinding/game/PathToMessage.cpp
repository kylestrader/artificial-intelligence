#include "PathToMessage.h"
#include "Game.h"
#include "GameApp.h"
#include "GridPathfinder.h"
#include "Map.h"
#include "Grid.h"
#include "GridGraph.h"

void PathToMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	if( pGame != NULL ) 
	{
		//GridGraph* pFromGridGraph = pGame->getGridGraphByMap(mpFromNode->getMapName());
		//GridGraph* pToGridGraph = pGame->getGridGraphByMap(mpToNode->getMapName());

		//Grid* pFromGrid = pGame->getMapByName(mpFromNode->getMapName())->getGrid();
		//Grid* pToGrid = pGame->getMapByName(mpToNode->getMapName())->getGrid();

		//Vector2D fromVec = Vector2D(pFromGrid->getULCornerOfSquare(mpFromNode->getId()));
		//Vector2D toVec = Vector2D(pToGrid->getULCornerOfSquare(mpToNode->getId()));

		//int fromIndex = pFromGrid->getSquareIndexFromPixelXY((int)fromVec.getX(), (int)fromVec.getY());
		//int toIndex = pToGrid->getSquareIndexFromPixelXY((int)toVec.getX(), (int)toVec.getY());

		//GridPathfinder* pPathfinder = pGame->getCurPathfinder();
		//pPathfinder->findPath(mpFromNode, mpToNode);

		pGame->createWorldPath(mpFromNode, mpToNode);

	}
}
