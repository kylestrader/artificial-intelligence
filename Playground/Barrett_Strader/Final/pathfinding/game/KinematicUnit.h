#pragma once

#include "Kinematic.h"
#include "Steering.h"
#include "vector"
//#include "WanderSeekFleeSteering.h"

/*KinematicUnit - a unit that is derived from the Kinematic class.  Adds behaviors and max speeds and a current Steering.

Dean Lawson
Champlain College
2011
*/

//forward declarations
class Sprite;
class GraphicsBuffer;
class WanderSeekFleeSteering;
class WallSolid;

const float RADIUS = 23.0f;
const float EPSILON = 2.0f;
const float CONFINEMENT = 0.9f;

extern Steering gNullSteering;//global object - can point to it for a "NULL" Steering

//minmimum forward speed a unit has to have inorder to rotate 
//(keeps unit from spinning in place after stopping
const float MIN_VELOCITY_TO_TURN_SQUARED = 1.0f;
const float MAX_VIS_DIST = 200.0f;

class KinematicUnit: public Kinematic
{
public:
	KinematicUnit(const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string mapName, float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	~KinematicUnit();

	//getters and setters
	void setTarget( const Vector2D& target ) { mTarget = target; };
	Vector2D getTarget(){ return mTarget; };
	const Vector2D& getPosition() const { return mPosition; };
	float getMaxVelocity() const { return mMaxVelocity; };
	inline void setMaxVelocity(int maxVelocity){ mMaxVelocity = maxVelocity; };
	Vector2D getVelocity() const { return mVelocity; };
	inline float getMaxAcceleration() const { return mMaxAcceleration; };
	inline void setMaxAcceleration(int maxAcceleration){ mMaxAcceleration = maxAcceleration; };
	inline void setVelocity( const Vector2D& velocity ){ mVelocity = velocity; };
	inline void setId(std::string tmpId){ Id = tmpId; };
	inline void setPosition(const Vector2D& pos){ mPosition = pos; };
	inline std::vector <KinematicUnit*> getNeighbors(){ return mNeighbors; };
	inline void setPaused(bool pause){ mPaused = pause; };

	virtual void setNewOrientation();//face the direction you are moving
	void dynamicSeek(Vector2D pTarget);

	int getVisRadius() { return mVisRadius; };
	void setVisRadius(int rad) { mVisRadius = rad; };

	//move according to the current velocities and update velocities based on current Steering
	void update(float time);

private:
	int mCurPathIndex;
	bool mReverse;
	Steering* mpCurrentSteering;
	Vector2D mTarget;//used only for Kinematic seek and arrive
	float mMaxVelocity;
	float mMaxAcceleration;
	float mVisRadius;
	bool isParentBehavior;
	bool mPaused;

	std::vector<KinematicUnit*> mNeighbors;

	void setSteering( Steering* pSteering );
	void applyDirectly(Vector2D lin, float ang);
	void confine(Vector2D lin, float ang);

	std::string Id;

};