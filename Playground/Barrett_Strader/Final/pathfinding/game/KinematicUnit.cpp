#include "Game.h"
#include "GameApp.h"
#include "Kinematic.h"
#include "KinematicUnit.h"
#include "GrueManager.h"
#include "Sprite.h"
#include "GraphicsSystem.h"
#include "Steering.h"
#include "DynamicSeekSteering.h"
#include "SwitchDirectionColorMessage.h"
#include "SwitchMapMessage.h"
#include "GameMessageManager.h"
#include "Portal.h"
#include "Map.h"

using namespace std;

Steering gNullSteering( gZeroVector2D, 0.0f );

KinematicUnit::KinematicUnit(const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxVelocity, float maxAcceleration)
:Kinematic( position, orientation, velocity, rotationVel )
,mpCurrentSteering(NULL)
,mMaxVelocity(maxVelocity)
,mMaxAcceleration(maxAcceleration)
,mVisRadius(MAX_VIS_DIST)
{
	Id = "";
	mCurPathIndex = 1;
	mReverse = false;
}

KinematicUnit::~KinematicUnit()
{
	delete mpCurrentSteering;
}

void KinematicUnit::applyDirectly(Vector2D lin, float ang)
{
	//not stopped
	if (getVelocity().getLengthSquared() > MIN_VELOCITY_TO_TURN_SQUARED)
	{
		setVelocity(lin);
		setOrientation(ang);
	}

	//since we are applying the steering directly we don't want any rotational velocity
	setRotationalVelocity(0.0f);
}

void KinematicUnit::confine(Vector2D lin, float ang)
{
	//not stopped
	if (getVelocity().getLengthSquared() > MIN_VELOCITY_TO_TURN_SQUARED)
	{
		setVelocity(lin + (lin - mVelocity) * CONFINEMENT);
		setOrientation(ang + mRotationVel * CONFINEMENT);
	}
}

void KinematicUnit::update(float time)
{
	if (!mPaused)
	{
		Steering* steering;
		if (mpCurrentSteering != NULL)
		{
			steering = mpCurrentSteering->getSteering();
		}
		else
			steering = &gNullSteering;

		//calculate new velocities
		calcNewVelocities(*steering, time, mMaxVelocity, 12.0f);

		if( steering->shouldApplyDirectly() )
		{
			applyDirectly(steering->getLinear(), steering->getAngular());
			steering->setAngular(0.0f);
		}
		else
		{
			//confine(steering->getLinear(), steering->getAngular());
		}

		//move the unit using current velocities
		Kinematic::update(time);
		//set the orientation to match the direction of travel
		setNewOrientation();
	}
}

//private - deletes old Steering before setting
void KinematicUnit::setSteering( Steering* pSteering )
{
	delete mpCurrentSteering;
	
	mpCurrentSteering = pSteering;
}

void KinematicUnit::setNewOrientation()
{ 
	mOrientation = getOrientationFromVelocity( mOrientation, mVelocity ); 
}

//void KinematicUnit::seek(const Vector2D &target)
//{
//	KinematicSeekSteering* pSeekSteering = new KinematicSeekSteering( this, target );
//	setSteering( pSeekSteering );
//}

void KinematicUnit::dynamicSeek( Vector2D pTarget )
{
	DynamicSeekSteering* pDynamicSeekSteering = new DynamicSeekSteering(this, pTarget);
	setSteering( pDynamicSeekSteering );
	setTarget(pTarget);
}

