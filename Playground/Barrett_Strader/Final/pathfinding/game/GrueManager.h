#pragma once

#include "Grue.h"
#include <vector>

/*GrueManager - a storage unit that manages all existing units in the game. 

Kyle Strader
Champlain College
2014
*/

//chase type enumerator 
enum ChaseType 
{ 
	NO_CHASE, 
	DYNAMIC_SEEK, 
	DYNAMIC_ARRIVE,
	WANDER_SEEK,
	WANDER_FLEE,
	FLOCK_BOIDS
};

class GrueManager :public Trackable
{
public:
	//constructors/destructors
	GrueManager();
	~GrueManager();

	//Add unit 
	void addUnit(Grue* unit);
	//std::string addUnit(KinematicUnit* unit, ChaseType chaseType = NO_CHASE, KinematicUnit* target = 0);
	//std::string addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id = "", float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	//std::string addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id = "", ChaseType chaseType = NO_CHASE, KinematicUnit* target = 0, float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	
	//get unit
	Grue* getUnit(int index);
	std::vector <Grue*>  getGrues(){ return mGrues; };
	
	//remove unit
	void removeUnit(int index);
	
	//utils
	void draw(GraphicsBuffer* pBuffer);
	void unitCleanUp();
	void update(float time);

private:
	int mpTotalUnits;
	int mpAccumUnits;
	std::vector <Grue*> mGrues;

};
