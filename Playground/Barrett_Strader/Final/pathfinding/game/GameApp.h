#pragma once

/*Game - class to hold all game related info.

Dean Lawson
Champlain College
2011
*/

#include "Game.h"
#include "Vector2D.h"
#include <map>
#include <vector>

//forward declarations
class KinematicUnit;
class GridGraph;
class GridPathfinder;
class DebugDisplay;
class GameHUD;
class Cursor;
class Map;
class Path;
class Node;
class GrueManager;
class Player;
class Grue;

class GameApp: public Game
{
public:
	GameApp();
	virtual ~GameApp();

	//game loop
	virtual void beginLoop();
	virtual void processLoop();
	virtual bool endLoop();

	//utils
	virtual bool init();
	virtual void cleanup();
	void loadEnviornment(std::string name);
	int switchMap(std::string name);
	int switchMap(int index);
	bool doesMapExist(std::string name);
	bool doesMapExist(Map* _map);
	void handlePortalClick(Vector2D pos);
	void resetDebugDisplay();
	void createWorldPath(Node* start, Node* end);
	std::vector<Vector2D> translatePath(Path* path);

	//accessors
	inline Map* getCurMap() { return mpCurMap; };
	Map* getMapByName(std::string mapName) { return mMaps.count(mapName) ? mMaps[mapName] : NULL; };
	inline int getNumMaps() { return mMaps.size(); };
	inline GridPathfinder* getCurPathfinder() { return mpCurPathfinder; };
	inline GridPathfinder* getPathfinderByMap(std::string mapName) { return mPathfinders[mapName]; };
	inline int getNumPathfinders() { return mPathfinders.size(); };
	inline GridGraph* getCurGridGraph() { return mpCurGridGraph; };
	inline GridGraph* getGridGraphByMap(std::string mapName) { return mGridGraphs[mapName]; };
	inline int getNumGridGraphs(){ return mGridGraphs.size(); };
	inline GameHUD* getHUD(){ return mpHUD; };
	inline Cursor* getCursor(){ return mpCursor; };
	inline GrueManager* getGrueManager(){ return mpGrueManager; };
	inline Path* getGlobalPath(){ return mpGlobalPath; };
	inline std::map<std::string, Map*> getMaps(){ return mMaps; };
	inline std::map<std::string, GridPathfinder*> getPathfinders(){ return mPathfinders; };
	inline std::map<std::string, GridGraph*> getGridGraphs(){ return mGridGraphs; };
	inline Player* getPlayer(){ return mpPlayer; };

	//setters
	//checks to see if the map is valid
	inline void setCurPathfinder(std::string mapName) { if (doesMapExist(mapName)){ mpCurPathfinder = mPathfinders[mapName]; } };
	inline void setCurGridGraph(std::string mapName) { if (doesMapExist(mapName)){ mpCurGridGraph = mGridGraphs[mapName]; } };
	inline void setCurMap(std::string mapName) { if (doesMapExist(mapName)){ mpCurMap = mMaps[mapName]; } };

	//cleaning
	void cleanupMaps();
	void cleanupPathFinders();
	void cleanupGridGraphs();

private:
	//utils
	void loadMap(std::string name);
	void refreshPortals();
	void setupPortalConnections();
	DebugDisplay* mpDebugDisplay;
	void initializePathfinders();

	void spawnGrues();

	//private variables
	GridPathfinder* mpCurPathfinder;
	GridGraph* mpCurGridGraph;
	Path* mpGlobalPath;

	Map* mpCurMap;
	GameHUD* mpHUD;
	Cursor* mpCursor;

	GrueManager* mpGrueManager;

	std::map<std::string, Map*> mMaps;
	std::map<std::string, GridPathfinder*> mPathfinders;
	std::map<std::string, GridGraph*> mGridGraphs;

	Player* mpPlayer;
	Grue* mpGrue1;
	Grue* mpGrue2;
	Grue* mpGrue3;
	Grue* mpGrue4;
};

