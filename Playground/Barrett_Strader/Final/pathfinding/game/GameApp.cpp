#include <allegro5/allegro.h>

#include "Defines.h"
#include "EnviornmentalDefines.h"

#include "GameApp.h"
#include "GameAppInputInterpretor.h"

#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "GraphicsBufferManager.h"
#include "Sprite.h"
#include "SpriteManager.h"

#include "GameHUD.h"
#include "Cursor.h"
#include "DebugDisplay.h"
#include "PathfindingDebugContent.h"

#include "Map.h"
#include "GridGraph.h"
#include "GridPathFinder.h"
#include "A_StarPathfinder.h"
#include "Path.h"
#include "Node.h"

#include "GrueManager.h"
#include "AddDynamicSeekAiMessage.h"
#include "Path.h"
#include "GameMessageManager.h"
#include "SwitchMapMessage.h"

#include "Player.h"
#include "Grue.h"

#include <fstream>
#include <vector>

const int MAX_VELOCITY_OVERWORLD = 80;
const int MAX_ACCELERATION_OVERWORLD = 1600;
const int MAX_VELOCITY_DUNGEON = 160;
const int MAX_ACCELERATION_DUNGEON = 3200;

const float GRUE_HANDICAP = 0.90f;

GameApp::GameApp()
:Game()
,mpCurMap(NULL) // cur map data
,mpCurGridGraph(NULL) // cur grid data
,mpCurPathfinder(NULL) // cur pathfinder data
,mpDebugDisplay(NULL)
,mpHUD(NULL)
{
}

GameApp::~GameApp()
{
	cleanup();
}

bool GameApp::init()
{
	bool retVal = Game::init();
	if( retVal == false )
		return false;

	//create Overlay
	mpHUD = new GameHUD(mpGraphicsSystem->getHeight(), 0, 256, mpGraphicsSystem->getHeight());
	mpCursor = new Cursor(Vector2D(-1, -1), GRID_SQUARE_SIZE_OVERWORLD);
	loadEnviornment(OVERWORLD_NAME);

	//create global path finder and grid graph
	//mpGlobalGridGraph = new GridGraph()
	//mpGlobalPathfinder = new A_StarPathfinder()
	mpGlobalPath = new Path();

	//setup interpretor
	mpInputInterpretor = new GameAppInputInterpretor();

	//setup units
	mpGrueManager = new GrueManager();

	Vector2D pos2;
	std::string mapName;
	int tileId;
	for (auto m : mMaps)
	{
		if (m.second->getNumPlayerSpawns() > 0)
		{
			tileId = m.second->getPlayerSpawns().begin()->second->getTileId();
			mapName = m.second->getPlayerSpawns().begin()->second->getMapName();
			Grid* grid = getGridGraphByMap(mapName)->getGrid();
			pos2 = grid->getULCornerOfSquare(tileId) / grid->getSquareSize();
		}
	}
	Vector2D vel2(0.0f, 0.0f);
	mpPlayer = new Player(mapName, tileId, mpSpriteManager->getSprite(PLAYER_SPRITE_ID), mpSpriteManager->getSprite(PLAYER_SMALL_SPRITE_ID), mpSpriteManager->getSprite(PLAYER_POWERPELLET_SMALL_SPRITE_ID), mpSpriteManager->getSprite(PLAYER_POWERPELLET_SPRITE_ID), pos2, 0.0f, vel2, 1.0f, mapName, MAX_VELOCITY_DUNGEON, MAX_VELOCITY_OVERWORLD, MAX_ACCELERATION_DUNGEON, MAX_ACCELERATION_OVERWORLD);

	switchMap(mapName);

	//Vector2D grue1Pos(4, 24);
	//Vector2D grue1Vel(0.0f, 0.0f);

	//mpGrue1 = new Grue(mpSpriteManager->getSprite(ENEMY_SPRITE_ID), mpSpriteManager->getSprite(ENEMY_SMALL_SPRITE_ID), grue1Pos, 0.0f, grue1Vel, 1.0f, "OverWorld.txt", (float)MAX_VELOCITY_DUNGEON * GRUE_HANDICAP, (float)MAX_VELOCITY_OVERWORLD * GRUE_HANDICAP, (float)MAX_ACCELERATION_DUNGEON * GRUE_HANDICAP, (float)MAX_ACCELERATION_OVERWORLD * GRUE_HANDICAP, OVERWORLD_NAME, 50);

	//Vector2D grue2Pos(20, 24);
	//Vector2D grue2Vel(0.0f, 0.0f);

	//mpGrue2 = new Grue(mpSpriteManager->getSprite(ENEMY_SPRITE_ID), mpSpriteManager->getSprite(ENEMY_SMALL_SPRITE_ID), grue2Pos, 0.0f, grue2Vel, 1.0f, "OverWorld.txt", (float)MAX_VELOCITY_DUNGEON * GRUE_HANDICAP, (float)MAX_VELOCITY_OVERWORLD * GRUE_HANDICAP, (float)MAX_ACCELERATION_DUNGEON * GRUE_HANDICAP, (float)MAX_ACCELERATION_OVERWORLD * GRUE_HANDICAP);

	//Vector2D grue3Pos(24, 15);
	//Vector2D grue3Vel(0.0f, 0.0f);

	//mpGrue3 = new Grue(mpSpriteManager->getSprite(ENEMY_SPRITE_ID), mpSpriteManager->getSprite(ENEMY_SMALL_SPRITE_ID), grue3Pos, 0.0f, grue3Vel, 1.0f, "OverWorld.txt", (float)MAX_VELOCITY_DUNGEON * GRUE_HANDICAP, (float)MAX_VELOCITY_OVERWORLD * GRUE_HANDICAP, (float)MAX_ACCELERATION_DUNGEON * GRUE_HANDICAP, (float)MAX_ACCELERATION_OVERWORLD * GRUE_HANDICAP);

	//Vector2D grue4Pos(20, 24);
	//Vector2D grue4Vel(0.0f, 0.0f);

	//mpGrue4 = new Grue(mpSpriteManager->getSprite(ENEMY_SPRITE_ID), mpSpriteManager->getSprite(ENEMY_SMALL_SPRITE_ID), grue4Pos, 0.0f, grue4Vel, 1.0f, "OverWorld.txt", (float)MAX_VELOCITY_DUNGEON * GRUE_HANDICAP, (float)MAX_VELOCITY_OVERWORLD * GRUE_HANDICAP, (float)MAX_ACCELERATION_DUNGEON * GRUE_HANDICAP, (float)MAX_ACCELERATION_OVERWORLD * GRUE_HANDICAP);

	spawnGrues();
	mpMasterTimer->start();
	return true;
}

void GameApp::cleanup()
{
	delete mpGrueManager;
	mpGrueManager = NULL;

	delete mpPlayer;
	mpPlayer = NULL;

	//delete mpGrue1;
	//mpGrue1 = NULL;

	//delete mpGrue2;
	//mpGrue2 = NULL;
	//
	//delete mpGrue3;
	//mpGrue3 = NULL;
	//
	//delete mpGrue4;
	//mpGrue4 = NULL;

	if (mpGlobalPath != NULL)
	{
		delete mpGlobalPath;
		mpGlobalPath = NULL;
	}

	delete mpInputInterpretor;
	mpInputInterpretor = NULL;

	delete mpHUD;
	mpHUD = NULL;

	delete mpCursor;
	mpCursor = NULL;

	delete mpDebugDisplay;
	mpDebugDisplay = NULL;

	cleanupMaps();
	cleanupPathFinders();
	cleanupGridGraphs();
}

void GameApp::cleanupMaps()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end();)
	{
		delete iter->second;
		iter = mMaps.erase(iter);
	}
}

void GameApp::cleanupPathFinders()
{
	std::map<std::string, GridPathfinder*>::iterator iter;
	for (iter = mPathfinders.begin(); iter != mPathfinders.end(); ++iter)
	{
		delete iter->second;
	}
	mPathfinders.clear();
}

void GameApp::cleanupGridGraphs()
{
	std::map<std::string, GridGraph*>::iterator iter;
	for (iter = mGridGraphs.begin(); iter != mGridGraphs.end(); ++iter)
	{
		delete iter->second;
	}
	mGridGraphs.clear();
}

void GameApp::beginLoop()
{
	//should be the first thing done
	Game::beginLoop();
}

void GameApp::processLoop()
{
	//mpUnitManager->update(LOOP_TARGET_TIME / 1000.0f);
	//get back buffer
	GraphicsBuffer* pBackBuffer = mpGraphicsSystem->getBackBuffer();
	//copy to back buffer
#ifdef VISUALIZE_PATH
	//show pathfinder visualizer
	//mpCurPathfinder->drawVisualization(mpCurMap->getGrid(), pBackBuffer);
#endif

	mpCurMap->draw(*pBackBuffer);
	mpHUD->draw(*pBackBuffer, mpPlayer->getScore());
	mpCursor->draw(*pBackBuffer);
	//if (mpCursor->getWarning())
	//	mpCursor->setWarning(false);
	//mpDebugDisplay->draw(*pBackBuffer);
	mpPlayer->update(LOOP_TARGET_TIME / 1000.0f);
	mpPlayer->draw(pBackBuffer);
	//mpGrue1->update(LOOP_TARGET_TIME / 1000.0f);
	//mpGrue1->draw(pBackBuffer);
	//mpGrue2->update(LOOP_TARGET_TIME / 1000.0f);
	//mpGrue2->draw(pBackBuffer);
	//mpGrue3->update(LOOP_TARGET_TIME / 1000.0f);
	//mpGrue3->draw(pBackBuffer);
	//mpGrue4->update(LOOP_TARGET_TIME / 1000.0f);
	//mpGrue4->draw(pBackBuffer);
	mpGrueManager->update(LOOP_TARGET_TIME / 1000.0f);
	mpGrueManager->draw(pBackBuffer);

	for (auto m : mMaps)
	{
		m.second->update(LOOP_TARGET_TIME / 1000.0f);
	}

	Game::processLoop();
}

bool GameApp::endLoop()
{
	return Game::endLoop();
}

void GameApp::createWorldPath(Node* start, Node* end)
{
	mpCurPathfinder->findPath(start, end);

	resetDebugDisplay();

	Path* path = mpCurPathfinder->getPath();

	if (path != NULL)
	{
		*(mpGlobalPath) = *(path);

		initializePathfinders();

		for (int i = 0; i < mpGlobalPath->getNumNodes(); i++)
		{
			mPathfinders[mpGlobalPath->peekNode(i)->getMapName()]->addNode(mpGlobalPath->peekNode(i));
		}
	}

	translatePath(mpGlobalPath);

	mpGrueManager->unitCleanUp();
	GameMessage* pMessage = new AddDynamicSeekAiMessage(translatePath(static_cast<GameApp*>(gpGame)->getGlobalPath()), start->getMapName());
	mpMessageManager->addMessage(pMessage, 0);

	//GameMessage* _pMessage = new SwitchMapMessage(start->getMapName());
	//gpGame->getGameMessageManager()->addMessage(_pMessage, 0);
}

void GameApp::initializePathfinders()
{
	std::map<std::string, GridPathfinder*>::iterator iter;
	for (iter = mPathfinders.begin(); iter != mPathfinders.end(); ++iter)
	{
		iter->second->initPath();
	}
}

void GameApp::loadEnviornment(std::string name)
{
	cleanupMaps();
	cleanupPathFinders();
	cleanupGridGraphs();

	loadMap(name);

	if (mMaps.size() > 0)
	{
		switchMap(0);
		resetDebugDisplay();
	}
	else
	{
		mpCurMap = new Map();
		mpCurMap->initialize(mpGraphicsSystem->getHeight(), mpGraphicsSystem->getHeight(), GRID_SQUARE_SIZE_OVERWORLD, OVERWORLD_NAME);
		mMaps[mpCurMap->getMapName()] = mpCurMap;

		mpCurGridGraph = new GridGraph(mpCurMap->getGrid(), mpCurMap->getMapName()); //SWITCH WITH MAP CHANGE

		//init the nodes and connections
		mpCurGridGraph->init();
		mpCurPathfinder = new A_StarPathfinder(mpCurGridGraph); //SWITCH WITH MAP CHANGE
		resetDebugDisplay();
	}
	mpCurMap->getGridVisualizer()->setModified();

	refreshPortals();
	setupPortalConnections();
}

void GameApp::spawnGrues()
{
	std::map<std::string, Map*>::iterator iter = mMaps.begin();
	for (; iter != mMaps.end(); ++iter)
	{
		Map* m = iter->second;
		std::map<int, GrueSpawn*> spawns = m->getGrueSpawns();
		std::map<int, GrueSpawn*>::iterator _iter = spawns.begin();

		for (; _iter != spawns.end(); ++_iter)
		{
			GrueSpawn* g = _iter->second;
			mpGrueManager->addUnit(new Grue(mpSpriteManager->getSprite(ENEMY_SPRITE_ID)
				, mpSpriteManager->getSprite(ENEMY_SMALL_SPRITE_ID)
				, m->getGrid()->getULCornerOfSquare(g->getTileId()) / Vector2D(m->getGrid()->getSquareSize(), m->getGrid()->getSquareSize())
				, 0.0f
				, Vector2D(0, 0)
				, 0.0f
				, g->getMapName()
				, (float)MAX_VELOCITY_DUNGEON * GRUE_HANDICAP
				, (float)MAX_VELOCITY_OVERWORLD * GRUE_HANDICAP
				, (float)MAX_ACCELERATION_DUNGEON * GRUE_HANDICAP
				, (float)MAX_ACCELERATION_OVERWORLD * GRUE_HANDICAP
				, g->getMapName()
				, g->getTileId()));
		}
	}
}

void GameApp::setupPortalConnections()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end();)
	{
		Map* map = iter->second;
		std::vector<Portal*> portals = map->getPortalDictionary();

		for each (Portal* p in portals)
		{
			if (mMaps.find(p->getToMapName()) == mMaps.end())
				continue;

			std::vector<Portal*> portalIdenticals = mMaps[p->getToMapName()]->getPortals(map->getMapName(), p->getLinkerID());
			Portal* mToPortal = NULL;

			if (portalIdenticals.size() > 2)
				std::cout << "Error in portal connections" << std::endl;
			else
			{
				for each (Portal* _p in portalIdenticals)
				{
					if (p->getTileID() != _p->getTileID())
						mToPortal = _p;
				}
			}

			if (mToPortal != NULL)
			{
				int toNodeTileID = mToPortal->getTileID();
				Node* toNode = mGridGraphs[p->getToMapName()]->getNode(toNodeTileID);
				mGridGraphs[map->getMapName()]->addPortalConnection(p->getTileID(), toNode);
			}
		}
		++iter;
	}
}

void GameApp::loadMap(std::string name)
{
	std::vector<std::string> maps2Load;
	std::ifstream theStream(std::string(MAPS_DIRECTORY + name).c_str());

	if (theStream.is_open())
	{
		Map* tmpMap = new Map();

		int gridSize = GRID_SQUARE_SIZE_DUNGEON;

		if (name == OVERWORLD_NAME)
			gridSize = GRID_SQUARE_SIZE_OVERWORLD;

		//setup templated map to load stream into
		tmpMap->initialize(gpGame->getGraphicsSystem()->getHeight(), gpGame->getGraphicsSystem()->getHeight(), gridSize, name);

		if (theStream.is_open() && !doesMapExist(tmpMap))
		{
			maps2Load = tmpMap->load(theStream, MAPS_DIRECTORY);

			GridGraph* gg = new GridGraph(tmpMap->getGrid(), tmpMap->getMapName());
			gg->init();
			GridPathfinder* gpf = new A_StarPathfinder(gg);
			mGridGraphs[tmpMap->getMapName()] = gg;
			mPathfinders[tmpMap->getMapName()] = gpf;
			mMaps[tmpMap->getMapName()] = tmpMap;

			for each (std::string s in maps2Load)
			{
				loadMap(s);
			}
		}
		else
			delete tmpMap;
	}
	theStream.close();
}

void GameApp::resetDebugDisplay()
{
	delete mpDebugDisplay;
	PathfindingDebugContent* pContent = new PathfindingDebugContent(mpCurPathfinder); //SWITCH WITH MAP CHANGE
	mpDebugDisplay = new DebugDisplay(Vector2D(0, 12), pContent);
}

bool GameApp::doesMapExist(std::string name)
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		if (iter->second->getMapName() == name)
			return true;
	}
	return false;
}

//Does not check to see if map adresses are the same (runs name comparison)
bool GameApp::doesMapExist(Map* _map)
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		if (iter->second->getMapName() == _map->getMapName())
			return true;
	}
	return false;
}

void GameApp::handlePortalClick(Vector2D pos)
{
	int id = mpCurMap->getGrid()->getSquareIndexFromPixelXY(pos.getX(), pos.getY());
	Portal* p = mpCurMap->getPortal(id);
	switchMap(p->getToMapName());
}

int GameApp::switchMap(std::string name)
{
	if (doesMapExist(name))
	{
		mpCurMap = mMaps[name];
		mpCurGridGraph = mGridGraphs[name];
		mpCurPathfinder = mPathfinders[name];

		mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());
		return 0;
	}
	return -1;
}

int GameApp::switchMap(int index)
{
	int size = mMaps.size();
	if (index < size)
	{
		std::map<std::string, Map*>::iterator iter;
		iter = mMaps.begin();

		int compIndex = 0;
		for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
		{
			if (compIndex == index)
				break;
			else
				compIndex++;
		}
		mpCurMap = iter->second;
		mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());

		mpCurGridGraph = mGridGraphs[mpCurMap->getMapName()];
		mpCurPathfinder = mPathfinders[mpCurMap->getMapName()];

		mpCursor->setCurGridSize(mpCurMap->getGrid()->getSquareSize());
		return 0;
	}
	return -1;
}

void GameApp::refreshPortals()
{
	std::map<std::string, Map*>::iterator iter;
	iter = mMaps.begin();

	for (iter = mMaps.begin(); iter != mMaps.end(); ++iter)
	{
		iter->second->refreshPortalConnections(mMaps);
	}
}

std::vector<Vector2D> GameApp::translatePath(Path* path)
{
	std::vector<Vector2D> translated;

	int size = path->getNumNodes();
	for (int i = 0; i < size; i++)
	{
		GridGraph* tmpGridGraph = static_cast<GameApp*>(gpGame)->getGridGraphByMap(path->peekNode(i)->getMapName());
		Grid* tmpGrid = tmpGridGraph->getGrid();
		Vector2D preBuff = tmpGrid->getULCornerOfSquare(path->peekNode(i)->getId());
		preBuff = Vector2D(preBuff.getX() + ((tmpGrid->getSquareSize()) / 2), preBuff.getY() + ((tmpGrid->getSquareSize()) / 2));
		translated.push_back(preBuff);
	}
	return translated;
}

