#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro.h>

#include <string>
#include <sstream>

#include "GameHUD.h"
#include "Game.h"
#include "GraphicsSystem.h"
#include "SpriteManager.h"

GameHUD::GameHUD(int x, int y, int w, int h)
{
	mX_Pos = x;
	mY_Pos = y;
	mWidth = w;
	mHeight = h;
	mpFont = al_load_ttf_font(std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 14, ALLEGRO_ALIGN_CENTER);
	mpFontUpper = al_load_ttf_font(std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 28, ALLEGRO_ALIGN_CENTER);
	mHelp = true;
	mPlayerSprite = gpGame->getSpriteManager()->getSprite(PLAYER_SPRITE_ID);
}

GameHUD::~GameHUD()
{
	al_destroy_font(mpFont);
	al_destroy_font(mpFontUpper);
}

void GameHUD::draw(GraphicsBuffer& gb, int score)
{

	ALLEGRO_BITMAP* pOldTarget = GraphicsSystem::switchTargetBitmap(gb.getBitmap());

	std::string scorestr;

	{
		std::stringstream ss;
		ss << "Score: ";
		ss << score;
		scorestr = ss.str();
	}

	al_draw_filled_rectangle(mX_Pos, mY_Pos, mX_Pos + mWidth, mY_Pos + mHeight, al_map_rgba(0.3f, 0.3f, 0.3f, 0.01f));
	al_draw_text(mpFont, al_map_rgb(255, 255, 255), mX_Pos + 30, mY_Pos + 10, 0, scorestr.c_str());

	if (mHelp)
	{
		al_draw_filled_rectangle(mX_Pos + 10, mY_Pos + 120, mX_Pos + mWidth - 10, mY_Pos + mHeight - 10, al_map_rgba(0, 0, 0, 25));
		al_draw_text(mpFontUpper, al_map_rgb(255, 75, 75), mX_Pos + 30 + mWidth / 2, 30, ALLEGRO_ALIGN_CENTER, std::string("Controls:").c_str());

		int yPos = 80;

		auto writeControl = [this](int& yPos, const char* str) {
			al_draw_text(mpFont, al_map_rgb(255, 75, 75), mX_Pos + 30, yPos, ALLEGRO_ALIGN_LEFT, str);
			yPos += 30;
		};

		writeControl(yPos, "UP: Move up");
		writeControl(yPos, "DOWN: Move down");
		writeControl(yPos, "LEFT: Move left");
		writeControl(yPos, "RIGHT: Move right");
		writeControl(yPos, "D: Debug");
		writeControl(yPos, "I: Invincibility");
		writeControl(yPos, "V: toggle Visualize path (Debug only)");
		writeControl(yPos, "T: hide/show debug Text (Debug only)");
		writeControl(yPos, "SPACE: Pause/resume all Grues (Debug only)");
		writeControl(yPos, "N: step all grues forward to the Next frame (Debug only)");

	}
		mPlayerSprite->draw(gb, mX_Pos + 270, 125, 0.0f);
		

	GraphicsSystem::switchTargetBitmap(pOldTarget);
}