#include "ChangePlayerDirectionMessage.h"
#include "GameApp.h"
#include "Game.h"

void ChangePlayerDirectionMessage::process()
{
	static_cast<GameApp*>(gpGame)->getPlayer()->changeDirec(mDirec);
}
