#pragma once

#include "GridPathfinder.h"
#include <vector>
#include <map>
#include <queue>
#include "Node.h"
#include "NodeF_CostHeap.h"

class Path;
class Graph;
class GraphicsBuffer;
class Grid;

class A_StarPathfinder :public GridPathfinder
{
public:
	A_StarPathfinder(Graph* pGraph);
	~A_StarPathfinder();

	Path* findPath(Node* pFrom, Node* pTo);//make sure to delete the path when you are done!

private:
	int estimateHeuristicCost(Node* currentNode, Node* targetNode);
	Node* findLowestFScore(std::map <std::string, NodeF_CostHeap*>* openSetMap);
	Node* findAndTakeLowestFScore(std::map <std::string, NodeF_CostHeap*>* openSetMap);
	Path* reconstructPath(std::map <std::string, std::map<int, Node*>>* cameFromMap, Node* currentNode);
	bool openSetMapValidity(std::map <std::string, NodeF_CostHeap*>* openSetMap);
};