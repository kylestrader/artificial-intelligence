#pragma once

#include <vector>
#include <queue>
#include <stdarg.h>

#include "Vector2D.h"

#include "KinematicUnit.h"
#include "Player.h"
#include "A_StarPathfinder.h"
#include "GameUnit.h"
#include "Timer.h"

class Grue: public GameUnit
{
public:
	Grue(Sprite* dungeonSprite, Sprite* overWorldSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration, std::string respawnMap, int respawnTileId);
	~Grue();

	void update(float time);
	void draw(GraphicsBuffer* pBuffer);

private:
	enum State {
		WANDER,
		SEEK,
		FLEE,
		INVESTIGATE
	};

	const int VIEW_DISTANCE = 150; // Distance at which player is visible

	bool mIsActive;
	bool mIsAtRespawnPoint;
	std::string mRespawnMapName;
	int mRespawnTileId;
	Timer* mpRespawnTimer;
	Vector2D mPosition;

	std::vector<Vector2D> mPath;
	Vector2D prevTeleport;
	Direction mDirection;

	Vector2D playerOldPos;

	// Player -- For seeking, fleeing, etc.
	Player* mpPlayer;

	// Used in many places
	A_StarPathfinder* pathfinder;

	// Debugging stuff
	bool mPause = false;
	bool mStepFrame = false;
	bool mStepped = false;
	bool mMoveLeftWhenAble = false;
	bool mKeydownN = false;
	bool mKeydownV = false;
	bool mKeydownSpace = false;
	bool mSeesPlayer = false;
	float distFromTargetTile = -1;
	Direction mTurnDirection = NONE;
	bool mDrawVisualization = true;

	// Makes the state machine go 'round.
	State mState;

	// FSM functions
	void doWander();
	void doSeek();
	bool canSeePlayer();
	bool isPlayerInvincible();
	void enterInvestigate();
	void doInvestigate();
	void doFlee();

	std::queue<Vector2D> mPathToPlayer;

	// Handy utility function
	bool canMoveToIndex(int index);

	// Handy function!
	static const char* dirToA(Direction d)
	{
		switch (d)
		{
		case UP:
			return "UP";
		case DOWN:
			return "DOWN";
		case LEFT:
			return "LEFT";
		case RIGHT:
			return "RIGHT";
		case NONE:
			return "";
		default:
			return "UNKNOWN DIRECTION";
		}
	}

	static const char* stateToA(State s)
	{
		switch (s)
		{
		case WANDER:
			return "WANDER";
		case SEEK:
			return "SEEK";
		case FLEE:
			return "FLEE";
		case INVESTIGATE:
			return "INVESTIGATE";
		default:
			return "UNKNOWN STATE";
		}
	}

	bool reachedTarget();

	// Drawing debug stuff
	float yOffset;
	void writeDbgStr(const char* format, ...);
};