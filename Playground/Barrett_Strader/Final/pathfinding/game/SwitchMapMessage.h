#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class SwitchMapMessage :public GameMessage
{
public:
	SwitchMapMessage(std::string name): mName(name), GameMessage(SWITCH_MAP_MESSAGE){};
	~SwitchMapMessage(){};

	void process();

private:
	std::string mName;
};