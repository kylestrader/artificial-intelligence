#include "Game.h"
#include "GameApp.h"
#include "GameMessageManager.h"
#include "SwitchDirectionColorMessage.h"
#include "kinematicUnit.h"
#include "GrueManager.h"
#include "GridPathfinder.h"

SwitchDirectionColorMessage::SwitchDirectionColorMessage(bool reversed) :GameMessage(SWITCH_DIRECTION_COLOR)
{
	mReversed = reversed;
}

void SwitchDirectionColorMessage::process()
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);
	std::map<std::string, GridPathfinder*> gridPathfinders = pGameApp->getPathfinders();

	std::map<std::string, GridPathfinder*>::iterator iter;
	for (iter = gridPathfinders.begin(); iter != gridPathfinders.end(); ++iter)
	{
		GridPathfinder* tmp = iter->second;

		tmp->setReversed(mReversed);
	}
}