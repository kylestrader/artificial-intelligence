#pragma once
#include "GameMessage.h"

class SwitchDirectionColorMessage : public GameMessage
{
public:
	SwitchDirectionColorMessage(bool reversed);
	~SwitchDirectionColorMessage(){};

	void process();

private:
	bool mReversed;
};