#pragma once
#pragma once

#include "KinematicUnit.h"
#include "GameUnit.h"

enum Direction
{
	NONE = 0,
	UP = 1,
	RIGHT = 2,
	DOWN = 3,
	LEFT = 4
};

class Player: public GameUnit
{
public:
	Player(std::string respawnMapName, int tileId, Sprite* dungeonSprite, Sprite* overWorldSprite, Sprite* powerupSprite, Sprite* dungeonPowerupSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::string mapName, float maxDungeonVelocity, float maxOverworldVelocity, float maxDungeonAcceleration, float maxOverworldAcceleration);
	~Player();

	void update(float time);
	
	//draw yourself to the indicated buffer
	void draw(GraphicsBuffer* pBuffer);

	void changeDirec(Direction direc){ mPlayerDirecUpdate = direc; };
	void kill();
	void toggleInvincibility();
	inline bool isInvincible() { return mInvincible; }
	inline int getScore() { return score; }

private:
	Direction mPlayerDirec;
	Direction mPlayerDirecUpdate;
	Vector2D prevTeleport;
	std::string respawnMapName;
	int spawnTileId;
	Sprite* mpPowerupSprite;
	Sprite* mpDungeonPowerupSprite;
	std::vector<Vector2D> mPath;
	bool mInvincible = false;
	bool mVisible = true;
	double mInvincTimer = 0;
	double mBlinkTimer = 0;
	int score = 0;

	static const double MAX_INVINC_SEC;
	static const double SEC_BETWEEN_BLINKS;
};

