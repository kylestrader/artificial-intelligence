#include "ToggleDrawVisitedMessage.h"
#include "Game.h"
#include "GameApp.h"
#include "Map.h"
#include "GridGraph.h"
#include "GridPathfinder.h"
#include "PathToMessage.h"
#include "GameMessageManager.h"

void ToggleDrawVisitedMessage::process()
{
	GameApp* pGameApp = dynamic_cast<GameApp*>(gpGame);

	pGameApp->getCurPathfinder()->setShouldDrawVisited(
		!pGameApp->getCurPathfinder()->getShouldDrawVisited());

	if (mpFromNode->getId() != mpToNode->getId() && mpFromNode->getMapName() != mpToNode->getMapName())
	{
		GameMessage* pMessage = new PathToMessage(mpFromNode, mpToNode);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
		std::cout << "Re-evaluated Path Generation" << std::endl;
	}
}