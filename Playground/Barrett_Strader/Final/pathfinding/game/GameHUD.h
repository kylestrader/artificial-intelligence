#pragma once

#include <vector>

#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "Sprite.h"

class GameHUD
{
public:
	//constructors/destructors
	GameHUD(int x, int y, int w, int h);
	~GameHUD();

	//utils
	void draw(GraphicsBuffer& gb, int score);
	void toggleHelp(){ mHelp = !mHelp; };
	void setCurItemSprite(Sprite* tmp){ mPlayerSprite = tmp; };

private:
	int mX_Pos;
	int mY_Pos;
	int mWidth;
	int mHeight;

	ALLEGRO_FONT* mpFont;
	ALLEGRO_FONT* mpFontUpper;

	bool mHelp;
	Sprite* mPlayerSprite;
};