#include "A_StarPathfinder.h"
#include "Path.h"
#include "Connection.h"
#include "GridGraph.h"
#include "Grid.h"
#include "Game.h"
#include "Vector2D.h"
#include <PerformanceTracker.h>
#include <list>
#include <algorithm>

using namespace std;

A_StarPathfinder::A_StarPathfinder(Graph* pGraph)
	:GridPathfinder(dynamic_cast<GridGraph*>(pGraph))
{
#ifdef VISUALIZE_PATH
	mpPath = NULL;
#endif
	mPathColor = al_map_rgb(0, 255, 0);
	mVisitedColor = al_map_rgba(100, 128, 100, 255);
	mStartStopColor = al_map_rgba(100, 100, 100, 255);
}

A_StarPathfinder::~A_StarPathfinder()
{
#ifdef VISUALIZE_PATH
	delete mpPath;
#endif
}

Path* A_StarPathfinder::findPath(Node* pFrom, Node* pTo)
{
	gpPerformanceTracker->clearTracker("path");
	gpPerformanceTracker->startTracking("path");

	//Uses A* search Algorithm
	// :http://en.wikipedia.org/wiki/A*_search_algorithm

	if (mpPath != NULL)
	{
		delete mpPath;
		mpPath = NULL;
	}

	if (mVisitedNodes.size() > 0) mVisitedNodes.clear();

	map <int, Node*> closedSet;
	map <int, Node*> openSet;
	map <int, Node*> cameFrom;

	Node* startNode = pFrom;
	Node* endNode = pTo;
	openSet[startNode->getId()] = startNode;
	startNode->setG_Score(0);
	startNode->setF_Score(startNode->getG_Score() + estimateHeuristicCost(startNode, endNode));

	while (!openSet.empty())
	{
		Node* currentNode = findLowestFScore(endNode, &openSet);
		mVisitedNodes.push_back(currentNode);
		if (currentNode == endNode)
		{
			mpPath = reconstructPath(&cameFrom, endNode);
			gpPerformanceTracker->stopTracking("path");
			mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
			return mpPath;
		}

		openSet.erase(currentNode->getId());
		closedSet[currentNode->getId()] = currentNode;

		std::vector<Connection*> neighbors = mpGraph->getConnections(currentNode->getId());

		for each(Connection* connection in neighbors)
		{
			Node* neighbor = connection->getToNode();

			if (closedSet.find(neighbor->getId()) != closedSet.end())
				continue;

			int tentative_g_score = currentNode->getG_Score() + distanceBetweenNodes(currentNode, neighbor);

			if (openSet.find(neighbor->getId()) == openSet.end() || tentative_g_score < neighbor->getG_Score())
			{
				cameFrom[neighbor->getId()] = currentNode;
				neighbor->setG_Score(tentative_g_score);
				neighbor->setF_Score(neighbor->getG_Score() + estimateHeuristicCost(neighbor, endNode));

				if (openSet.find(neighbor->getId()) == openSet.end())
				{
					openSet[neighbor->getId()] = neighbor;
				}
			}
		}
	}
	gpPerformanceTracker->stopTracking("path");
	mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
	return NULL;
}

int A_StarPathfinder::estimateHeuristicCost(Node* currentNode, Node* targetNode)
{
	//uses Euclidian monotonic heuristic
	// :http://stackoverflow.com/questions/16869920/a-heuristic-calculation-with-euclidean-distance
	Vector2D UL1 = dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getULCornerOfSquare(currentNode->getId());
	Vector2D UL2 = dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getULCornerOfSquare(targetNode->getId());
	//return abs(UL1.getX() - UL2.getX()) + abs(UL2.getY() - UL2.getY());
	return Vector2D::distanceSq(UL1, UL2);
}

Node* A_StarPathfinder::findLowestFScore(Node* end, map <int, Node*>* set)
{
	map <int, Node*>::iterator iter = set->begin();
	Node* smallestNode = NULL;

	int smallestF_Score = INT_MAX;

	for (; iter != set->end(); iter++)
	{
		if (iter->second->getG_Score() + estimateHeuristicCost(iter->second, end) < smallestF_Score)
		{
			smallestNode = iter->second;
			smallestF_Score = smallestNode->getF_Score();
		}
	}
	return smallestNode;
}

Path* A_StarPathfinder::reconstructPath(map <int, Node*>* cameFromList, Node* currentNode)
{
	Path* pPath = NULL;
	if (cameFromList->find(currentNode->getId()) != cameFromList->end())
	{
		pPath = reconstructPath(cameFromList, cameFromList->find(currentNode->getId())->second);
		pPath->addNode(currentNode);
	}

	else
	{
		pPath = new Path();
		pPath->addNode(currentNode);
	}

	return pPath;
}