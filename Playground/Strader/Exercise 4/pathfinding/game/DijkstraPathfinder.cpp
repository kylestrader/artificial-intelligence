#include "DijkstraPathfinder.h"
#include "Path.h"
#include "Connection.h"
#include "GridGraph.h"
#include "Grid.h"
#include "Game.h"
#include "Vector2D.h"
#include <PerformanceTracker.h>
#include <list>
#include <algorithm>

DijkstraPathfinder::DijkstraPathfinder(Graph* pGraph)
	:GridPathfinder(dynamic_cast<GridGraph*>(pGraph))
{
#ifdef VISUALIZE_PATH
	mpPath = NULL;
#endif
	mPathColor = al_map_rgb(255, 0, 0);
	mVisitedColor = al_map_rgba(128, 100, 100, 255);
	mStartStopColor = al_map_rgba(100, 100, 100, 255);

}

DijkstraPathfinder::~DijkstraPathfinder()
{
#ifdef VISUALIZE_PATH
	delete mpPath;
#endif
}

Path* DijkstraPathfinder::findPath(Node* pFrom, Node* pTo)
{
	gpPerformanceTracker->clearTracker("path");
	gpPerformanceTracker->startTracking("path");

	//uses Dijkstra search algorithm
	// :http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

	//ensure that the path is empty and ready to use
	if (mpPath != NULL)
	{
		delete mpPath;
		mpPath = NULL;
	}

	if (mVisitedNodes.size() > 0) mVisitedNodes.clear();

	//Begin Initialization
	map <int, Node*> gridNodes;
	map <int, Node*> pathNodes;

	//set the default distance from source node and previous nodes in all nodes within the graph
	for (int i = 0; i < mpGraph->getNumNodes(); i++)
	{
		Node* currentNode = mpGraph->getNode(i);

		if (currentNode != pFrom)
		{
			currentNode->setDistanceFromSource(INT_MAX); //default distance is INT_MAX because no distance should ever be any higher than this default.
			currentNode->setPrevNode(NULL); //unknown prev node
		}

		else
		{
			currentNode->setDistanceFromSource(0); //set to zero because this represents the source node looking at itself
			currentNode->setPrevNode(NULL); //unknown prev node
		}

		//add node to gridNodes vector
		gridNodes[currentNode->getId()] = currentNode;
	}

	while (!gridNodes.empty())
	{
		//set the current node to the node that is the closest to the source node
		Node* currentNode = findClosestNodeTo(pFrom, &gridNodes);


		if (currentNode != NULL)
		{
			//remove from gridNodes and add to pathnodes vector and visited nodes vector
			gridNodes.erase(currentNode->getId());
			pathNodes[currentNode->getId()] = currentNode;
			mVisitedNodes.push_back(currentNode);

			//if current node is equal to the pTo node, then the path has been found and we should leave this loop immediatelty.
			if (currentNode != pTo)
			{
				for (int i = 0; i < (signed)mpGraph->getConnections(currentNode->getId()).size(); i++)
				{
					Connection* currentConnection = mpGraph->getConnections(currentNode->getId())[i];
					Node* connectedNode = currentConnection->getToNode();
					if (pathNodes.find(connectedNode->getId()) == pathNodes.end())
					{
						float altDist = currentNode->getDistanceFromSource() + distanceBetweenNodes(connectedNode, currentNode);

						if (altDist < connectedNode->getDistanceFromSource())
						{
							connectedNode->setDistanceFromSource(altDist);
							connectedNode->setPrevNode(currentNode);
						}
					}
				}
			}
			else
				break;
		}
		else
			break;
	}

	Path* pPath = new Path();
	Node* nodeToAdd = pTo;
	vector <Node*> tmpStack;

	while (nodeToAdd != NULL)
	{
		tmpStack.push_back(nodeToAdd);
		nodeToAdd = nodeToAdd->getPreviousNode();
	}

	while (tmpStack.size() > 0)
	{
		pPath->addNode(tmpStack.back());
		tmpStack.pop_back();
	}

	gpPerformanceTracker->stopTracking("path");
	mpPath = pPath;
	mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
	return pPath;

}

Node* DijkstraPathfinder::findClosestNodeTo(Node* point, map <int, Node*>* nodes)
{
	map <int, Node*>::iterator iter = nodes->begin();
	map <int, Node*>::iterator smallestIter = iter;

	float shortestDistance = INT_MAX;

	for (; iter != nodes->end(); ++iter)
	{
		if (iter->second->getDistanceFromSource() < shortestDistance)
		{
			smallestIter = iter;
			shortestDistance = iter->second->getDistanceFromSource();
		}
	}
	return smallestIter->second;
}

