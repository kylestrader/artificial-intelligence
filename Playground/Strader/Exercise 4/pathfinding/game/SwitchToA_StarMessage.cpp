#include "SwitchToA_StarMessage.h"
#include "A_StarPathfinder.h"
#include "Game.h"
#include "GameApp.h"
#include "GridGraph.h"
#include "PathToMessage.h"
#include "GameMessageManager.h"

void SwitchToA_StarMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	GridGraph* pGridGraph = pGame->getGridGraph();
	A_StarPathfinder* aStarPathfinder = new A_StarPathfinder(pGridGraph);
	pGame->setPathfinder(aStarPathfinder);
	std::cout << "Switched to A* Pathfinder" << std::endl;

	if (mTo.getX() != mFrom.getX() || mTo.getY() != mFrom.getY())
	{
		GameMessage* pMessage = new PathToMessage(mTo, mFrom);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		std::cout << "Re-evaluated Path Generation" << std::endl;
	}
}