#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class SwitchToDijkstraMessage :public GameMessage
{
public:
	SwitchToDijkstraMessage(Vector2D to, Vector2D from) : mTo(to), mFrom(from), GameMessage(SWITCH_DIJKSTRA_MESSAGE){};
	~SwitchToDijkstraMessage(){};

	void process();

private:
	Vector2D mTo;
	Vector2D mFrom;
};