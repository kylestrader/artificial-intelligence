#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class SwitchToA_StarMessage :public GameMessage
{
public:
	SwitchToA_StarMessage(Vector2D to, Vector2D from) : mTo(to), mFrom(from), GameMessage(SWITCH_A_STAR_MESSAGE){};
	~SwitchToA_StarMessage(){};

	void process();

private:
	Vector2D mTo;
	Vector2D mFrom;
};