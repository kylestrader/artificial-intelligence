#pragma once

#include <vector>

#include "Game.h"
#include "LineSegment.h"

class LineSegmentPath : public Trackable
{
public:
	//constructors/destructors
	LineSegmentPath(){};
	~LineSegmentPath(){};

	//utils
	void draw();
	void addSegment(LineSegment linSeg){ mPath.push_back(linSeg); };
	void removeSegmentAt(int index){ mPath.erase(mPath.begin() + index); };
	void toggleAccessorize(bool acc);

private:
	std::vector<LineSegment> mPath;
	bool mAccessorize;
};