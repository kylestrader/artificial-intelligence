#include "Node.h"

Node::Node()
:mId(BAD_NODE_ID)
{
}

Node::Node( const NODE_ID& id )
:mId(id)
{
	mDistFromSource = 0;
	mG_Score = 0;
	mF_Score = 0;
	mpPrevNode = NULL;
}

Node::~Node()
{
}