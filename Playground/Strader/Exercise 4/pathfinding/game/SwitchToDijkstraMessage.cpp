#include "SwitchToDijkstraMessage.h"
#include "DijkstraPathfinder.h"
#include "Game.h"
#include "GameApp.h"
#include "GridGraph.h"
#include "PathToMessage.h"
#include "GameMessageManager.h"

void SwitchToDijkstraMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	GridGraph* pGridGraph = pGame->getGridGraph();
	DijkstraPathfinder* dijkstraPathfinder = new DijkstraPathfinder(pGridGraph);
	pGame->setPathfinder(dijkstraPathfinder);
	std::cout << "Switched to Dijkstra Pathfinder" << std::endl;

	if (mTo.getX() != mFrom.getX() || mTo.getY() != mFrom.getY())
	{
		GameMessage* pMessage = new PathToMessage(mTo, mFrom);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		std::cout << "Re-evaluated Path Generation" << std::endl;
	}
}