#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class ToggleDrawVisitedMessage :public GameMessage
{
public:
	ToggleDrawVisitedMessage(Vector2D to, Vector2D from) : mTo(to), mFrom(from), GameMessage(TOGGLE_DRAW_VISITED){};
	~ToggleDrawVisitedMessage(){};

	void process();

private:
	Vector2D mTo;
	Vector2D mFrom;
};