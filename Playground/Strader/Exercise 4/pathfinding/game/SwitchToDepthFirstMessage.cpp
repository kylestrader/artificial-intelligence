#include "SwitchToDepthFirstMessage.h"
#include "DepthFirstPathfinder.h"
#include "Game.h"
#include "GameApp.h"
#include "GridGraph.h"
#include "PathToMessage.h"
#include "GameMessageManager.h"

void SwitchToDepthFirstMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	GridGraph* pGridGraph = pGame->getGridGraph();
	DepthFirstPathfinder* DFS_Pathfinder = new DepthFirstPathfinder(pGridGraph);
	pGame->setPathfinder(DFS_Pathfinder);
	std::cout << "Switched to Depth First Pathfinder" << std::endl;

	if (mTo.getX() != mFrom.getX() || mTo.getY() != mFrom.getY())
	{
		GameMessage* pMessage = new PathToMessage(mTo, mFrom);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		std::cout << "Re-evaluated Path Generation" << std::endl;
	}
}