#pragma once

#include "Graph.h"

class Grid;

class GridGraph:public Graph
{
public:
	GridGraph( Grid* pGrid );
	virtual ~GridGraph();

	inline void setGrid( Grid* pGrid ) { mpGrid = pGrid; };
	inline Grid* getGrid(){ return mpGrid; };
	void init();

private:
	Grid* mpGrid;
};