#pragma once

#include "GridPathfinder.h"
#include <vector>
#include <map>


class Path;
class Graph;
class GraphicsBuffer;
class Grid;

class A_StarPathfinder :public GridPathfinder
{
public:
	A_StarPathfinder(Graph* pGraph);
	~A_StarPathfinder();

	Path* findPath(Node* pFrom, Node* pTo);//make sure to delete the path when you are done!

private:
	int estimateHeuristicCost(Node* currentNode, Node* targetNode);
	Node* findLowestFScore(Node* end, std::map <int, Node*>* set);
	Path* reconstructPath(std::map <int, Node*>* cameFromList, Node* currentNode);
};