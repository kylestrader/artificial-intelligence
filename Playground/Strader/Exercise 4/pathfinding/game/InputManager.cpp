#include <stdlib.h>
#include <time.h>

#include "InputManager.h"
#include "Game.h"
#include "GameApp.h"
#include "GameMessage.h"
#include "GameMessageManager.h"
#include "GraphicsSystem.h"
#include "PathToMessage.h"
#include "SwitchToDepthFirstMessage.h"
#include "SwitchToDijkstraMessage.h"
#include "SwitchToA_StarMessage.h"
#include "ToggleDrawVisitedMessage.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

InputManager::InputManager()
{
	mpKeyState.A_KEY_DOWN = false;
	mpKeyState.B_KEY_DOWN = false;
	mpKeyState.C_KEY_DOWN = false;
	mpKeyState.D_KEY_DOWN = false;
	mpKeyState.W_KEY_DOWN = false;
	mpKeyState.ESC_KEY_DOWN = false;
	mpKeyState.MINUS_KEY_DOWN = false;
	mpKeyState.PLUS_KEY_DOWN = false;
	mpKeyState.S_KEY_DOWN = false;
	mpKeyState.F_KEY_DOWN = false;
	mpKeyState.T_KEY_DOWN = false;
	mpKeyState.Y_KEY_DOWN = false;

	mpMouseState.LEFT_DOWN = false;
	mpMouseState.RIGHT_DOWN = false;
	mpMouseState.LEFT_LOC.setX(0);
	mpMouseState.LEFT_LOC.setY(0);
	mpMouseState.RIGHT_LOC.setX(0);
	mpMouseState.RIGHT_LOC.setY(0);
}

InputManager::~InputManager()
{
	al_destroy_event_queue(mpKeyboardEventQueue);
	al_uninstall_keyboard();
	al_uninstall_mouse();
}

int InputManager::initialize()
{
	srand(time(NULL));
	//install mouse
	if (!al_install_mouse())
	{
		printf("Mouse not installed!\n");
		return -1;
	}

	//install keyboard
	if (!al_install_keyboard())
	{
		printf("Keyboard not installed!\n");
		return -1;
	}

	//setup event queue for keyboard input
	mpKeyboardEventQueue = al_create_event_queue();
	al_register_event_source(mpKeyboardEventQueue, al_get_keyboard_event_source());

	return 0;
}

int InputManager::update()
{
	checkKeyboard();
	checkMouse(); 
	fireMessages();
	return 0;
}

int InputManager::checkKeyboard()
{
	//run through every keyboard event
	while (!al_event_queue_is_empty(mpKeyboardEventQueue))
	{
		al_get_next_event(mpKeyboardEventQueue, &mpEvent);
		if (mpEvent.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (mpEvent.keyboard.keycode)
			{
			case ALLEGRO_KEY_A: //A key pressed
				mpKeyState.A_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_B: //A key pressed
				mpKeyState.B_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_C: //C key pressed
				mpKeyState.C_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_D: //D key pressed
				mpKeyState.D_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_W: //W key pressed
				mpKeyState.W_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_ESCAPE: //Esc key pressed
				mpKeyState.ESC_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_EQUALS: //= key pressed
				mpKeyState.PLUS_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_MINUS: //- key pressed
				mpKeyState.MINUS_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_S: //S key pressed
				mpKeyState.S_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_F: //F key pressed
				mpKeyState.F_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_T: //S key pressed
				mpKeyState.T_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_Y: //F key pressed
				mpKeyState.Y_KEY_DOWN = true;
				break;

			default:
				break;
			}
		}
	}

	return 0;
}

int InputManager::checkMouse()
{
	//setup mouse state
	ALLEGRO_MOUSE_STATE mouseState;
	al_get_mouse_state(&mouseState);

	Vector2D mouseLoc;

	if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mpMouseState.LEFT_DOWN = true;

		if (mouseState.x <= 0)
			mpMouseState.LEFT_LOC.setX(1);
		else if (mouseState.x >= dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getWidth())
			mpMouseState.LEFT_LOC.setX(dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getWidth() - 1);
		else
			mpMouseState.LEFT_LOC.setX(mouseState.x);

		if (mouseState.y <= 0)
			mpMouseState.LEFT_LOC.setY(1);
		else if (mouseState.y >= dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getHeight())
			mpMouseState.LEFT_LOC.setY(dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getHeight() - 1);
		else
			mpMouseState.LEFT_LOC.setY(mouseState.y);
		mouseLoc = mpMouseState.LEFT_LOC;
	}

	else if (al_mouse_button_down(&mouseState, 2)) //right mouse click
	{
		mpMouseState.RIGHT_DOWN = true;

		if (mouseState.x <= 0)
			mpMouseState.RIGHT_LOC.setX(1);
		else if (mouseState.x >= dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getWidth())
			mpMouseState.RIGHT_LOC.setX(dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getWidth() - 1);
		else
			mpMouseState.RIGHT_LOC.setX(mouseState.x);

		if (mouseState.y <= 0)
			mpMouseState.RIGHT_LOC.setY(1);
		else if (mouseState.y >= dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getHeight())
			mpMouseState.RIGHT_LOC.setY(dynamic_cast<Game*>(gpGame)->getGraphicsSystem()->getHeight() - 1);
		else
			mpMouseState.RIGHT_LOC.setY(mouseState.y);
		mouseLoc = mpMouseState.RIGHT_LOC;
	}

	//create mouse text
	std::stringstream mousePos;
	mousePos << mouseLoc.getX() << ":" << mouseLoc.getY();

	//write text at mouse position
	al_draw_text(gpGame->getFont(), al_map_rgb(255, 255, 255), mouseLoc.getX(), mouseLoc.getY(), ALLEGRO_ALIGN_CENTRE, mousePos.str().c_str());

	return 0;
}

int InputManager::fireMessages()
{
	if (mpKeyState.A_KEY_DOWN) 
	{
		GameMessage* pMessage = new SwitchToA_StarMessage(mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.A_KEY_DOWN = false;
		std::cout << "A key pressed!" << std::endl;
	}

	if (mpKeyState.B_KEY_DOWN) 
	{
		mpKeyState.B_KEY_DOWN = false;
		std::cout << "B key pressed!" << std::endl;
	}

	if (mpKeyState.C_KEY_DOWN) 
	{
		GameMessage* pMessage = new ToggleDrawVisitedMessage(mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.C_KEY_DOWN = false;
		std::cout << "C key pressed!" << std::endl;
	}

	if (mpKeyState.D_KEY_DOWN) 
	{
		GameMessage* pMessage = new SwitchToDijkstraMessage(mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.D_KEY_DOWN = false;
		std::cout << "D key pressed!" << std::endl;
	}

	if (mpKeyState.W_KEY_DOWN) 
	{
		mpKeyState.W_KEY_DOWN = false;
		std::cout << "W key pressed!" << std::endl;
	}

	if (mpKeyState.ESC_KEY_DOWN) 
	{
		mpKeyState.ESC_KEY_DOWN = false;
		std::cout << "Esc key pressed!" << std::endl;
	}

	if (mpKeyState.PLUS_KEY_DOWN) 
	{
		mpKeyState.PLUS_KEY_DOWN = false;
		std::cout << "+ key pressed!" << std::endl;
	}

	if (mpKeyState.MINUS_KEY_DOWN) 
	{
		mpKeyState.MINUS_KEY_DOWN = false;
		std::cout << "- key pressed!" << std::endl;
	}

	if (mpKeyState.S_KEY_DOWN) 
	{
		mpKeyState.S_KEY_DOWN = false;
		std::cout << "S key pressed!" << std::endl;
	}

	if (mpKeyState.F_KEY_DOWN) 
	{
		GameMessage* pMessage = new SwitchToDepthFirstMessage(mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.F_KEY_DOWN = false;
		std::cout << "F key pressed!" << std::endl;
	}

	if (mpKeyState.T_KEY_DOWN) 
	{
		mpKeyState.T_KEY_DOWN = false;
		std::cout << "T key pressed!" << std::endl;
	}

	if (mpKeyState.Y_KEY_DOWN) 
	{
		mpKeyState.Y_KEY_DOWN = false;
		std::cout << "Y key pressed!" << std::endl;
	}

	if (mpMouseState.LEFT_DOWN) 
	{
		if (mpMouseState.LEFT_LOC.getX() != mpMouseState.RIGHT_LOC.getX() || mpMouseState.LEFT_LOC.getY() != mpMouseState.RIGHT_LOC.getY())
		{
			GameMessage* pMessage = new PathToMessage( mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC );
			dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		}
		mpMouseState.LEFT_DOWN = false;
		std::cout << "Left Mouse button pressed!" << std::endl;
	}

	if (mpMouseState.RIGHT_DOWN)
	{
		if (mpMouseState.LEFT_LOC.getX() != mpMouseState.RIGHT_LOC.getX() || mpMouseState.LEFT_LOC.getY() != mpMouseState.RIGHT_LOC.getY())
		{
			GameMessage* pMessage = new PathToMessage(mpMouseState.LEFT_LOC, mpMouseState.RIGHT_LOC);
			dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		}
		mpMouseState.RIGHT_DOWN = false;
		std::cout << "Right Mouse button pressed!" << std::endl;
	}

	return 0;
}




