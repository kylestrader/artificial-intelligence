#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class SwitchToDepthFirstMessage :public GameMessage
{
public:
	SwitchToDepthFirstMessage(Vector2D to, Vector2D from): mTo(to), mFrom(from), GameMessage(SWITCH_DFS_MESSAGE){};
	~SwitchToDepthFirstMessage(){};

	void process();

private:
	Vector2D mTo;
	Vector2D mFrom;
};