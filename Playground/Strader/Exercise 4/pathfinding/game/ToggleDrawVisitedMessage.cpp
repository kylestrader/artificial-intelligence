#include "ToggleDrawVisitedMessage.h"
#include "Game.h"
#include "GameApp.h"
#include "GridGraph.h"
#include "GridPathfinder.h"
#include "PathToMessage.h"
#include "GameMessageManager.h"

void ToggleDrawVisitedMessage::process()
{
	if (dynamic_cast<GameApp*>(gpGame)->getPathfinder()->getShouldDrawVisited())
	{
		dynamic_cast<GameApp*>(gpGame)->getPathfinder()->setShouldDrawVisited(false);
	}
	else
		dynamic_cast<GameApp*>(gpGame)->getPathfinder()->setShouldDrawVisited(true);

	if (mTo.getX() != mFrom.getX() || mTo.getY() != mFrom.getY())
	{
		GameMessage* pMessage = new PathToMessage(mTo, mFrom);
		dynamic_cast<GameApp*>(gpGame)->getMessageManager()->addMessage(pMessage, 0);
		std::cout << "Re-evaluated Path Generation" << std::endl;
	}
}