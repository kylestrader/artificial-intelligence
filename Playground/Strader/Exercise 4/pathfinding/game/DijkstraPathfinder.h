#pragma once

#include "GridPathfinder.h"
#include <vector>
#include <map>


class Path;
class Graph;
class GraphicsBuffer;
class Grid;

using namespace std;

class DijkstraPathfinder :public GridPathfinder
{
public:
	DijkstraPathfinder(Graph* pGraph);
	~DijkstraPathfinder();

	Path* findPath(Node* pFrom, Node* pTo);//make sure to delete the path when you are done!

private:
	Node* findClosestNodeTo(Node* point, map <int, Node*>* nodes);
};