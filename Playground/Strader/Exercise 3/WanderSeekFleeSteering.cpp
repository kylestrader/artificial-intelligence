#include "WanderSeekFleeSteering.h"

WanderSeekFleeSteering::WanderSeekFleeSteering(KinematicUnit* pMover, KinematicUnit* pTarget, bool shouldFlee)
:mpMover(pMover)
, mpTarget(pTarget)
{
	mApplyDirectly = false;
	mShouldFlee = shouldFlee;
	mpWander = new DynamicWanderSteering(mpMover);
	//mpWander = new GroupAlignmentSteering(mpMover);
	//mpWander = new GroupFlockSteering(mpMover);
	mpSeek = new DynamicSeekSteering(mpMover, mpTarget, mShouldFlee);
}
WanderSeekFleeSteering::~WanderSeekFleeSteering()
{
	delete mpWander;
	delete mpSeek;
}

void WanderSeekFleeSteering::setTarget(KinematicUnit* pTarget)
{
	mpTarget = pTarget; 
}

Steering* WanderSeekFleeSteering::getSteering()
{
	Vector2D pos = mpMover->getPosition();
	Vector2D targetPos = mpTarget->getPosition();
	float distFromTarget = sqrt(pow(pos.getX() - targetPos.getX(), 2) + pow(pos.getY() - targetPos.getY(), 2));
	float targetRadius = mpTarget->getVisRadius();

	//unit should be wandering
	if (distFromTarget > targetRadius)
	{
		//KinematicWanderSteering mpWander = KinematicWanderSteering(mpMover);
		mLinear = mpWander->getSteering()->getLinear();
		mAngular = mpWander->getSteering()->getAngular();
	}

	//unit should be seeking
	else 
	{
		//DynamicSeekSteering mpSeek = DynamicSeekSteering(mpMover, mpTarget, mShouldFlee);
		mLinear = mpSeek->getSteering()->getLinear();
		mAngular = mpSeek->getSteering()->getAngular();
	}

	return this;
}

bool WanderSeekFleeSteering::isColliding(WallSolid* tmpWall)
{
	Vector2D tmpLinear = mLinear;
	tmpLinear.normalize();
	tmpLinear *= 5;
	Vector2D nextStep = mpMover->getPosition() + tmpLinear;
	if (tmpWall->isInside(nextStep))
	{
		return true;
	}
	return false;
}

