#pragma once

#include "GameMessage.h"
#include "FlockGUI.h"

class Vector2D;

class AddDecrementFlockGuiMessage : public GameMessage
{
public:
	AddDecrementFlockGuiMessage();
	~AddDecrementFlockGuiMessage();

	void process();

private:
	GUI_BehaviorSelection mSelection;
};