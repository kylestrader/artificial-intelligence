#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "CircleSolidManager.h"
#include "Game.h"

class CircleAvoidanceSteering :public Steering
{
public:
	CircleAvoidanceSteering(KinematicUnit* pMover);
	virtual ~CircleAvoidanceSteering(){};

	virtual Steering* getSteering();

	float distance(Vector2D vec1, Vector2D vec2);
	Vector2D CircleAvoidanceSteering::findClosestPointToCircle(CircleSolid* circle, Vector2D unitPos);

private:
	KinematicUnit* mpMover;
	const float TARGET_VIS_DIST = 200;

};