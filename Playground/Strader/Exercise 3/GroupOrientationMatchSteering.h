#pragma once

#include "steering.h"
#include "Game.h"
#include "UnitManager.h"
#include <vector>

class KinematicUnit;

class GroupOrientationMatchSteering : public Steering
{
public:
	GroupOrientationMatchSteering(KinematicUnit* pMover);
	virtual ~GroupOrientationMatchSteering(){};

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;
	const float TARGET_VIS_DIST = 300;
};