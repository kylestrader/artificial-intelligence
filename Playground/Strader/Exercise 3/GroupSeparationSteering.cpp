#include "GroupSeparationSteering.h"

GroupSeparationSteering::GroupSeparationSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
	mApplyDirectly = false;
}

Steering* GroupSeparationSteering::getSteering()
{
	Vector2D direction;
	Vector2D acceleration;
	float dist;
	float strength;

	std::vector <KinematicUnit*> neighbors = mpMover->getNeighbors();
	int neighborCount = neighbors.size();

	if (neighborCount > 0)
	{
		for each (KinematicUnit* unit in neighbors)
		{
			direction = mpMover->getPosition() - unit->getPosition();
			dist = direction.getLength();
			if (dist < TARGET_VIS_DIST)
			{
				strength = mpMover->getMaxAcceleration() * (TARGET_VIS_DIST - dist) / TARGET_VIS_DIST;
				if (direction.getX() != 0 && direction.getY() != 0)
					direction.normalize();
				acceleration += (direction * strength);
			}
		}
	}

	mLinear = acceleration;

	return this;
}