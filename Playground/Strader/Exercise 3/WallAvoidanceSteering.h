#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "Game.h"

class WallAvoidanceSteering :public Steering
{
public:
	WallAvoidanceSteering(KinematicUnit* pMover);
	virtual ~WallAvoidanceSteering(){};

	virtual Steering* getSteering();

	Vector2D findClosestPointToWall(WallSolid* wall, Vector2D unitPos);

private:
	KinematicUnit* mpMover;

	const float TARGET_VIS_DIST = 100;

};