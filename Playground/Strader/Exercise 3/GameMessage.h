#pragma once

#include "Trackable.h"

enum MessageType
{
	INVALID_MESSAGE_TYPE = -1,
	PLAYER_MOVETO_MESSAGE = 0,
	EXIT_GAME_MESSAGE = 1,
	ADD_DYNAMIC_ARRIVE_AI = 2,
	ADD_DYNAMIC_SEEK_AI = 3,
	DELETE_RANDOM_UNIT_MESSAGE = 4,
	ADD_WANDER_SEEK_AI = 5,
	ADD_WANDER_FLEE_AI = 6,
	ADD_FLOCK_BOIDS = 7,
	FLOCK_GUI_CHANGE_SELECTION = 8,
	INCREMENT_FLOCK_GUI = 9,
	DECREMENT_FLOCK_GUI = 10,
	ADD_CIRCLE = 11,
	REMOVE_CIRCLE = 12
};

class GameMessage: public Trackable
{
public:
	friend class GameMessageManager;

	GameMessage( MessageType type  );
	~GameMessage();

	double getSubmittedTime() const { return mSubmittedTime; };
	double getScheduledTime() const { return mScheduledTime; };

protected:
	MessageType mType;
	double mSubmittedTime;
	double mScheduledTime;

private:
	virtual void process() = 0; 
};

