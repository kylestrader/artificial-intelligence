#include "CircleSolidManager.h"
#include "game.h"

CircleSolidManager::CircleSolidManager()
{
	mTotalCircleSolids = 0;
	mAccumCircleSolids = 0;
}

CircleSolidManager::~CircleSolidManager()
{
	circleSolidsMapCleanUp();
}

void CircleSolidManager::addCircleSolid(CircleSolid* circleSolid)
{
	std::string unitID = "wallSolid" + std::to_string(mAccumCircleSolids);

	mCircleSolidsMap[unitID] = circleSolid;
	mTotalCircleSolids++;
	mAccumCircleSolids++;
}

void CircleSolidManager::addCircleSolid(Vector2D pos, float rad)
{
	std::string circleSolidID = "circleSolid" + std::to_string(mAccumCircleSolids);

	CircleSolid* tmpCircleSolid = new CircleSolid(pos, rad);

	mCircleSolidsMap[circleSolidID] = tmpCircleSolid;
	mTotalCircleSolids++;
	mAccumCircleSolids++;
}

void CircleSolidManager::draw()
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end(); ++iter)
	{
		iter->second->drawRad();
	}
}

CircleSolid* CircleSolidManager::getCircleSolid(int index)
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (int i = 0; i < index; i++)
	{
		++iter;
	}

	if (iter->second != NULL)
	{
		return iter->second;
	}

	return NULL;
}

CircleSolid* CircleSolidManager::getCircleSolid(std::string id)
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end();)
	{
		if (iter->first == id){
			return iter->second;
		}

		else
		{
			++iter;
		}
	}

	return NULL;
}

void CircleSolidManager::removeCircleSolid(std::string id)
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end();)
	{
		if (iter->first == id)
		{
			delete iter->second;
			iter = mCircleSolidsMap.erase(iter);
			mTotalCircleSolids--;
		}

		else
		{
			++iter;
		}
	}
}

void CircleSolidManager::removeCircleSolid(CircleSolid* circleSolid)
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end();)
	{
		if (iter->second == circleSolid)
		{
			delete iter->second;
			iter = mCircleSolidsMap.erase(iter);
			mTotalCircleSolids--;
		}

		else
		{
			++iter;
		}
	}
}

void CircleSolidManager::removeRandomCircle()
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end();)
	{
		delete iter->second;
		iter = mCircleSolidsMap.erase(iter);
		mTotalCircleSolids--;
		return; 
	}

	return; //nothing was deleted, game should end!
}

void CircleSolidManager::circleSolidsMapCleanUp()
{
	std::map<std::string, CircleSolid*>::iterator iter;
	iter = mCircleSolidsMap.begin();

	for (iter = mCircleSolidsMap.begin(); iter != mCircleSolidsMap.end();)
	{
		delete iter->second;
		iter = mCircleSolidsMap.erase(iter);
		mTotalCircleSolids--;
	}
}