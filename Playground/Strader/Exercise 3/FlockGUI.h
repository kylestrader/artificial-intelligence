#pragma once

#include <string>

#include "KinematicUnit.h"

const std::string FILE_NAME = "settings.txt";

//enum type
enum GUI_BehaviorSelection
{
	WANDER = 0,
	ALIGN = 1,
	COHESION = 2,
	SEPARATION = 3,
	NONE = 4
};

class FlockGUI : public Trackable
{
public:
	FlockGUI();
	~FlockGUI(){ writeToFile(); };

	//utils
	void draw();
	void update();

	//getters
	float getWanderModifier(){ return mWanderModifier * MULTIPLIER; };
	float getAlignModifier(){ return mAlignModifier * MULTIPLIER; };
	float getCohesionModifier(){ return mCohesionModifier * MULTIPLIER; };
	float getSeparationModifier(){ return mSeparationModifier * MULTIPLIER; };
	GUI_BehaviorSelection getSelection(){ return mSelection; };

	//setters
	void setSelection(GUI_BehaviorSelection selection){ mSelection = selection; }

	//increment/decrement
	void incrementSelected();
	void decrementSelected();

	//file operations
	void readFromFile();
	void writeToFile();

private:

	ALLEGRO_COLOR mSelectedColor;
	ALLEGRO_COLOR mNotSelectedColor;
	ALLEGRO_COLOR mWanderColor;
	ALLEGRO_COLOR mAlignColor;
	ALLEGRO_COLOR mCohesionColor;
	ALLEGRO_COLOR mSeparationColor;

	const float MULTIPLIER = 10;
	const double INCREMENT = 0.1;
	float mWanderModifier;
	float mAlignModifier;
	float mCohesionModifier;
	float mSeparationModifier;
	GUI_BehaviorSelection mSelection;
};