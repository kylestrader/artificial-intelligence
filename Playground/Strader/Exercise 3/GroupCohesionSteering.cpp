#include "GroupCohesionSteering.h"

GroupCohesionSteering::GroupCohesionSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
	mApplyDirectly = false;
}

Steering* GroupCohesionSteering::getSteering()
{
	Vector2D cohesion = Vector2D(0.0f, 0.0f);
	std::vector <KinematicUnit*> neighbors = mpMover->getNeighbors();
	int neighborCount = neighbors.size();

	if (neighborCount > 0)
	{
		for each (KinematicUnit* unit in neighbors)
		{
			if (Vector2D(mpMover->getPosition() - unit->getPosition()).getLength() < mpMover->getVisRadius())
				cohesion += unit->getPosition();
		}
		cohesion /= neighborCount;
		mLinear = cohesion - mpMover->getPosition();
	}

	return this;
}