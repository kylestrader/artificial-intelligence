#pragma once

#include "CircleSolid.h"
#include <map>

class CircleSolidManager
{
public:
	//constructors/destructors
	CircleSolidManager();
	~CircleSolidManager();

	//Add Circle
	void addCircleSolid(CircleSolid* wallSolid);
	void addCircleSolid(Vector2D pos, float rad);

	//get Circle
	CircleSolid* getCircleSolid(std::string id);
	CircleSolid* getCircleSolid(int index);
	int getNumCirclesSolids(){ return mCircleSolidsMap.size(); };

	//remove Circle
	void removeCircleSolid(std::string id);
	void removeCircleSolid(CircleSolid* circle);
	void removeRandomCircle();

	//utils
	void draw();
	void circleSolidsMapCleanUp();

private:
	int mTotalCircleSolids;
	int mAccumCircleSolids;
	std::map <std::string, CircleSolid*> mCircleSolidsMap;

};