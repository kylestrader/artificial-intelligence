#include "Game.h"
#include "GameMessageManager.h"
#include "UnitManager.h"
#include "SpriteManager.h"
#include "AddCreateFlockBoidsMessage.h"

AddCreateFlockBoidsMessage::AddCreateFlockBoidsMessage(const Vector2D& origin, int numBoids) :GameMessage(ADD_FLOCK_BOIDS){
	mPos = origin;
	mNumBoids = numBoids;
}

void AddCreateFlockBoidsMessage::process()
{
	Vector2D pos2(mPos.getX() + 100, mPos.getY());
	Vector2D vel2(0.0f, 0.0f);
	for (int i = 0; i < mNumBoids; i++)
	{
		gpGame->getUnitManager()->addUnit(gpGame->getSpriteManager()->getSprite(gpGame->getEnemyIconBufferId()), pos2, 1, vel2, 0.0f, "", FLOCK_BOIDS, gpGame->getUnitManager()->getUnit(PLAYER_ID), 180.0f, 100.0f);
	}
}