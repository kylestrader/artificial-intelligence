#include "WallAvoidanceSteering.h"
#include "WallSolidManager.h"

WallAvoidanceSteering::WallAvoidanceSteering(KinematicUnit *pMover)
	:mpMover(pMover)
{
}

Steering* WallAvoidanceSteering::getSteering()
{
	Vector2D tmpVel = Vector2D(0, 0);
	int numWalls = gpGame->getWallSolidManager()->getNumWallsSolids();
	WallSolid* wall;

	for (int i = 0; i < numWalls; i++)
	{
		wall = gpGame->getWallSolidManager()->getWallSolid(i);
		Vector2D closestPt = findClosestPointToWall(wall, mpMover->getPosition());
		Vector2D target = mpMover->getPosition() - closestPt;
		float dist = target.getLength();
		if (dist < TARGET_VIS_DIST)
		{
			float percentMod = mpMover->getMaxAcceleration() * (TARGET_VIS_DIST - dist) / TARGET_VIS_DIST;
			target.normalize();
			tmpVel += target * percentMod;
		}
	}

	mLinear = tmpVel;
	return this;
}

Vector2D WallAvoidanceSteering::findClosestPointToWall(WallSolid* wall, Vector2D unitPos)
{
	Vector2D closestPt = Vector2D(0, 0);
	Vector2D wallVec = wall->getVector();
	Vector2D wallPos = wall->getPosition();
	Vector2D wallSize = wall->getSize();

	wallVec.normalize();

	if (wallVec.getX() == -1)
	{
		closestPt = Vector2D(unitPos.getX(), wallPos.getY());
	}

	else if (wallVec.getX() == 1)
	{
		closestPt = Vector2D(unitPos.getX(), wallPos.getY() + 20);
	}

	else if (wallVec.getY() == 1)
	{
		closestPt = Vector2D(wallPos.getX(), unitPos.getY());
	}

	else if (wallVec.getY() == -1)
	{
		closestPt = Vector2D(wallPos.getX() + 20, unitPos.getY());
	}

	return closestPt;
}