#pragma once

#include "GameMessage.h"
#include "Vector2d.h"

class Vector2D;

class AddDynamicArriveAiMessage : public GameMessage
{
public:
	AddDynamicArriveAiMessage(const Vector2D& playerPos);
	~AddDynamicArriveAiMessage(){};

	void process();

private:
	Vector2D mPos;
};