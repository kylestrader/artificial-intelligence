#pragma once

#include "GameMessage.h"
#include "Vector2d.h"

class Vector2D;

class AddCreateCircleMessage : public GameMessage
{
public:
	AddCreateCircleMessage(const Vector2D& origin);
	~AddCreateCircleMessage();

	void process();

private:
	Vector2D mPos;
	int mNumBoids;
};