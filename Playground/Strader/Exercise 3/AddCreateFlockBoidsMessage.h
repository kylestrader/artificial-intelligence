#pragma once

#include "GameMessage.h"
#include "Vector2d.h"

class Vector2D;

class AddCreateFlockBoidsMessage : public GameMessage
{
public:
	AddCreateFlockBoidsMessage(const Vector2D& origin, int numBoids);
	~AddCreateFlockBoidsMessage();

	void process();

private:
	Vector2D mPos;
	int mNumBoids;
};