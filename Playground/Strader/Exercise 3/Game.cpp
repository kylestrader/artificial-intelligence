#include <stdio.h>
#include <assert.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>

#include <sstream>

#include "Game.h"
#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"
#include "GraphicsBufferManager.h"
#include "GameMessageManager.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Timer.h"
#include "KinematicUnit.h"
#include "PlayerMoveToMessage.h"
#include "ExitGameMessage.h"
#include "UnitManager.h"
#include "InputManager.h"
#include "WallSolid.h"
#include "WallSolidManager.h"
#include "CircleSolidManager.h"
#include "FlockGUI.h"

using namespace std;

Game* gpGame = NULL;

const int WIDTH = 1024;
const int HEIGHT = 768;

Game::Game()
	:mpGraphicsSystem(NULL)
	,mpGraphicsBufferManager(NULL)
	,mpSpriteManager(NULL)
	,mpLoopTimer(NULL)
	,mpMasterTimer(NULL)
	,mShouldExit(false)
	,mpFont(NULL)
	,mpSample(NULL)
	,mBackgroundBufferID(INVALID_ID)
{
}

Game::~Game()
{
	cleanup();
}

bool Game::init()
{
	mShouldExit = false;

	//create Timers
	mpLoopTimer = new Timer;
	mpMasterTimer = new Timer;

	//startup allegro
	if(!al_init()) 
	{
		fprintf(stderr, "failed to initialize allegro!\n");
		return false;
	}

	//create and init GraphicsSystem
	mpGraphicsSystem = new GraphicsSystem();
	bool goodGraphics = mpGraphicsSystem->init( WIDTH, HEIGHT );
	if(!goodGraphics) 
	{
		fprintf(stderr, "failed to initialize GraphicsSystem object!\n");
		return false;
	}

	mpGraphicsBufferManager = new GraphicsBufferManager();
	mpSpriteManager = new SpriteManager();
	mpUnitManager = new UnitManager();
	mpInputManager = new InputManager();
	mpFlockGUI = new FlockGUI();
	mpFlockGUI->readFromFile();

	//startup a lot of allegro stuff

	//load image loader addon
	if( !al_init_image_addon() )
	{
		fprintf(stderr, "image addon failed to load!\n");
		return false;
	}

	//install audio stuff
	if( !al_install_audio() )
	{
		fprintf(stderr, "failed to initialize sound!\n");
		return false;
	}

	if(!al_init_acodec_addon())
	{
		fprintf(stderr, "failed to initialize audio codecs!\n");
		return false;
	}
 
	if (!al_reserve_samples(1))
	{
		fprintf(stderr, "failed to reserve samples!\n");
		return false;
	}

	mpInputManager->initialize();

	//should be somewhere else!
	al_init_font_addon();
	if( !al_init_ttf_addon() )
	{
		printf( "ttf font addon not initted properly!\n" ); 
		return false;
	}

	//actually load the font
	mpFont = al_load_ttf_font( "cour.ttf", 20, 0 );
	if( mpFont == NULL )
	{
		printf( "ttf font file not loaded properly!\n" ); 
		return false;
	}

	//show the mouse
	if( !al_hide_mouse_cursor( mpGraphicsSystem->getDisplay() ) )
	{
		printf( "Mouse cursor not able to be hidden!\n" ); 
		return false;
	}

	if( !al_init_primitives_addon() )
	{
		printf( "Primitives addon not added!\n" ); 
		return false;
	}

	//load the sample
	mpSample = al_load_sample( "clapping.wav" );
	if (!mpSample)
	{
		printf( "Audio clip sample not loaded!\n" ); 
		return false;
	}

	mpMessageManager = new GameMessageManager();

	//load buffers
	mBackgroundBufferID = mpGraphicsBufferManager->loadBuffer("Space.png");
	mPlayerIconBufferID = mpGraphicsBufferManager->loadBuffer("Player.png");
	mEnemyIconBufferID = mpGraphicsBufferManager->loadBuffer("Minion_Blue.png");

	//GraphicsBuffer* tmpGraphicsBuffer = new GraphicsBuffer(WIDTH, HEIGHT, al_map_rgb(0, 0, 0));
	GraphicsBuffer* tmpGraphicsBuffer = new GraphicsBuffer(20, HEIGHT);
	mWallDiagBufferID = gpGame->getGraphicsBufferManager()->manageBuffer(tmpGraphicsBuffer, "wallDiagBuffer");

	tmpGraphicsBuffer = new GraphicsBuffer(WIDTH, 20);
	mWallHorizBufferID = gpGame->getGraphicsBufferManager()->manageBuffer(tmpGraphicsBuffer, "wallHorizBuffer");

	
	//setup sprites
	GraphicsBuffer* pBackGroundBuffer = mpGraphicsBufferManager->getBuffer( mBackgroundBufferID );
	if( pBackGroundBuffer != NULL )
	{
		mpSpriteManager->createAndManageSprite( BACKGROUND_SPRITE_ID, pBackGroundBuffer, 0, 0, pBackGroundBuffer->getWidth(), pBackGroundBuffer->getHeight() );
	}
	GraphicsBuffer* pPlayerBuffer = mpGraphicsBufferManager->getBuffer( mPlayerIconBufferID );
	Sprite* pArrowSprite = NULL;
	if( pPlayerBuffer != NULL )
	{
		pArrowSprite = mpSpriteManager->createAndManageSprite( PLAYER_ICON_SPRITE_ID, pPlayerBuffer, 0, 0, pPlayerBuffer->getWidth(), pPlayerBuffer->getHeight() );
	}
	GraphicsBuffer* pAIBuffer = mpGraphicsBufferManager->getBuffer( mEnemyIconBufferID );
	Sprite* pEnemyArrow = NULL;
	if( pAIBuffer != NULL )
	{
		pEnemyArrow = mpSpriteManager->createAndManageSprite( AI_ICON_SPRITE_ID, pAIBuffer, 0, 0, pAIBuffer->getWidth(), pAIBuffer->getHeight() );
	}
	GraphicsBuffer* pWallDiagBuffer = mpGraphicsBufferManager->getBuffer(mWallDiagBufferID);
	Sprite* pWallDiagSolid = NULL;
	if (pWallDiagBuffer != NULL)
	{
		pWallDiagSolid = mpSpriteManager->createAndManageSprite(WALL_SPRITE_DIAG_ID, pWallDiagBuffer, 0, 0, pWallDiagBuffer->getWidth(), pWallDiagBuffer->getHeight());
	}
	GraphicsBuffer* pWallHorizBuffer = mpGraphicsBufferManager->getBuffer(mWallHorizBufferID);
	Sprite* pWallHorizSolid = NULL;
	if (pWallHorizBuffer != NULL)
	{
		pWallHorizSolid = mpSpriteManager->createAndManageSprite(WALL_SPRITE_HORIZ_ID, pWallHorizBuffer, 0, 0, pWallHorizBuffer->getWidth(), pWallHorizBuffer->getHeight());
	}

	////setup units
	Vector2D pos( 100.0f, 100.0f );
	Vector2D vel( 0.0f, 0.0f );
	mpUnitManager->addUnit(pArrowSprite, pos, 1, vel, 0.0f, PLAYER_ID, 500.0f, 100.0f);

	////setup walls
	mWalls = new WallSolidManager();
	mWalls->addWallSolid(WALL_SPRITE_DIAG_ID, Vector2D(0.0f, 0.0f), RIGHT);
	mWalls->addWallSolid(WALL_SPRITE_DIAG_ID, Vector2D(WIDTH - 20.0f, 0.0f), LEFT);
	mWalls->addWallSolid(WALL_SPRITE_HORIZ_ID, Vector2D(0.0f, 0.0f), BOTTOM);
	mWalls->addWallSolid(WALL_SPRITE_HORIZ_ID, Vector2D(0.0f, HEIGHT - 20.0f), TOP);

	////setup circles
	mCircles = new CircleSolidManager();

	return true;
}

void Game::cleanup()
{
	delete mpFlockGUI;
	mpFlockGUI = NULL;
	delete mCircles;
	mCircles = NULL;
	delete mWalls;
	mWalls = NULL;
	//delete units
	delete mpUnitManager;
	mpUnitManager = NULL;

	//delete Inputmanager
	delete mpInputManager;
	mpInputManager = NULL;

	//delete the timers
	delete mpLoopTimer;
	mpLoopTimer = NULL;
	delete mpMasterTimer;
	mpMasterTimer = NULL;

	//delete the graphics system
	delete mpGraphicsSystem;
	mpGraphicsSystem = NULL;

	delete mpGraphicsBufferManager;
	mpGraphicsBufferManager = NULL;
	delete mpSpriteManager;
	mpSpriteManager = NULL;
	delete mpMessageManager;
	mpMessageManager = NULL;

	al_destroy_sample(mpSample);
	mpSample = NULL;
	al_destroy_font(mpFont);
	mpFont = NULL;

	//shutdown components
	al_uninstall_audio();
	al_shutdown_image_addon();
	al_shutdown_font_addon();
	al_shutdown_ttf_addon();
	al_shutdown_primitives_addon();

}

void Game::beginLoop()
{
	mpLoopTimer->start();
}
	
void Game::processLoop()
{
	//update units
	mpUnitManager->update(LOOP_TARGET_TIME / 1000.0f);

	//update flock
	mpFlockGUI->update();
	
	//draw background
	Sprite* pBackgroundSprite = mpSpriteManager->getSprite( BACKGROUND_SPRITE_ID );
	pBackgroundSprite->draw( *(mpGraphicsSystem->getBackBuffer()), 0, 0 );

	//draw units
	mWalls->draw(*(mpGraphicsSystem->getBackBuffer()));
	mCircles->draw();
	mpUnitManager->draw(GRAPHICS_SYSTEM->getBackBuffer());
	mpFlockGUI->draw();

	mpMessageManager->processMessagesForThisframe();

	mpInputManager->update();



	mpGraphicsSystem->swap();
}

bool Game::endLoop()
{
	//mpMasterTimer->start();
	mpLoopTimer->sleepUntilElapsed( LOOP_TARGET_TIME );
	return mShouldExit;
}

float genRandomBinomial()
{
	return genRandomFloat() - genRandomFloat();
}

float genRandomFloat()
{
	float r = (float)rand()/(float)RAND_MAX;
	return r;
}