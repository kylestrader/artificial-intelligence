#include "DynamicWanderSteering.h"
#include "KinematicUnit.h"
#include "Game.h"

DynamicWanderSteering::DynamicWanderSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
	mApplyDirectly = false;
	mWanderOrientation = 0;
	mWanderOffset = 2;
	mWanderRadius = 10;
	mMaxAcceleration = 100;
}

DynamicWanderSteering::Steering* DynamicWanderSteering::getSteering()
{
	mWanderOrientation += genRandomBinomial() * MAX_WANDER_RATE;

	float targetorientation = mWanderOrientation + mpMover->getOrientation();

	Vector2D target = mpMover->getPosition() + (Vector2D(mWanderOffset * mpMover->getOrientationAsVector().getX(), mWanderOffset * mpMover->getOrientationAsVector().getY()));
	target += Vector2D(mWanderRadius * getOrientationAsVector(mWanderOrientation).getX(), mWanderRadius * getOrientationAsVector(mWanderOrientation).getY());

	Vector2D dir = target - mpMover->getPosition();

	mLinear = dir * mMaxAcceleration;
	mAngular = 0;

	return this;
}

Vector2D DynamicWanderSteering::getOrientationAsVector(float orientation)
{
	return Vector2D(sinf(orientation), cosf(orientation));
}