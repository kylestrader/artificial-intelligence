#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "Game.h"
#include "UnitManager.h"
#include <vector>
#include <map>

class GroupSeparationSteering :public Steering
{
public:
	GroupSeparationSteering(KinematicUnit* pMover);
	virtual ~GroupSeparationSteering(){};

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;

	const float TARGET_VIS_DIST = 100;

};