#include "Game.h"
#include "GameMessageManager.h"
#include "AddIncrementFlockGuiMessage.h"

AddIncrementFlockGuiMessage::AddIncrementFlockGuiMessage() :GameMessage(FLOCK_GUI_CHANGE_SELECTION){
}

void AddIncrementFlockGuiMessage::process()
{
	gpGame->getflockGUI()->incrementSelected();
}