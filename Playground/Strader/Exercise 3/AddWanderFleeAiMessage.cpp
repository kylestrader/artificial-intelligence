#include "Game.h"
#include "GameMessageManager.h"
#include "AddWanderFleeAiMessage.h"
#include "UnitManager.h"
#include "SpriteManager.h"

AddWanderFleeAiMessage::AddWanderFleeAiMessage(const Vector2D& playerPos) : GameMessage(ADD_WANDER_FLEE_AI){
	mPos = playerPos;
}

void AddWanderFleeAiMessage::process()
{
	Vector2D pos2(mPos.getX() + 100, mPos.getY());
	Vector2D vel2(0.0f, 0.0f);
	gpGame->getUnitManager()->addUnit(gpGame->getSpriteManager()->getSprite(gpGame->getEnemyIconBufferId()), pos2, 1, vel2, 0.0f, "", WANDER_FLEE, gpGame->getUnitManager()->getUnit(PLAYER_ID), 180.0f, 100.0f);
}