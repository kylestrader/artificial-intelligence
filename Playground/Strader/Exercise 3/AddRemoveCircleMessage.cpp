#include "Game.h"
#include "GameMessageManager.h"
#include "CircleSolidManager.h"
#include "AddRemoveCircleMessage.h"

AddRemoveCircleMessage::AddRemoveCircleMessage() :GameMessage(REMOVE_CIRCLE){
}

void AddRemoveCircleMessage::process()
{
	gpGame->getCircleSolidManager()->removeRandomCircle();
}