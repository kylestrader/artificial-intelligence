#include "Game.h"
#include "GameMessageManager.h"
#include "CircleSolidManager.h"
#include "AddCreateCircleMessage.h"

AddCreateCircleMessage::AddCreateCircleMessage(const Vector2D& origin) :GameMessage(ADD_CIRCLE){
	mPos = origin;
}

void AddCreateCircleMessage::process()
{
	gpGame->getCircleSolidManager()->addCircleSolid(mPos, 40);
}