#include "FlockGUI.h"
#include "game.h"
#include <iostream>
#include <fstream>

FlockGUI::FlockGUI()
{
	mNotSelectedColor = al_map_rgb(100, 100, 100);
	mSelectedColor = al_map_rgb(255, 255, 255);
	mWanderColor = mNotSelectedColor;
	mAlignColor = mNotSelectedColor;
	mCohesionColor = mNotSelectedColor;
	mSeparationColor = mNotSelectedColor;

	mWanderModifier = 0.0f;
	mAlignModifier = 0.0f;
	mCohesionModifier = 0.0f;
	mSeparationModifier = 0.0f;

	mSelection = WANDER;

	readFromFile();
}

void FlockGUI::incrementSelected()
{
	switch (mSelection)
	{
	case NONE:
		break;

	case WANDER:
		if (mWanderModifier < 10)
			mWanderModifier += (INCREMENT);
		break;

	case ALIGN:
		if (mAlignModifier < 10)
			mAlignModifier += (INCREMENT);
		break;

	case SEPARATION:
		if (mSeparationModifier < 10)
			mSeparationModifier += (INCREMENT);
		break;

	case COHESION:
		if (mCohesionModifier < 10)
			mCohesionModifier += (INCREMENT);
		break;
	}
}

void FlockGUI::decrementSelected()
{
	switch (mSelection)
	{
	case NONE:
		break;

	case WANDER:
		if (mWanderModifier > 0)
			mWanderModifier -= (INCREMENT);
		break;

	case ALIGN:
		if (mAlignModifier > 0)
			mAlignModifier -= (INCREMENT);
		break;

	case SEPARATION:
		if (mSeparationModifier > 0)
			mSeparationModifier -= (INCREMENT);
		break;

	case COHESION:
		if (mCohesionModifier > 0)
			mCohesionModifier -= (INCREMENT);
		break;
	}
}

void FlockGUI::draw()
{
	std::string tmp = "Wander Weight: " + std::to_string(mWanderModifier);
	al_draw_text(gpGame->getFont(), mWanderColor, 20, 20, 0, tmp.c_str());

	tmp = "Align Weight: " + std::to_string(mAlignModifier);
	al_draw_text(gpGame->getFont(), mAlignColor, 20, 40, 0, tmp.c_str());

	tmp = "Separation Weight: " + std::to_string(mSeparationModifier);
	al_draw_text(gpGame->getFont(), mSeparationColor, 20, 60, 0, tmp.c_str());

	tmp = "Cohesion Weight: " + std::to_string(mCohesionModifier);
	al_draw_text(gpGame->getFont(), mCohesionColor, 20, 80, 0, tmp.c_str());
}

void FlockGUI::update()
{
	mWanderColor = mNotSelectedColor;
	mAlignColor = mNotSelectedColor;
	mSeparationColor = mNotSelectedColor;
	mCohesionColor = mNotSelectedColor;

	switch (mSelection)
	{
	case NONE:
		break;

	case WANDER:
		mWanderColor = mSelectedColor;
		break;

	case ALIGN:
		mAlignColor = mSelectedColor;
		break;

	case SEPARATION:
		mSeparationColor = mSelectedColor;
		break;

	case COHESION:
		mCohesionColor = mSelectedColor;
		break;
	}
}

void FlockGUI::readFromFile()
{
	std::string line;
	std::ifstream fin(FILE_NAME);
	if (fin.is_open() && fin.peek() != std::ifstream::traits_type::eof())
	{
		getline(fin, line);
		mWanderModifier = std::stof(line.c_str());
		getline(fin, line);
		mAlignModifier = std::stof(line.c_str());
		getline(fin, line);
		mCohesionModifier = std::stof(line.c_str());
		getline(fin, line);
		mSeparationModifier = std::stof(line.c_str());
		fin.close();
	}
}

void FlockGUI::writeToFile()
{
	std::ofstream fout (FILE_NAME);
	fout << mWanderModifier << std::endl;
	fout << mAlignModifier << std::endl;
	fout << mCohesionModifier << std::endl;
	fout << mSeparationModifier << std::endl;
	fout.close();
}