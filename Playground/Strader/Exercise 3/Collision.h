#pragma once

#include "Vector2D.h"
#include "WallSolid.h"
#include "WallSolidManager.h"
#include "KinematicUnit.h"
#include "game.h"

const int MIN_DIST_TO_WALL = 20;
const int LOOK_AHEAD = 30;

bool checkWallCollision(KinematicUnit* const tmpUnit, float time);
bool checkWallAvoidance(KinematicUnit* const tmpUnit, float time);
Vector2D calculateBounceVector(KinematicUnit* const tmpUnit, WallSolid* wall, Vector2D velocity);
inline float dotProduct(const Vector2D& vec1, const Vector2D& vec2){ return (vec1.getX() * vec2.getX()) + (vec1.getY() * vec2.getY()); };
float dot(Vector2D vec1, Vector2D vec2);
Vector2D project(Vector2D onto, Vector2D from);