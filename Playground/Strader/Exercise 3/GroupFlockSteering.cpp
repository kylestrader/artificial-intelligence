#include "GroupFlockSteering.h"

GroupFlockSteering::GroupFlockSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
	mApplyDirectly = false;

	mpWander = new DynamicWanderSteering(mpMover);

	mpAlignment = new GroupAlignmentSteering(mpMover);
	mpCohesion = new GroupCohesionSteering(mpMover);
	mpSeparation = new GroupSeparationSteering(mpMover);
	mpWallAvoid = new WallAvoidanceSteering(mpMover);
	mpCircleAvoid = new CircleAvoidanceSteering(mpMover);
}

GroupFlockSteering::~GroupFlockSteering()
{
	delete mpWander;
	delete mpAlignment;
	delete mpCohesion;
	delete mpSeparation;
	delete mpWallAvoid;
	delete mpCircleAvoid;
}

Steering* GroupFlockSteering::getSteering()
{
	float wanderMod = gpGame->getflockGUI()->getWanderModifier();
	float alignmentMod = gpGame->getflockGUI()->getAlignModifier();
	float cohesionMod = gpGame->getflockGUI()->getCohesionModifier();
	float separationMod = gpGame->getflockGUI()->getSeparationModifier();

	Vector2D flockVel = mpWander->getSteering()->getLinear() * wanderMod;
	float flockOrientation = mpWander->getSteering()->getAngular() * wanderMod;

	//flockVel += mpWander->getSteering()->getLinear() * 1;
	//flockOrientation += mpWander->getSteering()->getAngular() * 1;

	flockVel += mpAlignment->getSteering()->getLinear() * alignmentMod;
	flockOrientation += mpAlignment->getSteering()->getAngular() * alignmentMod;

	flockVel += mpCohesion->getSteering()->getLinear() * cohesionMod;
	flockOrientation += mpCohesion->getSteering()->getAngular() * cohesionMod;

	flockVel += mpSeparation->getSteering()->getLinear() * separationMod;
	flockOrientation += mpSeparation->getSteering()->getAngular() * separationMod;

	flockVel += mpWallAvoid->getSteering()->getLinear() * 200;
	flockOrientation += mpWallAvoid->getSteering()->getAngular() * 200;

	flockVel += mpCircleAvoid->getSteering()->getLinear() * 200;
	flockOrientation += mpCircleAvoid->getSteering()->getAngular() * 200;


	//if (flockVel.getLength() > MAX_ACCEL)
	//{
	//THIS IS PREVENTING NICE FLOCKING!!!!!
		//flockVel.normalize();
		//lockVel *= mpMover->getMaxAcceleration();
	//}

	if (flockOrientation > MAX_ROT)
	{
		flockOrientation = MAX_ROT;
	}

	mLinear = flockVel;
	//mAngular = flockOrientation;

	return this;
}