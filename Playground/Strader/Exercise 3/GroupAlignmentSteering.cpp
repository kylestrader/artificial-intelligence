#include "GroupAlignmentSteering.h"

GroupAlignmentSteering::GroupAlignmentSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
	mApplyDirectly = false;
	mpVelocityMatch = new GroupVelocityMatchSteering(mpMover);
	mpOrientationMatch = new GroupOrientationMatchSteering(mpMover);
}

GroupAlignmentSteering::~GroupAlignmentSteering()
{
	delete mpVelocityMatch;
	delete mpOrientationMatch;
}

Steering* GroupAlignmentSteering::getSteering()
{
	mLinear = mpVelocityMatch->getSteering()->getLinear();
	mAngular = mpOrientationMatch->getSteering()->getAngular();
	return this;
}