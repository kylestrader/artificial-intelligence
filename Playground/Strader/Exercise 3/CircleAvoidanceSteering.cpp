#include "CircleAvoidanceSteering.h"

CircleAvoidanceSteering::CircleAvoidanceSteering(KinematicUnit *pMover)
	:mpMover(pMover)
{
}

Steering* CircleAvoidanceSteering::getSteering()
{
	Vector2D tmpVel = Vector2D(0, 0);
	int numCircles = gpGame->getCircleSolidManager()->getNumCirclesSolids();
	CircleSolid* circle;

	for (int i = 0; i < numCircles; i++)
	{
		circle = gpGame->getCircleSolidManager()->getCircleSolid(i);
		Vector2D closestPoint = findClosestPointToCircle(circle, mpMover->getPosition());
		Vector2D target = mpMover->getPosition() - closestPoint;
		float dist = target.getLength();

		if (dist < TARGET_VIS_DIST)
		{
			float percentMod = mpMover->getMaxAcceleration() * (TARGET_VIS_DIST - dist) / TARGET_VIS_DIST;
			target.normalize();
			tmpVel += target * percentMod;
		}
	}

	mLinear = tmpVel;
	return this;
}

float CircleAvoidanceSteering::distance(Vector2D vec1, Vector2D vec2)
{
	return sqrt(pow(vec1.getX() - vec2.getX(), 2) + pow(vec1.getY() - vec2.getY(), 2));
}

Vector2D CircleAvoidanceSteering::findClosestPointToCircle(CircleSolid* circle, Vector2D unitPos)
{
	Vector2D segment = circle->getPos() - unitPos;
	segment.normalize();
	segment *= circle->getRad();
	return circle->getPos() + segment;
}