#pragma once

#include "Vector2d.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

struct MouseState
{
	bool LEFT_DOWN;
	bool RIGHT_DOWN;
	Vector2D MOUSE_LOC;
};