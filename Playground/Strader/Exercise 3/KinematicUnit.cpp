#include "Game.h"
#include "Kinematic.h"
#include "KinematicUnit.h"
#include "Sprite.h"
#include "GraphicsSystem.h"
#include "Steering.h"
#include "KinematicSeekSteering.h"
#include "KinematicArriveSteering.h"
#include "KinematicWanderSteering.h"
#include "DynamicSeekSteering.h"
#include "DynamicArriveSteering.h"
#include "WanderSeekFleeSteering.h"
#include "GroupCohesionSteering.h"
#include "GroupAlignmentSteering.h"
#include "UnitManager.h"
#include "WallSolidManager.h"
#include "Collision.h"
#include "GroupFlockSteering.h"

using namespace std;

Steering gNullSteering( gZeroVector2D, 0.0f );

KinematicUnit::KinematicUnit(Sprite *pSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, float maxVelocity, float maxAcceleration)
:Kinematic( position, orientation, velocity, rotationVel )
,mpSprite(pSprite)
,mpCurrentSteering(NULL)
,mMaxVelocity(maxVelocity)
,mMaxAcceleration(maxAcceleration)
,mVisRadius(MAX_VIS_DIST)
{
	Id = "";
}

KinematicUnit::~KinematicUnit()
{
	delete mpCurrentSteering;
}

void KinematicUnit::draw( GraphicsBuffer* pBuffer )
{
	mpSprite->draw( *pBuffer, mPosition.getX(), mPosition.getY(), mOrientation );
}

void KinematicUnit::applyDirectly(Vector2D lin, float ang)
{
	//not stopped
	if (getVelocity().getLengthSquared() > MIN_VELOCITY_TO_TURN_SQUARED)
	{
		setVelocity(lin);
		setOrientation(ang);
	}

	//since we are applying the steering directly we don't want any rotational velocity
	setRotationalVelocity(0.0f);
}

void KinematicUnit::update(float time)
{
	//Collisions
	mNeighbors = gpGame->getUnitManager()->getNeighbors(this, MAX_VIS_DIST);

	//if (checkPlayerCollision(this))
	//{
		//applyDirectly(mVelocity, mOrientation);
		//setNewOrientation();
	//}

	if (checkWallAvoidance(this, time))
	{
		applyDirectly(mVelocity, mOrientation);
		setNewOrientation();
	}

	Steering* steering;
	if( mpCurrentSteering != NULL )
	{
		steering = mpCurrentSteering->getSteering();
	}
	else
		steering = &gNullSteering;

	if( steering->shouldApplyDirectly() )
	{
		applyDirectly(steering->getLinear(), steering->getAngular());
		steering->setAngular( 0.0f );
	}
	else
		setNewOrientation();

	//move the unit using current velocities
	Kinematic::update( time );
	//calculate new velocities
	calcNewVelocities( *steering, time, mMaxVelocity, 25.0f );
	//move to oposite side of screen if we are off
	GRAPHICS_SYSTEM->wrapCoordinates( mPosition );

	//set the orientation to match the direction of travel
	//setNewOrientation();
}

//private - deletes old Steering before setting
void KinematicUnit::setSteering( Steering* pSteering )
{
	delete mpCurrentSteering;
	
	mpCurrentSteering = pSteering;
}

void KinematicUnit::setSteering(WanderSeekFleeSteering* newSteering)
{
	delete mpCurrentSteering;

	mpCurrentSteering = newSteering;
}

void KinematicUnit::setNewOrientation()
{ 
	mOrientation = getOrientationFromVelocity( mOrientation, mVelocity ); 
}

void KinematicUnit::seek(const Vector2D &target)
{
	KinematicSeekSteering* pSeekSteering = new KinematicSeekSteering( this, target );
	setSteering( pSeekSteering );
}

void KinematicUnit::arrive(const Vector2D &target)
{
	KinematicArriveSteering* pArriveSteering = new KinematicArriveSteering( this, target );
	setSteering( pArriveSteering );
}

void KinematicUnit::wander()
{
	KinematicWanderSteering* pWanderSteering = new KinematicWanderSteering( this );
	setSteering( pWanderSteering );
}

void KinematicUnit::wanderSeek(KinematicUnit* pTarget)
{
	WanderSeekFleeSteering* pWanderSeekSteering = new WanderSeekFleeSteering(this, gpGame->getUnitManager()->getUnit(gpGame->getPlayerId()));
	setSteering(pWanderSeekSteering);
}

void KinematicUnit::wanderFlee(KinematicUnit* pTarget)
{
	WanderSeekFleeSteering* pWanderFleeSteering = new WanderSeekFleeSteering(this, gpGame->getUnitManager()->getUnit(gpGame->getPlayerId()), true);
	setSteering(pWanderFleeSteering);
}

void KinematicUnit::dynamicSeek( KinematicUnit* pTarget )
{
	DynamicSeekSteering* pDynamicSeekSteering = new DynamicSeekSteering(this, gpGame->getUnitManager()->getUnit(gpGame->getPlayerId()));
	setSteering( pDynamicSeekSteering );
}

void KinematicUnit::dynamicFlee( KinematicUnit* pTarget )
{
	DynamicSeekSteering* pDynamicSeekSteering = new DynamicSeekSteering(this, gpGame->getUnitManager()->getUnit(gpGame->getPlayerId()), true);
	setSteering( pDynamicSeekSteering );
}

void KinematicUnit::dynamicArrive( KinematicUnit* pTarget )
{
	DynamicArriveSteering* pDynamicArriveSteering = new DynamicArriveSteering(this, gpGame->getUnitManager()->getUnit(gpGame->getPlayerId()));
	setSteering( pDynamicArriveSteering );
}

void KinematicUnit::flock()
{
	GroupFlockSteering* flock = new GroupFlockSteering(this);
	setSteering( flock );
}

