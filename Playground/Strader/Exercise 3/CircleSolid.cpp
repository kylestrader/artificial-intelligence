#include "CircleSolid.h"
#include "game.h"
#include <allegro5/allegro_primitives.h>

CircleSolid::CircleSolid(Vector2D pos, float rad)
{
	mPos = pos + Vector2D(rad, rad);
	mRad = rad;
}

void CircleSolid::drawRad()
{
	al_draw_circle(mPos.getX(), mPos.getY(), mRad, al_map_rgb(0, 0, 0), 5);
}