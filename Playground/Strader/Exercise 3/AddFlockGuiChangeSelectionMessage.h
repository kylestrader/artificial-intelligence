#pragma once

#include "GameMessage.h"
#include "FlockGUI.h"

class Vector2D;

class AddFlockGuiChangeSelectionMessage : public GameMessage
{
public:
	AddFlockGuiChangeSelectionMessage(GUI_BehaviorSelection selection);
	~AddFlockGuiChangeSelectionMessage();

	void process();

private:
	GUI_BehaviorSelection mSelection;
};