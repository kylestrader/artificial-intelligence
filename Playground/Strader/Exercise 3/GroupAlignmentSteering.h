#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "UnitManager.h"
#include "GroupVelocityMatchSteering.h"
#include "GroupOrientationMatchSteering.h"
#include "game.h"
#include <map>
#include <vector>

class GroupAlignmentSteering :public Steering
{
public:
	GroupAlignmentSteering(KinematicUnit* pMover);
	virtual ~GroupAlignmentSteering();

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;
	GroupVelocityMatchSteering* mpVelocityMatch;
	GroupOrientationMatchSteering* mpOrientationMatch;
	const float TARGET_VIS_DIST = 300;
};