#include "Game.h"
#include "GameMessageManager.h"
#include "UnitManager.h"
#include "SpriteManager.h"
#include "AddFlockGuiChangeSelectionMessage.h"

AddFlockGuiChangeSelectionMessage::AddFlockGuiChangeSelectionMessage(GUI_BehaviorSelection selection) :GameMessage(FLOCK_GUI_CHANGE_SELECTION){
	mSelection = selection;
}

void AddFlockGuiChangeSelectionMessage::process()
{
	gpGame->getflockGUI()->setSelection(mSelection);
}