#include "GroupOrientationmatchSteering.h"

GroupOrientationMatchSteering::GroupOrientationMatchSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
}

Steering* GroupOrientationMatchSteering::getSteering()
{
	std::vector<KinematicUnit*> neighbors = mpMover->getNeighbors();

	if (neighbors.size() > 0)
	{
		float mOrientation = 0.0f;
		for each(KinematicUnit* unit in neighbors)
		{
			if (Vector2D(mpMover->getPosition() - unit->getPosition()).getLength() < mpMover->getVisRadius())
				mOrientation += unit->getOrientation();
		}

		mOrientation /= neighbors.size();
		mAngular = mOrientation;
	}

	return this;
}