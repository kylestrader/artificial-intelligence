#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "DynamicWanderSteering.h"
#include "GroupSeparationSteering.h"
#include "GroupCohesionSteering.h"
#include "GroupAlignmentSteering.h"
#include "WallAvoidanceSteering.h"
#include "CircleAvoidanceSteering.h"
#include "FlockGUI.h"

class GroupFlockSteering :public Steering
{
public:
	GroupFlockSteering(KinematicUnit* pMover);
	virtual ~GroupFlockSteering();

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;

	DynamicWanderSteering* mpWander;
	GroupSeparationSteering* mpSeparation;
	GroupCohesionSteering* mpCohesion;
	GroupAlignmentSteering* mpAlignment;
	WallAvoidanceSteering* mpWallAvoid;
	CircleAvoidanceSteering* mpCircleAvoid;

	const float MAX_ACCEL = 100.0f;
	const float MAX_ROT = 1.0f;
};