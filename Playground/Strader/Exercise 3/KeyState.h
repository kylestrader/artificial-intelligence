#pragma once

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

struct KeyState
{
	bool A_KEY_DOWN;
	bool B_KEY_DOWN;
	bool C_KEY_DOWN;
	bool S_KEY_DOWN;
	bool D_KEY_DOWN;
	bool F_KEY_DOWN;
	bool W_KEY_DOWN;
	bool MINUS_KEY_DOWN;
	bool PLUS_KEY_DOWN;
	bool ESC_KEY_DOWN;
	bool T_KEY_DOWN;
	bool Y_KEY_DOWN;
};