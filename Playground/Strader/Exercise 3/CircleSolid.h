#pragma once

#include "Trackable.h"
#include "Vector2D.h"

class CircleSolid : public Trackable
{
public:
	CircleSolid();
	CircleSolid(Vector2D pos, float rad);
	~CircleSolid(){};

	inline Vector2D getPos(){ return mPos; };
	inline float getRad(){ return mRad; };

	void drawRad();

private:
	Vector2D mPos;
	float mRad;

};