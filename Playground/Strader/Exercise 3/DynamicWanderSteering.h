#pragma once

#include "Steering.h"

class KinematicUnit;

const float MAX_WANDER_RATE = 0.75f;

class DynamicWanderSteering: public Steering
{
public:
	DynamicWanderSteering(KinematicUnit* pMover);
	virtual ~DynamicWanderSteering(){};

	virtual Steering* getSteering();

private:

	Vector2D getOrientationAsVector(float orientation);

	KinematicUnit* mpMover;
	int mWanderOffset;
	int mWanderRadius;

	float mWanderRate;
	float mWanderOrientation;
	float mMaxAcceleration;
};