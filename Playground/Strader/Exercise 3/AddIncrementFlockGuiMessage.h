#pragma once

#include "GameMessage.h"
#include "FlockGUI.h"

class Vector2D;

class AddIncrementFlockGuiMessage : public GameMessage
{
public:
	AddIncrementFlockGuiMessage();
	~AddIncrementFlockGuiMessage();

	void process();

private:
	GUI_BehaviorSelection mSelection;
};