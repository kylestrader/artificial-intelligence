#pragma once

#include "steering.h"
#include "Game.h"
#include "UnitManager.h"
#include <vector>

class KinematicUnit;

class GroupVelocityMatchSteering : public Steering
{
public:
	GroupVelocityMatchSteering(KinematicUnit* pMover);
	virtual ~GroupVelocityMatchSteering(){};

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;
	const float TARGET_VIS_DIST = 300;
};