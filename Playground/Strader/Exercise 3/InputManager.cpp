#include <stdlib.h>
#include <time.h>

#include "InputManager.h"
#include"Game.h"
#include "GameMessage.h"
#include "GameMessageManager.h"
#include "PlayerMoveToMessage.h"
#include "ExitGameMessage.h"
#include "AddDynamicArriveAiMessage.h"
#include "AddDynamicSeekAiMessage.h"
#include "DeleteRandomAiUnitMessage.h"
#include "AddWanderSeekAiMessage.h"
#include "AddWanderFleeAiMessage.h"
#include "UnitManager.h"
#include "GraphicsSystem.h"
#include "AddCreateFlockBoidsMessage.h"
#include "AddFlockGuiChangeSelectionMessage.h"
#include "AddIncrementFlockGuiMessage.h"
#include "AddDecrementFlockGuiMessage.h"
#include "AddCreateCircleMessage.h"
#include "AddRemoveCircleMessage.h"
#include "FlockGUI.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

InputManager::InputManager()
{
	mpKeyState.A_KEY_DOWN = false;
	mpKeyState.B_KEY_DOWN = false;
	mpKeyState.C_KEY_DOWN = false;
	mpKeyState.D_KEY_DOWN = false;
	mpKeyState.W_KEY_DOWN = false;
	mpKeyState.ESC_KEY_DOWN = false;
	mpKeyState.MINUS_KEY_DOWN = false;
	mpKeyState.PLUS_KEY_DOWN = false;
	mpKeyState.S_KEY_DOWN = false;
	mpKeyState.F_KEY_DOWN = false;
	mpKeyState.T_KEY_DOWN = false;
	mpKeyState.Y_KEY_DOWN = false;

	mpMouseState.LEFT_DOWN = false;
	mpMouseState.RIGHT_DOWN = false;
	mpMouseState.MOUSE_LOC.setX(0);
	mpMouseState.MOUSE_LOC.setY(0);
}

InputManager::~InputManager()
{
	al_destroy_event_queue(mpKeyboardEventQueue);
	al_uninstall_keyboard();
	al_uninstall_mouse();
}

int InputManager::initialize()
{
	srand(time(NULL));
	//install mouse
	if (!al_install_mouse())
	{
		printf("Mouse not installed!\n");
		return -1;
	}

	//install keyboard
	if (!al_install_keyboard())
	{
		printf("Keyboard not installed!\n");
		return -1;
	}

	//setup event queue for keyboard input
	mpKeyboardEventQueue = al_create_event_queue();
	al_register_event_source(mpKeyboardEventQueue, al_get_keyboard_event_source());

	return 0;
}

int InputManager::update()
{
	checkKeyboard();
	checkMouse(); 
	fireMessages();
	return 0;
}

int InputManager::checkKeyboard()
{
	//run through every keyboard event
	while (!al_event_queue_is_empty(mpKeyboardEventQueue))
	{
		al_get_next_event(mpKeyboardEventQueue, &mpEvent);
		if (mpEvent.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (mpEvent.keyboard.keycode)
			{
			case ALLEGRO_KEY_A: //A key pressed
				mpKeyState.A_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_B: //A key pressed
				mpKeyState.B_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_C: //C key pressed
				mpKeyState.C_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_D: //D key pressed
				mpKeyState.D_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_W: //W key pressed
				mpKeyState.W_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_ESCAPE: //Esc key pressed
				mpKeyState.ESC_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_EQUALS: //= key pressed
				mpKeyState.PLUS_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_MINUS: //- key pressed
				mpKeyState.MINUS_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_S: //S key pressed
				mpKeyState.S_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_F: //F key pressed
				mpKeyState.F_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_T: //S key pressed
				mpKeyState.T_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_Y: //F key pressed
				mpKeyState.Y_KEY_DOWN = true;
				break;

			default:
				break;
			}
		}
	}

	return 0;
}

int InputManager::checkMouse()
{
	//setup mouse state
	ALLEGRO_MOUSE_STATE mouseState;
	al_get_mouse_state(&mouseState);

	if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mpMouseState.LEFT_DOWN = true;
	}

	else if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mpMouseState.LEFT_DOWN = true;
	}

	mpMouseState.MOUSE_LOC.setX(mouseState.x);
	mpMouseState.MOUSE_LOC.setY(mouseState.y);

	//create mouse text
	std::stringstream mousePos;
	mousePos << mpMouseState.MOUSE_LOC.getX() << ":" << mpMouseState.MOUSE_LOC.getY();

	//write text at mouse position
	al_draw_text(gpGame->getFont(), al_map_rgb(255, 255, 255), mpMouseState.MOUSE_LOC.getX(), mpMouseState.MOUSE_LOC.getY(), ALLEGRO_ALIGN_CENTRE, mousePos.str().c_str());

	return 0;
}

int InputManager::fireMessages()
{
	if (mpKeyState.A_KEY_DOWN) //create dynamic arrive ai unit
	{
		GameMessage* pMessage = new AddFlockGuiChangeSelectionMessage(ALIGN);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.A_KEY_DOWN = false;
		std::cout << "A key pressed!" << std::endl;
	}

	if (mpKeyState.B_KEY_DOWN) //create flock ai units
	{
		Vector2D pos = Vector2D(gpGame->getGraphicsSystem()->getWidth() / 2, gpGame->getGraphicsSystem()->getHeight() / 2);
		GameMessage* pMessage = new AddCreateFlockBoidsMessage(pos, 5);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		//mpKeyState.B_KEY_DOWN = false;
		std::cout << "B key pressed!" << std::endl;
	}

	if (mpKeyState.C_KEY_DOWN) //create flock ai units
	{
		GameMessage* pMessage = new AddFlockGuiChangeSelectionMessage(COHESION);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.C_KEY_DOWN = false;
		std::cout << "C key pressed!" << std::endl;
	}

	if (mpKeyState.D_KEY_DOWN) //create delete random ai unit
	{
		GameMessage* pMessage = new DeleteRandomAiUnitMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.D_KEY_DOWN = false;
		std::cout << "D key pressed!" << std::endl;
	}

	if (mpKeyState.W_KEY_DOWN) //create flock ai units
	{
		GameMessage* pMessage = new AddFlockGuiChangeSelectionMessage(WANDER);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.W_KEY_DOWN = false;
		std::cout << "W key pressed!" << std::endl;
	}

	if (mpKeyState.ESC_KEY_DOWN) //exit game
	{
		GameMessage* pMessage = new ExitGameMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.ESC_KEY_DOWN = false;
		std::cout << "Esc key pressed!" << std::endl;
	}

	if (mpKeyState.PLUS_KEY_DOWN) //exit game
	{
		GameMessage* pMessage = new AddIncrementFlockGuiMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.PLUS_KEY_DOWN = false;
		std::cout << "+ key pressed!" << std::endl;
	}

	if (mpKeyState.MINUS_KEY_DOWN) //exit game
	{
		GameMessage* pMessage = new AddDecrementFlockGuiMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.MINUS_KEY_DOWN = false;
		std::cout << "- key pressed!" << std::endl;
	}

	if (mpKeyState.S_KEY_DOWN) //add dynamic seek ai unit
	{
		GameMessage* pMessage = new AddFlockGuiChangeSelectionMessage(SEPARATION);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.S_KEY_DOWN = false;
		std::cout << "S key pressed!" << std::endl;
	}

	if (mpKeyState.F_KEY_DOWN) //add dynamic seek ai unit
	{
		Vector2D pos = gpGame->getUnitManager()->getUnit(gpGame->getPlayerId())->getPosition();
		GameMessage* pMessage = new AddWanderFleeAiMessage(pos);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.F_KEY_DOWN = false;
		std::cout << "F key pressed!" << std::endl;
	}

	if (mpKeyState.T_KEY_DOWN) //add circle
	{
		GameMessage* pMessage = new AddCreateCircleMessage(Vector2D(rand() % 1024, rand() % 768));
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.T_KEY_DOWN = false;
		std::cout << "T key pressed!" << std::endl;
	}

	if (mpKeyState.Y_KEY_DOWN) //remove circle
	{
		GameMessage* pMessage = new AddRemoveCircleMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.Y_KEY_DOWN = false;
		std::cout << "Y key pressed!" << std::endl;
	}

	if (mpMouseState.LEFT_DOWN) //player move to
	{
		Vector2D pos(mpMouseState.MOUSE_LOC.getX(), mpMouseState.MOUSE_LOC.getY());
		GameMessage* pMessage = new PlayerMoveToMessage(pos);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpMouseState.LEFT_DOWN = false;
		std::cout << "Left Mouse button pressed!" << std::endl;
	}

	if (mpMouseState.RIGHT_DOWN)
	{
		mpMouseState.RIGHT_DOWN = false;
		std::cout << "Right Mouse button pressed!" << std::endl;
	}

	return 0;
}




