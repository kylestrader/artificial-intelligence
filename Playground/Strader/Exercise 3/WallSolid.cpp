#include "WallSolid.h"
#include "Sprite.h"
#include "GraphicsBuffer.h"
#include "GraphicsBuffermanager.h"
#include "SpriteManager.h"

WallSolid::WallSolid(const IDType ID, Vector2D pos, CollisionFace collisionFace)
{
	mPosition = pos;
	mpSprite = gpGame->getSpriteManager()->getSprite(ID);
	mSize = Vector2D(mpSprite->getSize());
	mCollisionFace = collisionFace;
	Vector2D wallVec = Vector2D(0.0f, 0.0f);

	switch (mCollisionFace)
	{
	case TOP:
		wallVec = Vector2D(mpSprite->getSize().getX(), 0.0f);
		break;
	case BOTTOM:
		wallVec = Vector2D(mpSprite->getSize().getX() * -1, 0.0f);
		break;
	case LEFT:
		wallVec = Vector2D(0.0f, mpSprite->getSize().getY());
		break;
	case RIGHT:
		wallVec = Vector2D(0.0f, mpSprite->getSize().getY() * -1);
		break;
	}
	
	mVector = wallVec;
}

WallSolid::~WallSolid()
{
	gpGame->getSpriteManager()->destroySprite(m_iSpriteID);
}

void WallSolid::draw(GraphicsBuffer& pBuffer)
{
	mpSprite->draw(pBuffer, mPosition.getX(), mPosition.getY());
}

bool WallSolid::isInside(Vector2D point)
{
	Vector2D tmpPoint = point;

	if ((point.getX() > mPosition.getX() && point.getX() < mPosition.getX() + mSize.getX()) &&
		(point.getY() > mPosition.getY() && point.getY() < mPosition.getY() + mSize.getY()))
	{
		return true;
	}
	return false;
}