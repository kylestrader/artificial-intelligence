#include "Collision.h"

bool checkWallCollision(KinematicUnit* const tmpUnit, float time)
{
	Vector2D tmpLinear = tmpUnit->getVelocity() * time;
	tmpLinear.normalize();
	tmpLinear *= 5;
	Vector2D nextStep = tmpUnit->getPosition() + tmpLinear;

	int numWalls = gpGame->getWallSolidManager()->getNumWallsSolids(); //Find the number of walls to check
	for (int i = 0; i < numWalls; i++)
	{
		if (gpGame->getWallSolidManager()->getWallSolid(i)->isInside(nextStep))
		{
			tmpUnit->setVelocity(calculateBounceVector(tmpUnit, gpGame->getWallSolidManager()->getWallSolid(i), tmpUnit->getVelocity() * time));
			return true;
		}
	}
	return false;
}

bool checkWallAvoidance(KinematicUnit* const tmpUnit, float time)
{
	Vector2D tmpLinear = tmpUnit->getVelocity();
	tmpLinear.normalize();
	tmpLinear *= LOOK_AHEAD;
	Vector2D nextStep = tmpUnit->getPosition() + tmpLinear;

	int numWalls = gpGame->getWallSolidManager()->getNumWallsSolids(); //Find the number of walls to check
	for (int i = 0; i < numWalls; i++)
	{
		WallSolid* tmpWall = gpGame->getWallSolidManager()->getWallSolid(i);
		if (tmpWall->isInside(nextStep))
		{
			Vector2D wallVec = tmpWall->getVector();
			return true;
		}
	}
	return false;
}

Vector2D calculateTargetPoint()
{
	return NULL;
}

Vector2D calculateBounceVector(KinematicUnit* const tmpUnit, WallSolid* wall, Vector2D velocity)
{
	Vector2D wallVec = wall->getVector();
	Vector2D bounceVector;
	wallVec.normalize();

	bounceVector = velocity;

	//Find the bounce
	float vecLength = dotProduct(bounceVector, wallVec);
	wallVec *= vecLength * 2.0f;

	bounceVector -= wallVec;
	bounceVector.normalize();

	//extend the normalized linear vector
	bounceVector *= tmpUnit->getMaxAcceleration();

	return bounceVector;
}

float dot(Vector2D vec1, Vector2D vec2)
{
	return float(vec1.getX() * vec2.getX() + vec1.getY() * vec2.getY());
}

Vector2D project(Vector2D onto, Vector2D from)
{
	return onto * (dot(from, onto) / onto.getLengthSquared());
}