#include "Game.h"
#include "GameMessageManager.h"
#include "AddDecrementFlockGuiMessage.h"

AddDecrementFlockGuiMessage::AddDecrementFlockGuiMessage() :GameMessage(FLOCK_GUI_CHANGE_SELECTION){
}

void AddDecrementFlockGuiMessage::process()
{
	gpGame->getflockGUI()->decrementSelected();
}