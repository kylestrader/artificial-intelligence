#pragma once

#include "GameMessage.h"
#include "Vector2d.h"

class Vector2D;

class AddRemoveCircleMessage : public GameMessage
{
public:
	AddRemoveCircleMessage();
	~AddRemoveCircleMessage();

	void process();
};