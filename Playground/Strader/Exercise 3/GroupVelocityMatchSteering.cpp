#include "GroupVelocityMatchSteering.h"

GroupVelocityMatchSteering::GroupVelocityMatchSteering(KinematicUnit* pMover)
{
	mpMover = pMover;
}

Steering* GroupVelocityMatchSteering::getSteering()
{
	std::vector<KinematicUnit*> neighbors = mpMover->getNeighbors();

	if (neighbors.size() > 0)
	{
		Vector2D velocity = 0.0f;
		for each(KinematicUnit* unit in neighbors)
		{
			if (Vector2D(mpMover->getPosition() - unit->getPosition()).getLength() < mpMover->getVisRadius())
				velocity += unit->getVelocity();
		}

		velocity /= neighbors.size();
		mLinear = velocity;
	}

	return this;
}