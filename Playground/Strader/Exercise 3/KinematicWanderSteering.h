#pragma once

#include "Steering.h"

class KinematicUnit;

const float MAX_WANDER_ROTATION = 1;

class KinematicWanderSteering: public Steering
{
public:
	KinematicWanderSteering( KinematicUnit* pMover );
	virtual~KinematicWanderSteering(){};

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;
};