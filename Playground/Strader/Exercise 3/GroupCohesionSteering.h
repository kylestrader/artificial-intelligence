#pragma once

#include "Steering.h"
#include "KinematicUnit.h"
#include "Game.h"
#include "UnitManager.h"
#include <vector>
#include <map>

class GroupCohesionSteering :public Steering
{
public:
	GroupCohesionSteering(KinematicUnit* pMover);
	virtual ~GroupCohesionSteering(){};

	virtual Steering* getSteering();

private:
	KinematicUnit* mpMover;
	std::vector<KinematicUnit*> mTargets;
	const float TARGET_VIS_DIST = 300;
	
};