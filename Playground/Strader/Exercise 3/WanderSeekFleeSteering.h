#pragma once

#include "Game.h"
#include "WallSolid.h"
#include "WallSolidManager.h"
#include "steering.h"
#include "KinematicWanderSteering.h"
#include "DynamicSeekSteering.h"
#include "DynamicWanderSteering.h"
#include "GroupAlignmentSteering.h"
#include "GroupCohesionSteering.h"
#include "GroupSeparationSteering.h"
#include "GroupVelocityMatchSteering.h"
#include "GroupFlockSteering.h"
#include "KinematicUnit.h"

class KinematicUnit;

class WanderSeekFleeSteering: public Steering
{
public:
	WanderSeekFleeSteering(KinematicUnit* pMover, KinematicUnit* pTarget, bool shouldFlee = false);
	virtual ~WanderSeekFleeSteering();

	void setTarget(KinematicUnit* pTarget);

	virtual Steering* getSteering();

	inline float dotProduct(const Vector2D& vec1, const Vector2D& vec2){ return (vec1.getX() * vec2.getX()) + (vec1.getY() * vec2.getY()); };

private:
	bool isColliding(WallSolid* tmpWall);

	KinematicUnit* mpTarget;
	KinematicUnit* mpMover;
	bool mShouldFlee;
	DynamicWanderSteering* mpWander;
	//GroupFlockSteering* mpWander;
	DynamicSeekSteering* mpSeek;
};