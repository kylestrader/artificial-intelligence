#pragma once

/*Editor - class to hold all application related info.

Dean Lawson
Champlain College
2011
*/

#include "Game.h"
#include <fstream>
#include <vector>
#include <string>
#include <Map.h>

//forward declarations
class Map;
class GraphicsBuffer;
class InputManager;
class GameMessageManager;
class Cursor;
class InputInterpretor;
class HUD;

class Editor: public Game
{
public:
	//constructor/destructor
	Editor();
	virtual ~Editor();

	//game loop
	virtual void beginLoop();
	virtual void processLoop();
	virtual bool endLoop();

	//utils
	virtual bool init();
	virtual void cleanup();
	void saveMaps();
	void loadEnviornment(std::string name);
	void switchNextMap();
	void switchPrevMap();
	bool doesMapExist(std::string name);
	bool doesMapExist(Map* map);
	void addBlock(Vector2D pos);
	void addPortal(Portal* portal);
	void clearBlock(Vector2D pos);
	void addMap(Map* map);
	void handlePortalClick(Vector2D pos);

	//getters
	inline Map* getMap() { return mpCurMap; };
	inline std::string getFileName(){ return OVERWORLD_NAME; };
	int getCurrentMapIndex(){ return mCurMapIndex; };
	int getNumMaps(){ return mMaps.size(); };
	Cursor* getCursor(){ return mpCursor; };
	HUD* getHUD(){ return mpHUD; };

private:
	//game loop
	void drawEditor();

	//utils
	int switchMap(std::string name);
	int switchMap(int index);
	void cleanupMaps();
	void loadMap(std::string name);
	Map* synthesizeNewMapData(std::string name);
	void refreshPortals();

	//private variables
	Map* mpCurMap;
	Cursor* mpCursor;
	HUD* mpHUD;
	std::map<std::string, Map*> mMaps;
	int mCurMapIndex;
};