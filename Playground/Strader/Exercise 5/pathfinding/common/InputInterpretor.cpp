#include "InputInterpretor.h"

InputInterpretor::InputInterpretor()
{
	mMousePos = Vector2D(0.0f, 0.0f);
	mPortalClickedState = false;
}

Vector2D InputInterpretor::transposeMousePos(Vector2D pos)
{
	Vector2D tmpPos = pos;
	if (tmpPos.getX() <= 0)
		tmpPos.setX(1);
	else if (tmpPos.getX() >= GRID_WIDTH)
		tmpPos.setX(GRID_WIDTH - 1);
	else

		if (tmpPos.getY() <= 0)
			tmpPos.setY(1);
		else if (tmpPos.getY() >= GRID_HEIGHT)
			tmpPos.setY(GRID_HEIGHT - 1);

	return tmpPos;
}