#include "LineSegment.h"

void LineSegment::draw()
{
	al_draw_line(mFrom.getX(), mFrom.getY(), mTo.getX(), mTo.getY(), al_map_rgb(0, 0, 0), 1);
}