#pragma once

#include <string>
#include <fstream>

#include "Game.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "Portal.h"
#include "Trackable.h"

const int HELPER_OBJECT_MARGIN = 4;
const int HELPER_OBJECT_BUFF = 4;

class Map: public Trackable
{
public:
	//constructor/destructor
	Map();
	~Map();

	//utils
	void initialize(int width, int height, int squareSize, std::string name);
	void draw(GraphicsBuffer& gb);
	inline void addPortal(Portal* portal) { mPortalDictionary.push_back(portal); };
	void removePortal(Portal* portal);
	void removePortal(int tileID);
	int checkForIdenticalPortals(Portal* portal);
	int checkNumMapPortalConnections(Portal* portal);
	void save(std::ofstream& theStream);
	std::vector<std::string> load(std::ifstream& theStream, std::string mapsDirectory);
	void refreshPortalConnections(std::map<std::string, Map*>& maps);
	inline std::vector < Portal* > getPortalDictionary() {return mPortalDictionary; };
	Portal* getInMapPortalConnection(Portal* portal);

	//getters
	inline Grid* getGrid() { return mpGrid; };
	inline GridVisualizer* getGridVisualizer() { return mpGridVisualizer; };
	inline std::string getMapName() { return mMapName; };
	Portal* getPortal(int tileID);
	std::vector<Portal*> getPortals(std::string toMapName, std::string linkerID);

	//setters
	void setSelectedTileIndex(int index){ selectedTileIndex = index; };

private:
	//utils
	void cleanupPortals();

	//private variables
	int selectedTileIndex;
	Grid* mpGrid;
	GridVisualizer* mpGridVisualizer;
	std::string mMapName;
	std::vector<Portal*> mPortalDictionary;

	ALLEGRO_FONT* mpFont;
};