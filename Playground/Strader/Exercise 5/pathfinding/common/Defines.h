#pragma once

#include <string>

typedef int IDType;
typedef std::string IDDescriptor;

const IDType INVALID_ID = -1;
const IDType HIGHEST_ID = 9999;

const float LOOP_TARGET_TIME = 33.3f;//how long should each frame of execution take? 30fps = 33.3ms/frame

const std::string RESOURCES_DIRECTORY = "../resources/";
const std::string MAPS_DIRECTORY = "../resources/maps/";
const std::string FONTS_DIRECTORY = "../resources/fonts/";
const std::string IMAGES_DIRECTORY = "../resources/images/";
