#pragma once

#include <string>

const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int GRID_WIDTH = 768;
const int GRID_HEIGHT = 768;

const std::string OVERWORLD_NAME = "OverWorld.txt";

const int GRID_SQUARE_SIZE_OVERWORLD = 16;
const int GRID_SQUARE_SIZE_DUNGEON = 32;