#include "Map.h"
#include <Allegro5/allegro_primitives.h>
#include <Allegro5/allegro_ttf.h>
#include <sstream>

Map::Map()
{
	mpFont = al_load_ttf_font(std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 12, 0);
	mpGrid = NULL;
	mpGridVisualizer = NULL;
	mMapName = "";
}

Map::~Map()
{
	al_destroy_font(mpFont);
	delete mpGrid;
	delete mpGridVisualizer;
	cleanupPortals();
}

void Map::initialize(int width, int height, int squareSize, std::string name)
{
	mpGrid = new Grid(width, height, squareSize);
	mpGridVisualizer = new GridVisualizer(mpGrid);
	mMapName = name;
}

void Map::cleanupPortals()
{
	for each (Portal* p in mPortalDictionary)
		delete p;
	mPortalDictionary.clear();
}

void Map::draw(GraphicsBuffer& gb)
{
	mpGridVisualizer->draw(gb);
	for each (Portal* p in mPortalDictionary)
	{
		std::string text = p->getLinkerID();
		Vector2D pos = mpGrid->getULCornerOfSquare(p->getTileID());
		std::stringstream ss;
		ss << text.c_str();



		if (p->getTileID() == selectedTileIndex)
		{
#pragma region DrawHelperArrow
			if (p->getFromMapName() == p->getToMapName())
			{
				Portal* toPortal = getInMapPortalConnection(p);

				if (toPortal != NULL)
				{
					int x1 = mpGrid->getULCornerOfSquare(p->getTileID()).getX() + (mpGrid->getSquareSize() / 2);
					int x2 = mpGrid->getULCornerOfSquare(toPortal->getTileID()).getX() + (mpGrid->getSquareSize() / 2);
					int y1 = mpGrid->getULCornerOfSquare(p->getTileID()).getY() + (mpGrid->getSquareSize() / 2);
					int y2 = mpGrid->getULCornerOfSquare(toPortal->getTileID()).getY() + (mpGrid->getSquareSize() / 2);
					al_draw_line(x1, y1, x2, y2, al_map_rgb(255, 0, 0), 2);
				}
			}
#pragma endregion
#pragma region DrawHelperObject
			std::string helperString = "To: " + p->getToMapName();

			//get dimensions of text
			int helperStringWidth = al_get_text_width(mpFont, helperString.c_str());
			int helperStringHeight = al_get_font_line_height(mpFont);
			int X_StringBuff = 0;
			int Y_StringBuff = 0;

			//dimensions of helper box
			int helperBoxWidth = helperStringWidth + HELPER_OBJECT_MARGIN * 2;
			int helperBoxHeight = helperStringHeight + HELPER_OBJECT_MARGIN * 2;
			int X_BoxBuff = 0;
			int Y_BoxBuff = 0;

			int L_Dist2Border = pos.getX();
			int R_Dist2Border = gpGame->getGraphicsSystem()->getHeight() - pos.getX();
			int U_Dist2Border = pos.getY();
			int D_Dist2Border = gpGame->getGraphicsSystem()->getHeight() - pos.getY();
			int array[4] = { L_Dist2Border, R_Dist2Border, U_Dist2Border, D_Dist2Border }; // 1) left, 2) right, 3) up, 4) down

			int max = 0;
			int maxIndex = 0;
			//find edge with the most space to draw
			for (int i = 0; i < 4; i++)
			{
				if (array[i]>max)
				{
					max = array[i];
					maxIndex = i;
				}
			}

			switch (maxIndex)
			{
			case 0: //left
				X_BoxBuff = -1 * (helperBoxWidth + HELPER_OBJECT_BUFF);
				X_StringBuff = X_BoxBuff + HELPER_OBJECT_MARGIN;
				Y_BoxBuff = ((mpGrid->getSquareSize() - helperBoxHeight) / 2);
				Y_StringBuff = ((mpGrid->getSquareSize() - helperStringHeight) / 2);
				break;
			case 1: //right
				X_BoxBuff = (mpGrid->getSquareSize() + HELPER_OBJECT_BUFF);
				X_StringBuff = X_BoxBuff + HELPER_OBJECT_MARGIN;
				Y_BoxBuff = ((mpGrid->getSquareSize() - helperBoxHeight) / 2);
				Y_StringBuff = ((mpGrid->getSquareSize() - helperStringHeight) / 2);
				break;
			case 2: //up
				X_BoxBuff = ((mpGrid->getSquareSize() / 2) - (helperBoxWidth / 2));
				X_StringBuff = X_BoxBuff + HELPER_OBJECT_MARGIN;
				Y_BoxBuff = (((-1 * helperBoxHeight) - HELPER_OBJECT_BUFF));
				Y_StringBuff = Y_BoxBuff + HELPER_OBJECT_MARGIN;
				break;
			case 3: //down
				X_BoxBuff = ((mpGrid->getSquareSize() / 2) - (helperBoxWidth / 2));
				X_StringBuff = X_BoxBuff + HELPER_OBJECT_MARGIN;
				Y_BoxBuff = (mpGrid->getSquareSize() + HELPER_OBJECT_BUFF);
				Y_StringBuff = Y_BoxBuff + HELPER_OBJECT_MARGIN;
				break;
			}

			int x1 = pos.getX() + X_BoxBuff;
			int y1 = pos.getY() + Y_BoxBuff;
			int x2 = x1 + helperBoxWidth;
			int y2 = y1 + helperBoxHeight;
			al_draw_filled_rectangle(
				x1,
				y1,
				x2,
				y2,
				al_map_rgb(100, 100, 100));

			al_draw_text(mpFont,
				al_map_rgb(255, 255, 255),
				pos.getX() + X_StringBuff,
				pos.getY() + Y_StringBuff,
				ALLEGRO_ALIGN_LEFT,
				helperString.c_str());
#pragma endregion
		}
#pragma region DrawLinkerID
		al_draw_text(mpFont, 
			al_map_rgb(0, 0, 0), 
			pos.getX() + mpGrid->getSquareSize() / 2, 
			pos.getY(),
			ALLEGRO_ALIGN_CENTER, 
			ss.str().c_str());
#pragma endregion 
	}
}

void Map::removePortal(Portal* portal)
{
	for (std::vector<Portal*>::iterator it = mPortalDictionary.begin(); it != mPortalDictionary.end();)
	{
		if (mPortalDictionary.at(it - mPortalDictionary.begin()) == portal)
		{
			delete mPortalDictionary.at(it - mPortalDictionary.begin());
			mPortalDictionary.erase(it);
			return;
		}
		else
			++it;
	}
}

void Map::removePortal(int tileIndex)
{
	for (std::vector<Portal*>::iterator it = mPortalDictionary.begin(); it != mPortalDictionary.end();)
	{
		if (mPortalDictionary.at(it - mPortalDictionary.begin())->getTileID() == tileIndex)
		{
			delete mPortalDictionary.at(it - mPortalDictionary.begin());
			mPortalDictionary.erase(it);
			return;
		}
		else
			++it;
	}
}

int Map::checkForIdenticalPortals(Portal* portal)
{
	int count = 0;
	for each (Portal* p in mPortalDictionary)
	{
		if (p->getToMapName() == portal->getToMapName() 
			&& p->getFromMapName() == portal->getFromMapName()
			&& p->getLinkerID() == portal->getLinkerID())
		{
			count++;
		}
	}

	return count;
}

int Map::checkNumMapPortalConnections(Portal* portal)
{
	int count = 0;
	for each (Portal* p in mPortalDictionary)
	{
		if (p->getToMapName() == mMapName)
		{
			if (p->getToMapName() == portal->getToMapName()
				&& p->getFromMapName() == portal->getFromMapName()
				&& p->getLinkerID() == portal->getLinkerID())
			{
				count++;
			}
		}
	}

	return count;
}

void Map::save(std::ofstream& theStream) 
{ 
	mpGrid->save(theStream); 
	int size = mPortalDictionary.size();
	for (int i = 0; i < size; i++)
	{
		Portal* p = mPortalDictionary.at(i);
		theStream << " ( tID: " << p->getTileID() << " " << p->getFromMapName() << " -> " << p->getToMapName() << " lID: " << p->getLinkerID() << " )";
	}
}

std::vector<std::string> Map::load(std::ifstream& theStream, std::string mapsDirectory)
{ 
	cleanupPortals();
	mpGrid->load(theStream); 
	delete mpGridVisualizer;
	mpGridVisualizer = new GridVisualizer(mpGrid); 

	std::vector<std::string> maps2Load;
	std::string tileIndex;
	std::string garb;
	int tID;
	std::string fromFileName;
	std::string toFileName;
	std::string lID;
	bool good = true;
	while (good)
	{
		theStream >> garb; //catch opening parentheses
		if (garb != "(")
		{
			good = false;
			continue;
		}
		theStream >> garb; //catch "tID:"
		theStream >> tileIndex;
		tID = atoi(tileIndex.c_str()); //convert tID to an int
		theStream >> fromFileName;
		theStream >> garb;
		theStream >> toFileName;
		theStream >> garb;
		theStream >> lID;
		theStream >> garb; //catch closing parentheses

		//see if file can be opened
		std::ifstream stream(std::string(mapsDirectory + toFileName).c_str());
		
		bool exists = false;
		for each (std::string s in maps2Load)
		{
			if (toFileName == s)
			{
				exists = true;
				break;
			}
		}

		if (!exists)
		{
			if (stream)
				maps2Load.push_back(std::string(toFileName)); //push to maps2load vector
			stream.close();
		}
		mPortalDictionary.push_back(new Portal(fromFileName, toFileName, lID, tID));

		good = !(theStream.peek() == -1);
	}

	return maps2Load;
}

Portal* Map::getPortal(int ID)
{
	for each (Portal* p in mPortalDictionary)
	{
		if (p->getTileID() == ID)
		{
			return p;
		}
	}

	return NULL;
}

Portal* Map::getInMapPortalConnection(Portal* portal)
{
	for each (Portal* p in mPortalDictionary)
	{
		if (p->getToMapName() == portal->getToMapName())
		{
			if (p != portal)
			{
				if (p->getLinkerID() == portal->getLinkerID())
					return p;
			}
		}
	}

	return NULL;
}

std::vector<Portal*> Map::getPortals(std::string toMapName, std::string linkerID)
{
	std::vector<Portal*> portals;
	for each(Portal* portal in mPortalDictionary)
	{
		if (portal->getToMapName() == toMapName && portal->getLinkerID() == linkerID)
			portals.push_back(portal);
	}
	return portals;
}

//only should call once all maps in the enviornment have been loaded
void Map::refreshPortalConnections(std::map<std::string, Map*>& maps)
{
	for each (Portal* portal in mPortalDictionary)
	{
		if (maps.find(portal->getToMapName()) != maps.end())
		{
			Map* tmpMap = maps[portal->getToMapName()];
			if (portal->getFromMapName() == portal->getToMapName())
			{
				if (tmpMap->getPortals(portal->getFromMapName(), portal->getLinkerID()).size() > 1)
				{
					mpGrid->setValueAtIndex(portal->getTileID(), 2);
				}
				else
				{
					mpGrid->setValueAtIndex(portal->getTileID(), 4);
				}
			}
			else
			{
				if (tmpMap->getPortals(portal->getFromMapName(), portal->getLinkerID()).size() > 0)
				{
					mpGrid->setValueAtIndex(portal->getTileID(), 2);
				}
				else
				{
					mpGrid->setValueAtIndex(portal->getTileID(), 4);
				}
			}
		}
		else
		{
			mpGrid->setValueAtIndex(portal->getTileID(), 3);
		}
	}
	mpGridVisualizer->setModified();
}