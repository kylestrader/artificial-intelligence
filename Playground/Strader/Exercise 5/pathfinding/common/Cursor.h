#pragma once

#include <Vector2D.h>
#include <Allegro5/allegro.h>
#include <Allegro5/allegro_primitives.h>

#include "GraphicsSystem.h"
#include "GraphicsBuffer.h"

class Cursor : public Trackable
{
public:
	Cursor(Vector2D pos, int gridSize);
	~Cursor(){};

	//setters
	inline void setTilePos(Vector2D pos){ mTilePos = pos; };
	inline void setCurGridSize(int size){ mCurGridSize = size; };
	inline void setColor(int r, int g, int b){ mColor = al_map_rgb(r, g, b); };
	inline void setColor(int r, int g, int b, int a){ mColor = al_map_rgba(r, g, b, a); };
	inline void setColor(ALLEGRO_COLOR color){ mColor = color; };
	inline void setWarning(bool warning){ mWarning = warning; };
	inline bool getWarning(){ return mWarning; };

	//getters
	inline Vector2D getPos(){ return mTilePos; };
	inline int getGridSize(){ return mCurGridSize; };
	inline ALLEGRO_COLOR getColor(){ return mColor; };

	//util
	void draw(GraphicsBuffer& gb);

private:
	Vector2D mTilePos;
	int mCurGridSize;
	ALLEGRO_COLOR mColor;
	bool mWarning;
};

