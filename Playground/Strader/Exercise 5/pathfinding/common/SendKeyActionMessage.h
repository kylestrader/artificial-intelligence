#pragma once

#include "GameMessage.h"
#include <string>

class SendKeyActionMessage :public GameMessage
{
public:
	SendKeyActionMessage(std::string action) : mAction(action), GameMessage(SEND_KEY_ACTION){};
	~SendKeyActionMessage(){};

	void process();

private:
	std::string mAction;
};