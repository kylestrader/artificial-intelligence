#include "SendMouseActionMessage.h"
#include "Game.h"

void SendMouseActionMessage::process()
{
	gpGame->getInputInterpretor()->interpretInput(mMousePos, mClick);
}