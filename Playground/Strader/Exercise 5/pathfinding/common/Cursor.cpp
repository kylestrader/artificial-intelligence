#include "Cursor.h"

#include "Game.h"
#include "Grid.h"

Cursor::Cursor(Vector2D tilePos, int gridSize)
{
	mTilePos = tilePos;
	mCurGridSize = gridSize;
	mColor = al_map_rgb(255, 0, 0);
	mWarning = false;
}

void Cursor::draw(GraphicsBuffer& gb)
{

	ALLEGRO_BITMAP* pOldTarget = GraphicsSystem::switchTargetBitmap(gb.getBitmap());

	//draw a red square to mark where the portal will be placed
	int x1 = mTilePos.getX() * mCurGridSize;
	int x2 = (mTilePos.getX() + 1) * mCurGridSize;
	int y1 = mTilePos.getY() * mCurGridSize;
	int y2 = (mTilePos.getY() + 1) * mCurGridSize;

	al_draw_rectangle(x1, y1, x2, y2, mColor, 2);

	if (mWarning)
	{
		al_draw_line(x1, y1, x2, y2, al_map_rgb(255, 0, 0), 2);
	}

	GraphicsSystem::switchTargetBitmap(pOldTarget);
}