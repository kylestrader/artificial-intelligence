#include "Game.h"
#include "GameApp.h"
#include "Kinematic.h"
#include "KinematicUnit.h"
#include "UnitManager.h"
#include "Sprite.h"
#include "GraphicsSystem.h"
#include "Steering.h"
#include "DynamicSeekSteering.h"
#include "SwitchDirectionColorMessage.h"
#include "SwitchMapMessage.h"
#include "UnitManager.h"
#include "GameMessageManager.h"
#include "Portal.h"
#include "Map.h"

using namespace std;

Steering gNullSteering( gZeroVector2D, 0.0f );

KinematicUnit::KinematicUnit(Sprite *pSprite, const Vector2D &position, float orientation, const Vector2D &velocity, float rotationVel, std::vector<Vector2D> path, std::string mapName, float maxVelocity, float maxAcceleration)
:Kinematic( position, orientation, velocity, rotationVel )
,mpSprite(pSprite)
,mpCurrentSteering(NULL)
,mMaxVelocity(maxVelocity)
,mMaxAcceleration(maxAcceleration)
,mVisRadius(MAX_VIS_DIST)
,mpPath(path)
,mMapName(mapName)
{
	Id = "";
	mCurPathIndex = 1;
	mReverse = false;
	mLastIndex = 0;
	prevTeleport = Vector2D(-1, -1);
}

KinematicUnit::~KinematicUnit()
{
	delete mpCurrentSteering;
}

void KinematicUnit::draw( GraphicsBuffer* pBuffer )
{
	if (mMapName == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
		mpSprite->draw( *pBuffer, mPosition.getX(), mPosition.getY(), mOrientation );
}

void KinematicUnit::applyDirectly(Vector2D lin, float ang)
{
	//not stopped
	if (getVelocity().getLengthSquared() > MIN_VELOCITY_TO_TURN_SQUARED)
	{
		setVelocity(lin);
		setOrientation(ang);
	}

	//since we are applying the steering directly we don't want any rotational velocity
	setRotationalVelocity(0.0f);
}

void KinematicUnit::update(float time)
{
	Steering* steering;
	if( mpCurrentSteering != NULL )
	{
		steering = mpCurrentSteering->getSteering();
	}
	else
		steering = &gNullSteering;

	if( steering->shouldApplyDirectly() )
	{
		applyDirectly(steering->getLinear(), steering->getAngular());
		steering->setAngular( 0.0f );
	}
	else
		setNewOrientation();

	//move the unit using current velocities
	Kinematic::update( time );
	//calculate new velocities
	calcNewVelocities( *steering, time, mMaxVelocity, 25.0f );

	//set the orientation to match the direction of travel
	setNewOrientation();

	Map* map = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
	std::vector<Portal*> portals = map->getPortalDictionary();

	int unitIndex = map->getGrid()->getSquareIndexFromPixelXY(mPosition.getX(), mPosition.getY());

	//if (unitIndex != mLastIndex)
	//{
	//	mLastIndex = unitIndex;
	//	if (!mCanTeleport)
	//	{
	//		mCanTeleport = true;
	//	}
	//}
	for each (Portal* p in portals)
	{
		if ((Vector2D::distance(prevTeleport, mPosition) > map->getGrid()->getSquareSize()) || 
			(prevTeleport.getX() == -1 && prevTeleport.getY() == -1))
		{
			prevTeleport = Vector2D(-1, -1);
			if (unitIndex == p->getTileID())
			{
				GameMessage* pMessage = new SwitchMapMessage(p->getToMapName());
				gpGame->getGameMessageManager()->addMessage(pMessage, 0);
				mMapName = p->getToMapName();
				Map* toMap = static_cast<GameApp*>(gpGame)->getMapByName(mMapName);
				std::vector<Portal*> connectedPortals = toMap->getPortals(p->getFromMapName(), p->getLinkerID());
				if (connectedPortals.size() <= 2)
				{
					//int peek = 1;
					//if (mReverse)
					//	peek = -1;
					//if (toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID()) != mTarget)
						//continue;

					if (p != connectedPortals.at(0))
						mPosition = toMap->getGrid()->getOriginOfSquare(connectedPortals.at(0)->getTileID());
					else
						mPosition = toMap->getGrid()->getOriginOfSquare(connectedPortals.at(1)->getTileID());

					prevTeleport = mPosition;

					if (mReverse)
						mCurPathIndex -= 1;
					else mCurPathIndex += 1;
					dynamicSeek(mpPath.at(mCurPathIndex));
				}
			}
		}
	}

	//update pathing
	if (Vector2D::distanceSq(mPosition, mTarget) < 10)
	{
		if (mReverse)
		{
			mCurPathIndex--;
			if (mCurPathIndex == -1)
			{
				mCurPathIndex = 1;
				mReverse = false;
				GameMessage* pMessage = new SwitchDirectionColorMessage(mReverse);
				gpGame->getGameMessageManager()->addMessage(pMessage, 0);
			}
			else
			{
				dynamicSeek(mpPath.at(mCurPathIndex));
			}
		}
		else
		{
			mCurPathIndex++;
			if (mCurPathIndex >= mpPath.size())
			{
				mCurPathIndex = mpPath.size() - 2;
				mReverse = true;
				GameMessage* pMessage = new SwitchDirectionColorMessage(mReverse);
				gpGame->getGameMessageManager()->addMessage(pMessage, 0);
			}
			else
			{
				dynamicSeek(mpPath.at(mCurPathIndex));
			}
		}
	}
}

//private - deletes old Steering before setting
void KinematicUnit::setSteering( Steering* pSteering )
{
	delete mpCurrentSteering;
	
	mpCurrentSteering = pSteering;
}

void KinematicUnit::setNewOrientation()
{ 
	mOrientation = getOrientationFromVelocity( mOrientation, mVelocity ); 
}

//void KinematicUnit::seek(const Vector2D &target)
//{
//	KinematicSeekSteering* pSeekSteering = new KinematicSeekSteering( this, target );
//	setSteering( pSeekSteering );
//}

void KinematicUnit::dynamicSeek( Vector2D pTarget )
{
	DynamicSeekSteering* pDynamicSeekSteering = new DynamicSeekSteering(this, pTarget);
	setSteering( pDynamicSeekSteering );
	setTarget(pTarget);
}

