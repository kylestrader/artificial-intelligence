#include "SwitchMapMessage.h"
#include "Game.h"
#include "GameApp.h"

void SwitchMapMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	pGame->switchMap(mName);
}