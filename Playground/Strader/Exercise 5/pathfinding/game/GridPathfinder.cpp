#include "GridPathfinder.h"
#include "Grid.h"
#include "GridGraph.h"
#include "GridVisualizer.h"
#include "Path.h"
#include "Game.h"
#include "Map.h"
#include "GameApp.h"
#include "GraphicsBuffer.h"
#include <LineSegmentPath.h>
#include <LineSegment.h>

GridPathfinder::GridPathfinder(GridGraph* pGraph)
:Pathfinder(pGraph)
,mTimeElapsed(0.0)
,mShouldDrawVisited(false)
{
#ifdef VISUALIZE_PATH
	mpVisualizer = NULL;
	mpPath = NULL;
	mReversed = false;
#endif
}

GridPathfinder::~GridPathfinder()
{
#ifdef VISUALIZE_PATH
	delete mpVisualizer;
#endif
}

#ifdef VISUALIZE_PATH
void GridPathfinder::drawVisualization(Grid* pGrid, GraphicsBuffer* pDest)
{
	LineSegmentPath lineSegmentPath;
	delete mpVisualizer;
	mpVisualizer = new GridVisualizer(pGrid);

	if (mpPath != NULL)
	{
		Vector2D to;
		Vector2D from;

		if (mShouldDrawVisited)
		{
			//creates visible visited nodes
			for (int i = 0; i < mVisitedNodes.size(); i++)
			{
				if (mVisitedNodes.at(i)->getMapName() == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
					mpVisualizer->addColor(mVisitedNodes.at(i)->getId(), mVisitedColor);
			}

			//draws visited nodes
			mpVisualizer->draw(*pDest);

			//remove all color entries
			mpVisualizer->removeAllEntriesOfColor(mVisitedColor);
		}

		if (mReversed)
			mPathColor = mStopColor;
		else
			mPathColor = mStartColor;

		//creates basic path with appropriate path color
		for (int i = 0; i < mpPath->getNumNodes(); i++)
		{
			if (mpPath->peekNode(i)->getMapName() == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
			{
				mpVisualizer->addColor(mpPath->peekNode(i)->getId(), mPathColor);

				if (i != mpPath->getNumNodes() - 1)
				{
					from = pGrid->getOriginOfSquare(mpPath->peekNode(i)->getId());
					to = pGrid->getOriginOfSquare(mpPath->peekNode(i + 1)->getId());
					if (mpPath->peekNode(i)->getMapName() == mpPath->peekNode(i + 1)->getMapName())
						lineSegmentPath.addSegment(LineSegment(from, to));
				}
			}
		}

		//draws basic path
		mpVisualizer->draw(*pDest);

		//remove all color entries
		mpVisualizer->removeAllEntriesOfColor(mPathColor);

		//add start and end node colors to map
		if (mpPath->peekNode(0)->getMapName() == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
			mpVisualizer->addColor(mpPath->peekNode(0)->getId(), mStartColor);
		if (mpPath->peekNode(mpPath->getNumNodes() - 1)->getMapName() == static_cast<GameApp*>(gpGame)->getCurMap()->getMapName())
			mpVisualizer->addColor(mpPath->peekNode(mpPath->getNumNodes() - 1)->getId(), mStopColor);

		//draw start and end nodes
		mpVisualizer->draw(*pDest);

		//draw accessorized line segment path
		lineSegmentPath.toggleAccessorize(true);
		lineSegmentPath.draw();
	}
}
#endif

float GridPathfinder::distanceBetweenNodes(Node* pointA, Node* pointB)
{
	Vector2D a = dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getULCornerOfSquare(pointA->getId());
	Vector2D b = dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getULCornerOfSquare(pointB->getId());

	return Vector2D::distance(a, b);
}