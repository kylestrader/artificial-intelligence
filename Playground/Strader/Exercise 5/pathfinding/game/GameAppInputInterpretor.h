#pragma once

#include "InputInterpretor.h"
#include "Node.h"
#include "Path.h"

class GameAppInputInterpretor : public InputInterpretor
{
public:
	//constructor/destructor
	GameAppInputInterpretor() :
		mpLastLeftClickedNode(NULL)
		, mpLastRightClickedNode(NULL)
		, mLeftClicked(false)
		, mRightClicked(false)
		, InputInterpretor(){};

	~GameAppInputInterpretor(){};

	//virtualized functions
	void interpretInput(std::string inputChar);
	void interpretInput(Vector2D mousePos, MouseClick click);

private:
	std::vector<Vector2D> translatePath(Path* path);
	void sendPathToMessage();
	Vector2D mMousePos;
	bool mPortalClickedState;

	Node* mpLastLeftClickedNode;
	Node* mpLastRightClickedNode;

	bool mLeftClicked;
	bool mRightClicked;
};