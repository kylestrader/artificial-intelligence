#include "UnitManager.h"
#include "Game.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

UnitManager::UnitManager()
{
	mpTotalUnits = 0;
	mpAccumUnits = 0;
}

UnitManager::~UnitManager()
{
	unitMapCleanUp();
}

std::string UnitManager::addUnit(KinematicUnit* unit)
{
	std::string unitID = "unit" + std::to_string(mpAccumUnits);

	mpUnitMap[unitID] = unit;
	mpTotalUnits++;
	mpAccumUnits++;

	return unitID;
}
//
//std::string UnitManager::addUnit(KinematicUnit* unit, ChaseType chaseType, KinematicUnit* target)
//{
//	std::string unitID = "unit" + std::to_string(mpAccumUnits);
//
//	//allows user to specify the behavior of the ai on creation
//	if (chaseType != NO_CHASE && target != NULL)
//	{
//		switch (chaseType)
//		{
//		case DYNAMIC_SEEK: //tell unit to use dynamic seek
//			unit->dynamicSeek(target);
//			break;
//		}
//	}
//
//	//add unit to map
//	mpUnitMap[unitID] = unit;
//	mpTotalUnits++;
//	mpAccumUnits++;
//
//	return unitID;
//}
//
//std::string UnitManager::addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id, float maxVelocity, float maxAcceleration)
//{
//	std::string unitID = "";
//
//	if (id == "") //if user does not specify a name then create a unique id
//	{
//		unitID = "unit" + std::to_string(mpAccumUnits);
//	}
//
//	else
//	{
//		unitID = id;
//	}
//
//	KinematicUnit* tmpUnit = new KinematicUnit(unitSprite, position, orientation, velocity, rotationVel, maxVelocity, maxAcceleration); //create the unit
//
//	mpUnitMap[unitID] = tmpUnit; //add the unit to the map
//	mpTotalUnits++;
//	mpAccumUnits++;
//
//	return unitID;
//}
//
//std::string UnitManager::addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id, ChaseType chaseType, KinematicUnit* target, float maxVelocity, float maxAcceleration)
//{
//	std::string unitID = "";
//
//	if (id == "") //if user does not specify a name then create a unique id
//	{
//		unitID = "unit" + std::to_string(mpAccumUnits);
//	}
//	
//	else
//	{
//		unitID = id;
//	}
//
//	KinematicUnit* tmpUnit = new KinematicUnit(unitSprite, position, orientation, velocity, rotationVel, maxVelocity, maxAcceleration); //create the unit
//
//	if (chaseType != NO_CHASE && target != NULL)
//	{
//		switch (chaseType)
//		{
//		case DYNAMIC_SEEK: //tell unit to use dynamic seek
//			tmpUnit->dynamicSeek(target);
//			break;
//		}
//	}
//
//	mpUnitMap[unitID] = tmpUnit;
//	mpTotalUnits++;
//	mpAccumUnits++;
//
//	return unitID;
//}

float distanceBetweenUnits(KinematicUnit* unit1, KinematicUnit* unit2)
{
	Vector2D tmpVec2D_1 = unit1->getPosition();
	Vector2D tmpVec2D_2 = unit2->getPosition();
	return sqrtf(pow(tmpVec2D_1.getX() - tmpVec2D_2.getX(), 2) + pow(tmpVec2D_1.getY() - tmpVec2D_2.getY(), 2));
}

int UnitManager::draw(GraphicsBuffer& pBuffer)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end(); ++iter)
	{
		iter->second->draw(&pBuffer);
	}

	return 0;
}

KinematicUnit* UnitManager::getUnit(std::string id)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first == id){
			return iter->second;
		}
		
		else
		{
			++iter;
		}
	}

	return NULL;
}

std::vector<KinematicUnit*> UnitManager::getNeighbors(KinematicUnit* unit, float radius)
{
	std::vector<KinematicUnit*> neighbors;
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end(); ++iter)
	{
		if (unit != iter->second && distanceBetweenUnits(unit, iter->second) < radius)
		{
			neighbors.push_back(iter->second);
		}
	}
	return neighbors;
}

int UnitManager::removeUnit(std::string id)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first == id)
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
		}

		else
		{
			++iter;
		}
	}

	return 0;
}

int UnitManager::removeUnit(KinematicUnit* unit)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->second == unit)
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
		}
		
		else
		{
			++iter;
		}
	}

	return 0;
}

int UnitManager::removeRandomAiUnit(std::string playerId)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first != PLAYER_ID)
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
			return 0; //unit was deleted, game may continue!
		}
		++iter; //if nothing was delete, move up one index.
	}

	return 1; //nothing was deleted, game should end!
}

int UnitManager::unitMapCleanUp()
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		delete iter->second;
		iter = mpUnitMap.erase(iter);
		mpTotalUnits--;
	}

	if (mpUnitMap.size() == 0)
	{
		return 0;
	}
	
	else
	{
		return -1;
	}
}

int UnitManager::update(float time)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end(); ++iter)
	{
		iter->second->update(time);
	}

	return 0;
}

