#pragma once

#include "Kinematic.h"
#include "Steering.h"
#include "vector"
//#include "WanderSeekFleeSteering.h"

/*KinematicUnit - a unit that is derived from the Kinematic class.  Adds behaviors and max speeds and a current Steering.

Dean Lawson
Champlain College
2011
*/

//forward declarations
class Sprite;
class GraphicsBuffer;
class WanderSeekFleeSteering;
class WallSolid;

const float RADIUS = 23.0f;
const float EPSILON = 2.0f;

extern Steering gNullSteering;//global object - can point to it for a "NULL" Steering

//minmimum forward speed a unit has to have inorder to rotate 
//(keeps unit from spinning in place after stopping
const float MIN_VELOCITY_TO_TURN_SQUARED = 1.0f;
const float MAX_VIS_DIST = 200.0f;

class KinematicUnit: public Kinematic
{
public:
	KinematicUnit(Sprite* pSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::vector<Vector2D> path, std::string mapName, float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	~KinematicUnit();

	//getters and setters
	void setTarget( const Vector2D& target ) { mTarget = target; };
	const Vector2D& getPosition() const { return mPosition; };
	float getMaxVelocity() const { return mMaxVelocity; };
	Vector2D getVelocity() const { return mVelocity; };
	float getMaxAcceleration() const { return mMaxAcceleration; };
	void setVelocity( const Vector2D& velocity ){ mVelocity = velocity; };
	inline void setId(std::string tmpId){ Id = tmpId; };
	inline std::vector <KinematicUnit*> getNeighbors(){ return mNeighbors; };

	virtual void setNewOrientation();//face the direction you are moving
	void dynamicSeek(Vector2D pTarget);

	int getVisRadius() { return mVisRadius; };
	void setVisRadius(int rad) { mVisRadius = rad; };

	//draw yourself to the indicated buffer
	void draw( GraphicsBuffer* pBuffer );
	//move according to the current velocities and update velocities based on current Steering
	void update(float time);

private:
	int mCurPathIndex;
	bool mReverse;
	Sprite* mpSprite;
	Steering* mpCurrentSteering;
	Vector2D mTarget;//used only for Kinematic seek and arrive
	std::vector<Vector2D> mpPath;
	float mMaxVelocity;
	float mMaxAcceleration;
	float mVisRadius;
	bool isParentBehavior;
	std::string mMapName;
	int mLastIndex;
	Vector2D prevTeleport;

	std::vector<KinematicUnit*> mNeighbors;

	void setSteering( Steering* pSteering );
	void applyDirectly(Vector2D lin, float ang);

	std::string Id;

};