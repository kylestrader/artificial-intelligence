#pragma once

#include "GameMessage.h"

class ToggleHelpMessage :public GameMessage
{
public:
	ToggleHelpMessage() : GameMessage(TOGGLE_HELP_MESSAGE){};
	~ToggleHelpMessage(){};

	void process();
};