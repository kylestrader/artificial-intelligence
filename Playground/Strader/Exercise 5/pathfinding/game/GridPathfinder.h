#pragma once

#include <allegro5/allegro.h>
#include "Pathfinder.h"
#include "Path.h"
class GridGraph;
class GridVisualizer;
class GraphicsBuffer;
class PathfindingDebugContent;

class GridPathfinder:public Pathfinder
{
public:
	friend class PathfindingDebugContent;

	GridPathfinder( GridGraph* pGraph );
	virtual ~GridPathfinder();

	virtual Path* findPath( Node* pFrom, Node* pTo ) = 0;

	void setShouldDrawVisited(bool setter){ mShouldDrawVisited = setter; };
	bool getShouldDrawVisited(){ return mShouldDrawVisited; };
	void addVisitedNode(Node* node){ mVisitedNodes.push_back(node); };
	Path* getPath(){ return mpPath; };
	inline void initPath(){ if (mpPath != NULL){ delete mpPath; mpPath = NULL; } };
	inline void addNode(Node* node){ if (mpPath == NULL){ mpPath = new Path(); } mpPath->addNode(node); };
	void setPathColor(ALLEGRO_COLOR mColor){ mPathColor = mColor; };
	void setVisitedColor(ALLEGRO_COLOR mColor){ mVisitedColor = mColor; };
	void setStartColor(ALLEGRO_COLOR mColor){ mStartColor = mColor; };
	void setStopColor(ALLEGRO_COLOR mColor){ mStopColor = mColor; };
	ALLEGRO_COLOR getPathColor(){ return mPathColor; };
	ALLEGRO_COLOR getVisitedColor(){ return mVisitedColor; };
	ALLEGRO_COLOR getStartColor(){ return mStartColor; };
	ALLEGRO_COLOR getStopColor(){ return mStopColor; };
	void setReversed(bool reversed){ mReversed = reversed; };
	
#ifdef VISUALIZE_PATH
	//just for visualization
public:
	void drawVisualization( Grid* pGrid, GraphicsBuffer* pDest );
protected:
	std::vector<Node*> mVisitedNodes;
	Path* mpPath;
	GridVisualizer* mpVisualizer;
	ALLEGRO_COLOR mPathColor;
	ALLEGRO_COLOR mVisitedColor;
	ALLEGRO_COLOR mStartColor;
	ALLEGRO_COLOR mStopColor;
#endif
	float distanceBetweenNodes(Node* pointA, Node* pointB);

	double mTimeElapsed;

private:
	bool mShouldDrawVisited;
	bool mReversed;
};