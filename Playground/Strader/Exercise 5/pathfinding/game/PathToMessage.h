#pragma once

#include "GameMessage.h"
#include "Node.h"
#include "Vector2D.h"

class PathToMessage:public GameMessage
{
public:
	PathToMessage::PathToMessage(Node* fromNode, Node* toNode) : mpFromNode(fromNode), mpToNode(toNode), GameMessage(PATH_TO_MESSAGE){}
	~PathToMessage();

	void process();

private:
	Node* mpFromNode;
	Node* mpToNode;
};