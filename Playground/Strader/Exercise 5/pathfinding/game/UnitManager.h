#pragma once

#include "KinematicUnit.h"
#include "Sprite.h"
#include <map>
#include <vector>

/*UnitManager - a storage unit that manages all existing units in the game. 

Kyle Strader
Champlain College
2014
*/

//chase type enumerator 
enum ChaseType 
{ 
	NO_CHASE, 
	DYNAMIC_SEEK, 
	DYNAMIC_ARRIVE,
	WANDER_SEEK,
	WANDER_FLEE,
	FLOCK_BOIDS
};

class UnitManager :public Trackable
{
public:
	//constructors/destructors
	UnitManager();
	~UnitManager();

	//Add unit 
	std::string addUnit(KinematicUnit* unit);
	//std::string addUnit(KinematicUnit* unit, ChaseType chaseType = NO_CHASE, KinematicUnit* target = 0);
	//std::string addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id = "", float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	//std::string addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id = "", ChaseType chaseType = NO_CHASE, KinematicUnit* target = 0, float maxVelocity = 1.0f, float maxAcceleration = 1.0f);
	
	//get unit
	KinematicUnit* getUnit(std::string id);
	std::map <std::string, KinematicUnit*>  getUnitsMap(){ return mpUnitMap; };
	std::vector<KinematicUnit*> getNeighbors(KinematicUnit* unit, float radius);
	
	//remove unit
	int removeUnit(std::string id);
	int removeUnit(KinematicUnit* unit);
	int removeRandomAiUnit(std::string playerId);
	
	//utils
	int draw(GraphicsBuffer& pBuffer);
	int unitMapCleanUp();
	int update(float time);

private:
	int mpTotalUnits;
	int mpAccumUnits;
	std::map <std::string, KinematicUnit*> mpUnitMap;

};
