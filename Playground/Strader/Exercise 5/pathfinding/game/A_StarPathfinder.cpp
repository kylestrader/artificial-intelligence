#include "A_StarPathfinder.h"
#include "Path.h"
#include "Connection.h"
#include "GridGraph.h"
#include "Grid.h"
#include "Game.h"
#include "GameApp.h"
#include "Vector2D.h"
#include <Cursor.h>
#include <PerformanceTracker.h>
#include <list>
#include <algorithm>

using namespace std;

A_StarPathfinder::A_StarPathfinder(Graph* pGraph)
	:GridPathfinder(dynamic_cast<GridGraph*>(pGraph))
{
#ifdef VISUALIZE_PATH
	mpPath = NULL;
#endif
	mPathColor = al_map_rgba(0, 150, 10, 40);
	mVisitedColor = al_map_rgba(100, 128, 100, 255);
	mStartColor = al_map_rgba(60, 150, 70, 255);
	mStopColor = al_map_rgba(60, 70, 150, 255);
}

A_StarPathfinder::~A_StarPathfinder()
{
#ifdef VISUALIZE_PATH
	delete mpPath;
#endif
}

const int DIST_MAX = 16; //the maximum distance any two nodes can have between one another (including portal connections).

Path* A_StarPathfinder::findPath(Node* pFrom, Node* pTo)
{
	gpPerformanceTracker->clearTracker("path");
	gpPerformanceTracker->startTracking("path");

	//Uses A* search Algorithm
	// :http://en.wikipedia.org/wiki/A*_search_algorithm

	if (mpPath != NULL)
	{
		delete mpPath;
		mpPath = NULL;
	}

	if (mVisitedNodes.size() > 0) mVisitedNodes.clear();

	map <std::string, map<int, Node*>> closedSetMap;

	map <std::string, std::map<int, Node*>> openSetMap;

	map <std::string, map<int, Node*>> cameFromMap;

	Node* startNode = pFrom;
	Node* endNode = pTo;
	openSetMap[startNode->getMapName()][startNode->getId()] = startNode;
	startNode->setG_Score(0);
	startNode->setF_Score(startNode->getG_Score() + estimateHeuristicCost(startNode, endNode));
	bool empty = false;

	while (openSetMapValidity(&openSetMap))
	{
		//check to make sure there is atleast one open set

		Node* currentNode = findLowestFScore(endNode, &openSetMap);
		int nodeIndex = currentNode->getId();
		static_cast<GameApp*>(gpGame)->getPathfinderByMap(currentNode->getMapName())->addVisitedNode(currentNode);
		if (currentNode == endNode)
		{
			mpPath = reconstructPath(&cameFromMap, endNode);
			gpPerformanceTracker->stopTracking("path");
			mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
			return mpPath;
		}

		int currentNodeID = currentNode->getId();
		openSetMap[currentNode->getMapName()].erase(currentNode->getId());
		closedSetMap[currentNode->getMapName()][currentNode->getId()] = currentNode;

		std::vector<Connection*> connections = static_cast<GameApp*>(gpGame)->getGridGraphByMap(currentNode->getMapName())->getConnections(currentNode->getId());
		int numConnections = connections.size();

		for (int i = 0; i < numConnections; i++)
		{
			Connection* currentConnection = connections[i];
			Node* neighbor = currentConnection->getToNode();

			if (closedSetMap[neighbor->getMapName()].find(neighbor->getId()) != closedSetMap[neighbor->getMapName()].end())
				continue;

			int tentative_g_score = currentNode->getG_Score() + dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getSquareSize();

			if (openSetMap[neighbor->getMapName()].find(neighbor->getId()) == openSetMap[neighbor->getMapName()].end() || tentative_g_score < neighbor->getG_Score())
			{
				cameFromMap[neighbor->getMapName()][neighbor->getId()] = currentNode;
				neighbor->setG_Score(tentative_g_score);
				neighbor->setF_Score(neighbor->getG_Score() + estimateHeuristicCost(neighbor, endNode));

				if (openSetMap[neighbor->getMapName()].find(neighbor->getId()) == openSetMap[neighbor->getMapName()].end())
				{
					openSetMap[neighbor->getMapName()][neighbor->getId()] = neighbor;
				}
			}
		}
	}
	gpPerformanceTracker->stopTracking("path");
	mTimeElapsed = gpPerformanceTracker->getElapsedTime("path");
	static_cast<GameApp*>(gpGame)->getCursor()->setWarning(true);

	return NULL;
}

int A_StarPathfinder::estimateHeuristicCost(Node* currentNode, Node* targetNode)
{

	if (currentNode->getMapName() == targetNode->getMapName())
	{
		//uses Manhattan heuristic
		//updated so that the heuristic looks at the distance on a tile basis (as opposed to a pixel basis)
		GridGraph* gridGraph = dynamic_cast <GridGraph*>(mpGraph);

		Vector2D currentPos = gridGraph->getGrid()->getULCornerOfSquare(currentNode->getId());
		Vector2D targetPos = gridGraph->getGrid()->getULCornerOfSquare(targetNode->getId());

		int toX = static_cast<int>(targetPos.getX() / gridGraph->getGrid()->getSquareSize());
		int toY = static_cast<int>(targetPos.getY() / gridGraph->getGrid()->getSquareSize());
		int fromX = static_cast<int>(currentPos.getX() / gridGraph->getGrid()->getSquareSize());
		int fromY = static_cast<int>(currentPos.getY() / gridGraph->getGrid()->getSquareSize());
		int graphDistX = static_cast<int>(abs(toX - fromX));
		int graphDistY = static_cast<int>(abs(toY - fromY));

		return graphDistX + graphDistY;
		//return int(Vector2D::distance(UL1, UL2));
	}
	else //distance is set to width of a tile since they are in different play spaces
	{
		return dynamic_cast <GridGraph*>(mpGraph)->getGrid()->getSquareSize();
	}
}

Node* A_StarPathfinder::findLowestFScore(Node* end, map <std::string, std::map<int, Node*>>* set)
{
	map <std::string, std::map<int, Node*>>::iterator mapIter = set->begin();

	Node* smallestNode = NULL;

	int smallestF_Score = INT_MAX;
	for (; mapIter != set->end(); mapIter++)
	{
		map <int, Node*>::iterator iter = mapIter->second.begin();

		for (; iter != mapIter->second.end(); iter++)
		{
			if (iter->second->getF_Score() + estimateHeuristicCost(iter->second, end) < smallestF_Score)
			{
				smallestNode = iter->second;
				smallestF_Score = smallestNode->getF_Score();
			}
		}
	}
	return smallestNode;
}

Path* A_StarPathfinder::reconstructPath(map <std::string, map<int, Node*>>* cameFromMap, Node* currentNode)
{
	Path* pPath = NULL;
	if (cameFromMap->find(currentNode->getMapName())->second.find(currentNode->getId()) != cameFromMap->find(currentNode->getMapName())->second.end())
	{
		Node* node = cameFromMap->find(currentNode->getMapName())->second.find(currentNode->getId())->second;

		pPath = reconstructPath(cameFromMap, node);
		pPath->addNode(currentNode);
	}

	else
	{
		pPath = new Path();
		pPath->addNode(currentNode);
	}

	return pPath;
}

bool A_StarPathfinder::openSetMapValidity(std::map <std::string, std::map<int, Node*>>* set)
{
	map <std::string, std::map<int, Node*>>::iterator mapIter = set->begin();
	for (; mapIter != set->end(); mapIter++)
	{
		map <int, Node*>::iterator iter = mapIter->second.begin();

		for (; iter != mapIter->second.end(); iter++)
		{
			if (iter->second != NULL)
			{
				return true;
			}
		}
	}
	return false;
}