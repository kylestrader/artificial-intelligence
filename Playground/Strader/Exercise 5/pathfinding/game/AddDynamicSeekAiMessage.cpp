#include "Game.h"
#include "GameApp.h"
#include "GameMessageManager.h"
#include "AddDynamicSeekAiMessage.h"
#include "UnitManager.h"
#include "SpriteManager.h"

AddDynamicSeekAiMessage::AddDynamicSeekAiMessage(std::vector<Vector2D> path, std::string mapName) :GameMessage(ADD_DYNAMIC_SEEK_AI){
	mPath = path;
	mMapName = mapName;
}

void AddDynamicSeekAiMessage::process()
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);

	Vector2D pos2(mPath.at(0).getX(), mPath.at(0).getY());
	Vector2D vel2(0.0f, 0.0f);
	Vector2D targ(mPath.at(1).getX(), mPath.at(1).getY());
	KinematicUnit* tmp = new KinematicUnit(pGameApp->getSpriteManager()->getSprite(PLAYER_SPRITE_ID), pos2, 0.0f, vel2, 0.0f, mPath, mMapName, 6.0f, 20.0f);
	tmp->dynamicSeek(targ);
	static_cast<GameApp*>(gpGame)->getUnitManager()->addUnit(tmp);

}