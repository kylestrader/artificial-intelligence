#include "ToggleHelpMessage.h"
#include "Game.h"
#include "GameApp.h"
#include "HUD.h"

void ToggleHelpMessage::process()
{
	GameApp* pGame = dynamic_cast<GameApp*>(gpGame);
	pGame->getHUD()->toggleHelp();
}