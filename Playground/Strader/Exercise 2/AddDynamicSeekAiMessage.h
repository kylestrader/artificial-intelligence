#pragma once

#include "GameMessage.h"
#include "Vector2d.h"

class Vector2D;

class AddDynamicSeekAiMessage : public GameMessage
{
public:
	AddDynamicSeekAiMessage(const Vector2D& playerPos);
	~AddDynamicSeekAiMessage();

	void process();

private:
	Vector2D mPos;
};