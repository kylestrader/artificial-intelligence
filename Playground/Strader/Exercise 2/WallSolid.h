#pragma once

#include <allegro5/allegro_primitives.h>
#include <string>

#include "Game.h"
#include "Vector2D.h"
#include "Sprite.h"
#include "KinematicUnit.h"
#include "GraphicsBuffer.h"

enum CollisionFace
{
	TOP,
	BOTTOM,
	LEFT,
	RIGHT
};

class WallSolid
{
public:

	WallSolid(int ID, Vector2D pos, CollisionFace face);
	~WallSolid();

	void draw(GraphicsBuffer& pBuffer);

	Vector2D getPosition(){ return mPosition; }
	Vector2D getSize(){ return mSize; }
	Vector2D getVector(){ return mVector; };

	void setPosition(Vector2D pos){ mPosition = pos; }
	void setSize(Vector2D size){ mSize = size; }

	bool isInside(Vector2D point);

private:
	Vector2D mPosition;
	Vector2D mSize;
	Vector2D mVector;
	Sprite* mpSprite;
	int m_iSpriteID;
	CollisionFace mCollisionFace;

};