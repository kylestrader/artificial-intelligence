#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class Vector2D;

class AddWanderSeekAiMessage : public GameMessage
{
public:
	AddWanderSeekAiMessage(const Vector2D& playerPos);
	~AddWanderSeekAiMessage(){};

	void process();

private:
	Vector2D mPos;
};