#pragma once

#include "Game.h"
#include "WallSolid.h"
#include "WallSolidManager.h"
#include "steering.h"
#include "KinematicWanderSteering.h"
#include "DynamicSeekSteering.h"
#include "KinematicUnit.h"

class KinematicUnit;

class WanderSeekFleeSteering:public Steering
{
public:
	WanderSeekFleeSteering(KinematicUnit* pMover, KinematicUnit* pTarget, float visDist = 300, bool shouldFlee = false);
	WanderSeekFleeSteering(){}

	void setTarget(KinematicUnit* pTarget);

	virtual Steering* getSteering();

	inline float dotProduct(const Vector2D& vec1, const Vector2D& vec2){ return (vec1.getX() * vec2.getX()) + (vec1.getY() * vec2.getY()); };

private:
	bool isColliding(WallSolid* tmpWall);

	KinematicUnit* mpTarget;
	KinematicUnit* mpMover;
	float mVisDist;
	bool mShouldFlee;
};