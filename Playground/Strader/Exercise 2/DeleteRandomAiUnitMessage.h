#pragma once

#include "GameMessage.h"

class DeleteRandomAiUnitMessage : public GameMessage
{
public:
	DeleteRandomAiUnitMessage() :GameMessage(DELETE_RANDOM_UNIT_MESSAGE){};
	~DeleteRandomAiUnitMessage();

	void process();

};