#pragma once

#include "GameMessage.h"
#include "Vector2D.h"

class Vector2D;

class AddWanderFleeAiMessage : public GameMessage
{
public:
	AddWanderFleeAiMessage(const Vector2D& playerPos);
	~AddWanderFleeAiMessage(){};

	void process();

private:
	Vector2D mPos;
};