#pragma once

#include "WallSolid.h"

class WallSolidManager
{
public:
	//constructors/destructors
	WallSolidManager();
	~WallSolidManager();

	//Add Wall
	void addWallSolid(WallSolid* wallSolid);
	void WallSolidManager::addWallSolid(int spriteID, Vector2D pos, CollisionFace collisionFace);

	//get unit
	WallSolid* getWallSolid(std::string id);
	WallSolid* getWallSolid(int index);
	int getNumWallsSolids(){ return mWallSolidsMap.size(); };

	//remove unit
	void removeWallSolid(std::string id);
	void removeWallSolid(WallSolid* wall);

	//utils
	void draw(GraphicsBuffer& pBuffer);
	void wallSolidsMapCleanUp();

private:
	int mTotalWallSolids;
	int mAccumWallSolids;
	std::map <std::string, WallSolid*> mWallSolidsMap;

};