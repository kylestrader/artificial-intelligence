#include "Game.h"
#include "GameMessageManager.h"
#include "AddDynamicArriveAiMessage.h"
#include "UnitManager.h"
#include "SpriteManager.h"

AddDynamicArriveAiMessage::AddDynamicArriveAiMessage(const Vector2D& playerPos) :GameMessage(ADD_DYNAMIC_ARRIVE_AI){
	mPos = playerPos;
}

void AddDynamicArriveAiMessage::process()
{
	Vector2D pos2(mPos.getX() + 200, mPos.getY());
	Vector2D vel2(0.0f, 0.0f);
	gpGame->getUnitManager()->addUnit(gpGame->getSpriteManager()->getSprite(gpGame->getEnemyIconBufferId()), pos2, 1, vel2, 0.0f, "", DYNAMIC_ARRIVE, gpGame->getUnitManager()->getUnit(PLAYER_ID), 180.0f, 100.0f);
}