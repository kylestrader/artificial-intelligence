#pragma once

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include <sstream>

#include "Vector2D.h"
#include "KeyState.h"
#include "MouseState.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

class InputManager :public Trackable
{
public:
	//Constructors/Destructors
	InputManager();
	~InputManager();


	int initialize();
	int update();
	int checkKeyboard();
	int checkMouse();
	int fireMessages();

private:

	ALLEGRO_EVENT_QUEUE* mpKeyboardEventQueue;
	ALLEGRO_EVENT mpEvent;
	ALLEGRO_MOUSE_STATE mpMouseCondition;

	//Input state systems
	KeyState mpKeyState;
	MouseState mpMouseState;

};