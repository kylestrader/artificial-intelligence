#include "UnitManager.h"
#include "game.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

UnitManager::UnitManager()
{
	mpTotalUnits = 0;
	mpAccumUnits = 0;
}

UnitManager::~UnitManager()
{
	unitMapCleanUp();
}

std::string UnitManager::addUnit(KinematicUnit* unit)
{
	std::string unitID = "unit" + std::to_string(mpAccumUnits);

	mpUnitMap[unitID] = unit;
	mpTotalUnits++;
	mpAccumUnits++;

	return unitID;
}

std::string UnitManager::addUnit(KinematicUnit* unit, ChaseType chaseType, KinematicUnit* target)
{
	std::string unitID = "unit" + std::to_string(mpAccumUnits);

	//allows user to specify the behavior of the ai on creation
	if (chaseType != NO_CHASE && target != NULL)
	{
		switch (chaseType)
		{

		case DYNAMIC_ARRIVE: //tell unit to use dynamic arrive
			unit->dynamicArrive(target);
			break;

		case DYNAMIC_SEEK: //tell unit to use dynamic seek
			unit->dynamicSeek(target);
			break;

		case WANDER_FLEE: //tell unit to use wander flee method
			unit->wanderFlee(target);
			break;

		case WANDER_SEEK: //tell unit to use wander seek method
			unit->wanderSeek(target);
			break;
		}
	}

	//add unit to map
	mpUnitMap[unitID] = unit;
	mpTotalUnits++;
	mpAccumUnits++;

	return unitID;
}

std::string UnitManager::addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id, float maxVelocity, float maxAcceleration)
{
	std::string unitID = "";

	if (id == "") //if user does not specify a name then create a unique id
	{
		unitID = "unit" + std::to_string(mpAccumUnits);
	}

	else
	{
		unitID = id;
	}

	KinematicUnit* tmpUnit = new KinematicUnit(unitSprite, position, orientation, velocity, rotationVel, maxVelocity, maxAcceleration); //create the unit

	mpUnitMap[unitID] = tmpUnit; //add the unit to the map
	mpTotalUnits++;
	mpAccumUnits++;

	return unitID;
}

std::string UnitManager::addUnit(Sprite* unitSprite, const Vector2D& position, float orientation, const Vector2D& velocity, float rotationVel, std::string id, ChaseType chaseType, KinematicUnit* target, float maxVelocity, float maxAcceleration)
{
	std::string unitID = "";

	if (id == "") //if user does not specify a name then create a unique id
	{
		unitID = "unit" + std::to_string(mpAccumUnits);
	}
	
	else
	{
		unitID = id;
	}

	KinematicUnit* tmpUnit = new KinematicUnit(unitSprite, position, orientation, velocity, rotationVel, maxVelocity, maxAcceleration); //create the unit

	if (chaseType != NO_CHASE && target != NULL)
	{
		switch (chaseType)
		{
		case DYNAMIC_ARRIVE: //tell unit to use dynamic arrive
			tmpUnit->dynamicArrive(target);
			break;

		case DYNAMIC_SEEK: //tell unit to use dynamic seek
			tmpUnit->dynamicSeek(target);
			break;

		case WANDER_FLEE: //tell unit to use wander flee method
			tmpUnit->wanderFlee(target);
			break;

		case WANDER_SEEK: //tell unit to use wander seek method
			tmpUnit->wanderSeek(target);
			break;

		}
	}

	mpUnitMap[unitID] = tmpUnit;
	mpTotalUnits++;
	mpAccumUnits++;

	return unitID;
}

int UnitManager::draw(GraphicsBuffer* pBuffer)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end(); ++iter)
	{
		iter->second->draw(pBuffer);
	}

	return 0;
}

KinematicUnit* UnitManager::getUnit(std::string id)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first == id){
			return iter->second;
		}
		
		else
		{
			++iter;
		}
	}

	return NULL;
}

int UnitManager::removeUnit(std::string id)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first == id)
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
		}

		else
		{
			++iter;
		}
	}

	return 0;
}

int UnitManager::removeUnit(KinematicUnit* unit)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->second == unit)
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
		}
		
		else
		{
			++iter;
		}
	}

	return 0;
}

int UnitManager::removeRandomAiUnit(std::string playerId)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		if (iter->first != gpGame->getPlayerId())
		{
			delete iter->second;
			iter = mpUnitMap.erase(iter);
			mpTotalUnits--;
			return 0; //unit was deleted, game may continue!
		}
		++iter; //if nothing was delete, move up one index.
	}

	return 1; //nothing was deleted, game should end!
}

int UnitManager::unitMapCleanUp()
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end();)
	{
		delete iter->second;
		iter = mpUnitMap.erase(iter);
		mpTotalUnits--;
	}

	if (mpUnitMap.size() == 0)
	{
		return 0;
	}
	
	else
	{
		return -1;
	}
}

int UnitManager::update(float time)
{
	std::map<std::string, KinematicUnit*>::iterator iter;
	iter = mpUnitMap.begin();

	for (iter = mpUnitMap.begin(); iter != mpUnitMap.end(); ++iter)
	{
		iter->second->update(time);
	}

	return 0;
}

