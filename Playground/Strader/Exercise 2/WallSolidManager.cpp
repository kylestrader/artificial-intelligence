#include "WallSolidManager.h"
#include "WallSolid.h"
#include "game.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

WallSolidManager::WallSolidManager()
{
	mTotalWallSolids = 0;
	mAccumWallSolids = 0;
}

WallSolidManager::~WallSolidManager()
{
	wallSolidsMapCleanUp();
}

void WallSolidManager::addWallSolid(WallSolid* wallSolid)
{
	std::string unitID = "wallSolid" + std::to_string(mAccumWallSolids);

	mWallSolidsMap[unitID] = wallSolid;
	mTotalWallSolids++;
	mAccumWallSolids++;
}

void WallSolidManager::addWallSolid(int spriteID, Vector2D pos, CollisionFace collisionFace)
{
	std::string wallSolidID = "wallSolid" + std::to_string(mAccumWallSolids);

	WallSolid* tmpWallSolid = new WallSolid(spriteID, pos, collisionFace);

	mWallSolidsMap[wallSolidID] = tmpWallSolid;
	mTotalWallSolids++;
	mAccumWallSolids++;
}

void WallSolidManager::draw(GraphicsBuffer& pBuffer)
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (iter = mWallSolidsMap.begin(); iter != mWallSolidsMap.end(); ++iter)
	{
		iter->second->draw(pBuffer);
	}
}

WallSolid* WallSolidManager::getWallSolid(int index)
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (int i = 0; i < index; i++)
	{
		++iter;
	}

	if (iter->second != NULL)
	{
		return iter->second;
	}

	return NULL;
}

WallSolid* WallSolidManager::getWallSolid(std::string id)
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (iter = mWallSolidsMap.begin(); iter != mWallSolidsMap.end();)
	{
		if (iter->first == id){
			return iter->second;
		}

		else
		{
			++iter;
		}
	}

	return NULL;
}

void WallSolidManager::removeWallSolid(std::string id)
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (iter = mWallSolidsMap.begin(); iter != mWallSolidsMap.end();)
	{
		if (iter->first == id)
		{
			delete iter->second;
			iter = mWallSolidsMap.erase(iter);
			mTotalWallSolids--;
		}

		else
		{
			++iter;
		}
	}
}

void WallSolidManager::removeWallSolid(WallSolid* wallSolid)
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (iter = mWallSolidsMap.begin(); iter != mWallSolidsMap.end();)
	{
		if (iter->second == wallSolid)
		{
			delete iter->second;
			iter = mWallSolidsMap.erase(iter);
			mTotalWallSolids--;
		}

		else
		{
			++iter;
		}
	}
}

void WallSolidManager::wallSolidsMapCleanUp()
{
	std::map<std::string, WallSolid*>::iterator iter;
	iter = mWallSolidsMap.begin();

	for (iter = mWallSolidsMap.begin(); iter != mWallSolidsMap.end();)
	{
		delete iter->second;
		iter = mWallSolidsMap.erase(iter);
		mTotalWallSolids--;
	}
}