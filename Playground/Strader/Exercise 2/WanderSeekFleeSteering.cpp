#include "WanderSeekFleeSteering.h"

WanderSeekFleeSteering::WanderSeekFleeSteering(KinematicUnit* pMover, KinematicUnit* pTarget, float visDist, bool shouldFlee)
:mpMover(pMover)
, mpTarget(pTarget)
{
	mApplyDirectly = true;
	mVisDist = visDist;
	mShouldFlee = shouldFlee;
}

void WanderSeekFleeSteering::setTarget(KinematicUnit* pTarget)
{
	mpTarget = pTarget; 
}

Steering* WanderSeekFleeSteering::getSteering()
{
	Vector2D pos = mpMover->getPosition();
	Vector2D targetPos = mpTarget->getPosition();
	float distFromTarget = sqrt(pow(pos.getX() - targetPos.getX(), 2) + pow(pos.getY() - targetPos.getY(), 2));
	float targetRadius = mpTarget->getVisRadius();

	//unit should be wandering
	if (distFromTarget > 300)
	{
		KinematicWanderSteering mpWander = KinematicWanderSteering(mpMover);
		mLinear = mpWander.getSteering()->getLinear();
		mAngular = mpWander.getSteering()->getAngular();
		mApplyDirectly = true;
	}

	//unit should be seeking
	else 
	{
		DynamicSeekSteering mpSeek = DynamicSeekSteering(mpMover, mpTarget, mShouldFlee);
		mLinear = mpSeek.getSteering()->getLinear();
		mAngular = mpSeek.getSteering()->getAngular();
		mApplyDirectly = true;
	}

	int numWalls = gpGame->getWallSolidManager()->getNumWallsSolids(); //Find the number of walls to check
	for (int i = 0; i < numWalls; i++)
	{
		if (isColliding(gpGame->getWallSolidManager()->getWallSolid(i))) //if a collision is happening
		{
			//get the normal of the wall and normalize it.
			Vector2D wallVec = gpGame->getWallSolidManager()->getWallSolid(i)->getVector();
			wallVec.normalize();

			//Find the bounce
			float vecLength = dotProduct(mpMover->getVelocity(), wallVec);
			wallVec *= vecLength * 2.0f;

			//subtract the bounce from mLinear and normalize it
			mLinear -= wallVec;
			mLinear.normalize();

			//extend the normalized linear vector
			mLinear *= mpMover->getMaxVelocity();

			//modify the angular to compensate for the bounce
			mAngular += atan2f(mLinear.getY(), mLinear.getX());

			//reset orientation
			mpMover->setNewOrientation();
		}
	}

	return this;
}

bool WanderSeekFleeSteering::isColliding(WallSolid* tmpWall)
{
	Vector2D tmpLinear = mLinear;
	tmpLinear.normalize();
	tmpLinear *= 5;
	Vector2D nextStep = mpMover->getPosition() + tmpLinear;
	if (tmpWall->isInside(nextStep))
	{
		return true;
	}
	return false;
}

