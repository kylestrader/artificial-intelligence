#include "Game.h"
#include "GameMessageManager.h"
#include "DeleteRandomAiUnitMessage.h"
#include "UnitManager.h"

void DeleteRandomAiUnitMessage::process(){
	int endGame = gpGame->getUnitManager()->removeRandomAiUnit(gpGame->getPlayerId());

	if (endGame == 1){
		gpGame->setShouldExit(true);
	}

}