#pragma once

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

struct KeyState
{
	bool A_KEY_DOWN;
	bool S_KEY_DOWN;
	bool D_KEY_DOWN;
	bool ESC_KEY_DOWN;
};