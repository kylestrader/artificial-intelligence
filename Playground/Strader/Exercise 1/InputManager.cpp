#include "InputManager.h"
#include"Game.h"
#include "GameMessage.h"
#include "GameMessageManager.h"
#include "PlayerMoveToMessage.h"
#include "ExitGameMessage.h"
#include "AddDynamicArriveAiMessage.h"
#include "AddDynamicSeekAiMessage.h"
#include "DeleteRandomAiUnitMessage.h"
#include "UnitManager.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

InputManager::InputManager()
{
	mpKeyState.A_KEY_DOWN = false;
	mpKeyState.D_KEY_DOWN = false;
	mpKeyState.ESC_KEY_DOWN = false;
	mpKeyState.S_KEY_DOWN = false;

	mpMouseState.LEFT_DOWN = false;
	mpMouseState.RIGHT_DOWN = false;
	mpMouseState.MOUSE_LOC.setX(0);
	mpMouseState.MOUSE_LOC.setY(0);
}

InputManager::~InputManager()
{
	al_destroy_event_queue(mpKeyboardEventQueue);
	al_uninstall_keyboard();
	al_uninstall_mouse();
}

int InputManager::initialize()
{
	//install mouse
	if (!al_install_mouse())
	{
		printf("Mouse not installed!\n");
		return -1;
	}

	//install keyboard
	if (!al_install_keyboard())
	{
		printf("Keyboard not installed!\n");
		return -1;
	}

	//setup event queue for keyboard input
	mpKeyboardEventQueue = al_create_event_queue();
	al_register_event_source(mpKeyboardEventQueue, al_get_keyboard_event_source());

	return 0;
}

int InputManager::update()
{
	checkKeyboard();
	checkMouse(); 
	fireMessages();
	return 0;
}

int InputManager::checkKeyboard()
{
	//run through every keyboard event
	while (!al_event_queue_is_empty(mpKeyboardEventQueue))
	{
		al_get_next_event(mpKeyboardEventQueue, &mpEvent);
		if (mpEvent.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (mpEvent.keyboard.keycode)
			{
			case ALLEGRO_KEY_A: //A key pressed
				mpKeyState.A_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_D: //D key pressed
				mpKeyState.D_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_ESCAPE: //Esc key pressed
				mpKeyState.ESC_KEY_DOWN = true;
				break;

			case ALLEGRO_KEY_S: //S key pressed
				mpKeyState.S_KEY_DOWN = true;
				break;

			default:
				break;
			}
		}
	}

	return 0;
}

int InputManager::checkMouse()
{
	//setup mouse state
	ALLEGRO_MOUSE_STATE mouseState;
	al_get_mouse_state(&mouseState);

	if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mpMouseState.LEFT_DOWN = true;
	}

	else if (al_mouse_button_down(&mouseState, 1)) //left mouse click
	{
		mpMouseState.LEFT_DOWN = true;
	}

	mpMouseState.MOUSE_LOC.setX(mouseState.x);
	mpMouseState.MOUSE_LOC.setY(mouseState.y);

	//create mouse text
	std::stringstream mousePos;
	mousePos << mpMouseState.MOUSE_LOC.getX() << ":" << mpMouseState.MOUSE_LOC.getY();

	//write text at mouse position
	al_draw_text(gpGame->getFont(), al_map_rgb(255, 255, 255), mpMouseState.MOUSE_LOC.getX(), mpMouseState.MOUSE_LOC.getY(), ALLEGRO_ALIGN_CENTRE, mousePos.str().c_str());

	return 0;
}

int InputManager::fireMessages()
{
	if (mpKeyState.A_KEY_DOWN) //create dynamic arrive ai unit
	{
		Vector2D pos = gpGame->getUnitManager()->getUnit(gpGame->getPlayerId())->getPosition();
		GameMessage* pMessage = new AddDynamicArriveAiMessage(pos);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.A_KEY_DOWN = false;
		std::cout << "A key pressed!" << std::endl;
	}

	if (mpKeyState.D_KEY_DOWN) //create delete random ai unit
	{
		GameMessage* pMessage = new DeleteRandomAiUnitMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.D_KEY_DOWN = false;
		std::cout << "D key pressed!" << std::endl;
	}

	if (mpKeyState.ESC_KEY_DOWN) //exit game
	{
		GameMessage* pMessage = new ExitGameMessage();
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.ESC_KEY_DOWN = false;
		std::cout << "Esc key pressed!" << std::endl;
	}

	if (mpKeyState.S_KEY_DOWN) //add dynamic seek ai unit
	{
		Vector2D pos = gpGame->getUnitManager()->getUnit(gpGame->getPlayerId())->getPosition();
		GameMessage* pMessage = new AddDynamicSeekAiMessage(pos);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpKeyState.S_KEY_DOWN = false;
		std::cout << "S key pressed!" << std::endl;
	}

	if (mpMouseState.LEFT_DOWN) //player move to
	{
		Vector2D pos(mpMouseState.MOUSE_LOC.getX(), mpMouseState.MOUSE_LOC.getY());
		GameMessage* pMessage = new PlayerMoveToMessage(pos);
		gpGame->getMessageManager()->addMessage(pMessage, 0);
		mpMouseState.LEFT_DOWN = false;
		std::cout << "Left Mouse button pressed!" << std::endl;
	}

	if (mpMouseState.RIGHT_DOWN)
	{
		mpMouseState.RIGHT_DOWN = false;
		std::cout << "Right Mouse button pressed!" << std::endl;
	}

	return 0;
}




