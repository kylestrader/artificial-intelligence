#pragma once

#include "GameMessage.h"
#include "Vector2D.h"
#include "InputInterpretor.h"

class SendMouseActionMessage :public GameMessage
{
public:
	SendMouseActionMessage(MouseClick click, Vector2D mousePos) : mClick(click), mMousePos(mousePos), GameMessage(SEND_MOUSE_ACTION){};
	~SendMouseActionMessage(){};

	void process();

private:
	Vector2D mMousePos;
	MouseClick mClick;
};