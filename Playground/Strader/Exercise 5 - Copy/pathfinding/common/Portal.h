#pragma once

#include <string>
#include <Trackable.h>

class Portal:public Trackable
{
public:
	//constructor/destructor
	Portal(std::string fromMapName, std::string toMapName, std::string _linkerID, int _tileID);
	~Portal(){};

	//getters
	inline const std::string getToMapName(){ return mToMapName; };
	inline const std::string getFromMapName(){ return mFromMapName; };
	inline const std::string getLinkerID(){ return mLinkerID; };
	inline const int getTileID(){ return mTileID; };

private:
	//private variables
	std::string mToMapName;
	std::string mFromMapName;
	std::string mLinkerID;
	int mTileID;
};