#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro.h>

#include "HUD.h"
#include "Game.h"
#include "GraphicsSystem.h"

HUD::HUD(int x, int y, int w, int h)
{
	mX_Pos = x;
	mY_Pos = y;
	mWidth = w;
	mHeight = h;
	mpFont = al_load_ttf_font( std::string(FONTS_DIRECTORY + "arial.ttf").c_str(), 14, ALLEGRO_ALIGN_CENTER);
	mHelp = false;
}

HUD::~HUD()
{
	al_destroy_font(mpFont);
}

void HUD::draw(GraphicsBuffer& gb)
{

	ALLEGRO_BITMAP* pOldTarget = GraphicsSystem::switchTargetBitmap(gb.getBitmap());

	al_draw_filled_rectangle(mX_Pos, mY_Pos, mX_Pos + mWidth, mY_Pos + mHeight, al_map_rgba(0, 0, 0, 75));

	if (mHelp)
	{
		al_draw_filled_rectangle(mX_Pos + 10, mY_Pos + 120, mX_Pos + mWidth - 10, mY_Pos + mHeight - 10, al_map_rgba(0, 0, 0, 25));
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + mWidth / 2, 140, ALLEGRO_ALIGN_CENTER, std::string("Controls:").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 170, ALLEGRO_ALIGN_LEFT, std::string("L: load dungeons").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 190, ALLEGRO_ALIGN_LEFT, std::string("S: Save dungeons").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 210, ALLEGRO_ALIGN_LEFT, std::string("ESC: Quit").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 230, ALLEGRO_ALIGN_LEFT, std::string("+: View next map").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 250, ALLEGRO_ALIGN_LEFT, std::string("-: View previous map").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 270, ALLEGRO_ALIGN_LEFT, std::string("LMB: Place wall/traverse").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 290, ALLEGRO_ALIGN_LEFT, std::string("MMB: Place portal").c_str());
		al_draw_text(mpFont, al_map_rgb(0, 0, 0), mX_Pos + 15, 310, ALLEGRO_ALIGN_LEFT, std::string("RMB: Remove wall/portal").c_str());
	}

	GraphicsSystem::switchTargetBitmap(pOldTarget);
}