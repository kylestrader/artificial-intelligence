#pragma once

#include <stdio.h>
#include <allegro5/allegro.h>
#include <stack>
#include <sstream>
#include <utility>

#include "Vector2D.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

class InputManager :public Trackable
{
public:
	//Constructors/Destructors
	InputManager();
	~InputManager();


	int initialize();
	int update();

private:
	int checkKeyboard();
	int checkMouse();
	int fireMessages();

	ALLEGRO_EVENT_QUEUE* mpKeyboardEventQueue;
	ALLEGRO_EVENT mpKeyEvent;
	ALLEGRO_EVENT_QUEUE* mpMouseEventQueue;
	ALLEGRO_EVENT mpMouseEvent;

	//Input state systems
	std::stack<std::string> mKeyPressStack;
	std::stack<std::pair<int, Vector2D>> mMouseClickStack;

};