#pragma once

#include <Trackable.h>

enum MessageType
{
	INVALID_MESSAGE_TYPE = -1,
	SEND_KEY_ACTION = 0,
	SEND_MOUSE_ACTION = 1,
	PATH_TO_MESSAGE = 2,
	SWITCH_MAP_MESSAGE = 3,
	TOGGLE_DRAW_VISITED_MESSAGE = 4,
	TOGGLE_HELP_MESSAGE = 5
};

class GameMessage: public Trackable
{
public:
	friend class GameMessageManager;

	GameMessage( MessageType type  );
	~GameMessage();

	double getSubmittedTime() const { return mSubmittedTime; };
	double getScheduledTime() const { return mScheduledTime; };

protected:
	MessageType mType;
	double mSubmittedTime;
	double mScheduledTime;

private:
	virtual void process() = 0; 
};


