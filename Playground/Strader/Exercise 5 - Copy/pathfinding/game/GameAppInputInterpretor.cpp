#include "GameAppInputInterpretor.h"
#include "GameApp.h"
#include "HUD.h"
#include "Cursor.h"
#include "GridVisualizer.h"
#include "GridGraph.h"
#include "GameMessageManager.h"
#include "GameMessage.h"
#include "PathToMessage.h"
#include "ToggleDrawVisitedMessage.h"
//for keyboard
void GameAppInputInterpretor::interpretInput(std::string input)
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);

	//alphabet
	if (input == "H")
	{
		pGameApp->getHUD()->toggleHelp();
	}

	if (input == "C")
	{
		if (mLeftClicked && mRightClicked)
		{
			GameMessage* pMessage = new ToggleDrawVisitedMessage(mpLastLeftClickedNode, mpLastRightClickedNode);
			gpGame->getGameMessageManager()->addMessage(pMessage, 0);
		}
	}

	//alternative
	else if (input == "ESCAPE")
	{
		gpGame->markForExit();
	}

	else if (input == "F1")
	{
		pGameApp->getHUD()->toggleHelp();
	}
}

//for mouse
void GameAppInputInterpretor::interpretInput(Vector2D mousePos, MouseClick click)
{
	GameApp* pGameApp = static_cast<GameApp*>(gpGame);
	Grid* pGrid = pGameApp->getCurMap()->getGrid();

	mMousePos = transposeMousePos(mousePos);

	pGameApp->getCursor()->setTilePos(Vector2D(int(mMousePos.getX() / pGrid->getSquareSize()), int(mMousePos.getY() / pGrid->getSquareSize())));

	if (click == LEFT_DOWN && !mPortalClickedState)
	{
		if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == CLEAR_VALUE)
		{
			Node* tmpNode;
			int index = pGrid->getSquareIndexFromPixelXY(mMousePos.getX(), mMousePos.getY());
			tmpNode = pGameApp->getCurGridGraph()->getNode(index);
			mpLastLeftClickedNode = tmpNode;
			if (!mLeftClicked)
				mLeftClicked = true;
			sendPathToMessage();
		}

		else if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == PORTAL_VALUE)
		{
			pGameApp->handlePortalClick(mMousePos);
			mPortalClickedState = true;
		}
	}

	else if (click == LEFT_UP && mPortalClickedState)
		mPortalClickedState = false;

	else if (click == RIGHT_DOWN)
	{
		if (pGrid->getValueAtPixelXY(mMousePos.getX(), mMousePos.getY()) == CLEAR_VALUE)
		{
			Node* tmpNode;
			int index = pGrid->getSquareIndexFromPixelXY(mMousePos.getX(), mMousePos.getY());
			tmpNode = pGameApp->getCurGridGraph()->getNode(index);
			mpLastRightClickedNode = tmpNode;
			if (!mRightClicked)
				mRightClicked = true;
			sendPathToMessage();
		}
	}

	else if (click == NO_CLICK)
	{
		pGameApp->getCurMap()->setSelectedTileIndex(
			pGrid->getSquareIndexFromPixelXY(mMousePos.getX(), 
			mMousePos.getY()));
	}
}

void GameAppInputInterpretor::sendPathToMessage()
{
	if (mLeftClicked && mRightClicked)
	{
		GameMessage* pMessage = new PathToMessage(mpLastLeftClickedNode, mpLastRightClickedNode);
		gpGame->getGameMessageManager()->addMessage(pMessage, 0);
	}
}