#pragma once

#include <allegro5/allegro.h>
#include "Pathfinder.h"
class GridGraph;
class GridVisualizer;
class GraphicsBuffer;
class PathfindingDebugContent;

class GridPathfinder:public Pathfinder
{
public:
	friend class PathfindingDebugContent;

	GridPathfinder( GridGraph* pGraph );
	virtual ~GridPathfinder();

	virtual Path* findPath( Node* pFrom, Node* pTo ) = 0;

	void setShouldDrawVisited(bool setter){ mShouldDrawVisited = setter; };
	bool getShouldDrawVisited(){ return mShouldDrawVisited; };
	void addVisitedNode(Node* node){ mVisitedNodes.push_back(node); };
	
#ifdef VISUALIZE_PATH
	//just for visualization
public:
	void drawVisualization( Grid* pGrid, GraphicsBuffer* pDest );
protected:
	std::vector<Node*> mVisitedNodes;
	Path* mpPath;
	GridVisualizer* mpVisualizer;
	ALLEGRO_COLOR mPathColor;
	ALLEGRO_COLOR mVisitedColor;
	ALLEGRO_COLOR mStartStopColor;
#endif
	float distanceBetweenNodes(Node* pointA, Node* pointB);

	double mTimeElapsed;

private:
	bool mShouldDrawVisited;
};