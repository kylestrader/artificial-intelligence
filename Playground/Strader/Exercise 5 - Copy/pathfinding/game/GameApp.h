#pragma once

/*Game - class to hold all game related info.

Dean Lawson
Champlain College
2011
*/

#include "Game.h"
#include "Vector2D.h"
#include <map>
#include "Node.h"

//forward declarations
class KinematicUnit;
class GridGraph;
class GridPathfinder;
class DebugDisplay;
class HUD;
class Cursor;
class Map;

class GameApp: public Game
{
public:
	GameApp();
	virtual ~GameApp();

	//game loop
	virtual void beginLoop();
	virtual void processLoop();
	virtual bool endLoop();

	//utils
	virtual bool init();
	virtual void cleanup();
	void loadEnviornment(std::string name);
	int switchMap(std::string name);
	int switchMap(int index);
	bool doesMapExist(std::string name);
	bool doesMapExist(Map* _map);
	void handlePortalClick(Vector2D pos);
	void resetDebugDisplay();

	//accessors
	inline Map* getCurMap() { return mpCurMap; };
	Map* getMapByName(std::string mapName) { return mMaps[mapName]; };
	inline int getNumMaps() { return mMaps.size(); };
	inline GridPathfinder* getCurPathfinder() { return mpCurPathfinder; };
	inline GridPathfinder* getGlobalPathfinder(){ return mpGlobalPathfinder; };
	inline GridPathfinder* getPathfinderByMap(std::string mapName) { return mPathfinders[mapName]; };
	inline int getNumPathfinders() { return mPathfinders.size(); };
	inline GridGraph* getCurGridGraph() { return mpCurGridGraph; };
	inline GridGraph* getGridGraphByMap(std::string mapName) { return mGridGraphs[mapName]; };
	inline int getNumGridGraphs(){ return mGridGraphs.size(); };
	inline HUD* getHUD(){ return mpHUD; };
	inline Cursor* getCursor(){ return mpCursor; };
	inline std::map<std::string, Map*> getMaps(){ return mMaps; };

	//setters
	//checks to see if the map is valid
	inline void setCurPathfinder(std::string mapName) { if (doesMapExist(mapName)){ mpCurPathfinder = mPathfinders[mapName]; } };
	inline void setCurGridGraph(std::string mapName) { if (doesMapExist(mapName)){ mpCurGridGraph = mGridGraphs[mapName]; } };
	inline void setCurMap(std::string mapName) { if (doesMapExist(mapName)){ mpCurMap = mMaps[mapName]; } };

	//cleaning
	void cleanupMaps();
	void cleanupPathFinders();
	void cleanupGridGraphs();

private:
	//utils
	void loadMap(std::string name);
	void refreshPortals();
	void setupPortalConnections();
	DebugDisplay* mpDebugDisplay;

	//private variables
	GridPathfinder* mpCurPathfinder;
	GridPathfinder* mpGlobalPathfinder;
	GridGraph* mpCurGridGraph;
	GridGraph* mpGlobalGridGraph;

	Map* mpCurMap;
	HUD* mpHUD;
	Cursor* mpCursor;

	std::map<std::string, Map*> mMaps;
	std::map<std::string, GridPathfinder*> mPathfinders;
	std::map<std::string, GridGraph*> mGridGraphs;
};

