#pragma once

#include "GameMessage.h"
#include "Vector2D.h"
#include "Node.h"

class ToggleDrawVisitedMessage :public GameMessage
{
public:
	ToggleDrawVisitedMessage(Node* fromNode, Node* toNode) 
	:mpFromNode(fromNode)
	,mpToNode(toNode)
	,GameMessage(TOGGLE_DRAW_VISITED_MESSAGE){};

	~ToggleDrawVisitedMessage(){};

	void process();

private:
	Node* mpFromNode;
	Node* mpToNode;
};