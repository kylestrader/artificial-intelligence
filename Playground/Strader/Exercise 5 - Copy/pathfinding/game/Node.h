#pragma once

#include <Trackable.h>

const int BAD_NODE_ID = -1;
#define NODE_ID int

class Node:public Trackable
{
public:
	Node( const NODE_ID& id, std::string mapName );
	Node();
	~Node();

	//setters
	inline void setDistanceFromSource(int dist) { mDistFromSource = dist; };
	inline void setG_Score(int score) { mG_Score = score; };
	inline void setF_Score(int score) { mF_Score = score; };
	inline void setPrevNode(Node* prev) { mpPrevNode = prev; };

	//getters
	const NODE_ID& getId() const {return mId;};
	inline int getDistanceFromSource() { return mDistFromSource; };
	inline int getG_Score() { return mG_Score; };
	inline int getF_Score() { return mF_Score; };
	inline std::string getMapName() { return mMapName; };
	inline Node* getPreviousNode(){ return mpPrevNode; };

private:
	std::string mMapName;
	const NODE_ID mId;
	int mDistFromSource;
	int mG_Score;
	int mF_Score;
	Node* mpPrevNode;
};