#pragma once

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

struct KeyState
{
	bool A_KEY_DOWN;
	bool B_KEY_DOWN;
	bool C_KEY_DOWN;
	bool D_KEY_DOWN;
	bool E_KEY_DOWN;
	bool F_KEY_DOWN;
	bool G_KEY_DOWN;
	bool H_KEY_DOWN;
	bool I_KEY_DOWN;
	bool J_KEY_DOWN;
	bool K_KEY_DOWN;
	bool L_KEY_DOWN;
	bool M_KEY_DOWN;
	bool N_KEY_DOWN;
	bool O_KEY_DOWN;
	bool P_KEY_DOWN;
	bool Q_KEY_DOWN;
	bool R_KEY_DOWN;
	bool S_KEY_DOWN;
	bool T_KEY_DOWN;
	bool U_KEY_DOWN;
	bool V_KEY_DOWN;
	bool W_KEY_DOWN;
	bool X_KEY_DOWN;
	bool Y_KEY_DOWN;
	bool Z_KEY_DOWN;
	bool ZERO_KEY_DOWN;
	bool ONE_KEY_DOWN;
	bool TWO_KEY_DOWN;
	bool THREE_KEY_DOWN;
	bool FOUR_KEY_DOWN;
	bool FIVE_KEY_DOWN;
	bool SIX_KEY_DOWN;
	bool SEVEN_KEY_DOWN;
	bool EIGHT_KEY_DOWN;
	bool NINE_KEY_DOWN;
	bool MINUS_KEY_DOWN;
	bool PLUS_KEY_DOWN;
	bool ESC_KEY_DOWN;
	bool ENTER_KEY_DOWN;
	bool F1_KEY_DOWN;
};