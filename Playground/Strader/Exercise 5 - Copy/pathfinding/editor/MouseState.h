#pragma once

#include "Vector2d.h"

/*UnitManager - a storage unit that manages all existing units in the game.

Kyle Strader
Champlain College
2014
*/

struct MouseState
{
	bool LEFT_DOWN;
	bool MIDDLE_DOWN;
	bool RIGHT_DOWN;
	Vector2D LEFT_LOC;
	Vector2D MIDDLE_LOC;
	Vector2D RIGHT_LOC;
	Vector2D PREV_LEFT;
	Vector2D PREV_MIDDLE;
	Vector2D PREV_RIGHT;
};