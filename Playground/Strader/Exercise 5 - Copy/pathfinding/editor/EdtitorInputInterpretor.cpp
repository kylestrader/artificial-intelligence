#include "EditorInputInterpretor.h"
#include "Editor.h"
#include "HUD.h"
#include "Grid.h"
#include "GridVisualizer.h"
#include "Cursor.h"
#include <fstream>

//For Keyboard
void EditorInputInterpretor::interpretInput(std::string input)
{
	Editor* pEditor = dynamic_cast<Editor*>(gpGame);

	//Alphabet
	if (input == "L")
	{
		if (pEditor != NULL)
		{
			pEditor->loadEnviornment(pEditor->getFileName());
			pEditor->getCursor()->setCurGridSize(pEditor->getMap()->getGrid()->getSquareSize());
			std::cout << "Map(s) loaded!\n";
		}
	}

	else if (input == "S")
	{
		if (pEditor != NULL)
		{
			pEditor->saveMaps();
			std::cout << "Map(s) saved!\n";
		}
	}

	else if (input == "H")
	{
		pEditor->getHUD()->toggleHelp();
	}

	//Alternative
	else if (input == "ESCAPE")
	{
		gpGame->markForExit();
	}

	else if (input == "KEY62") //PLUS
	{
		pEditor->switchNextMap();
	}

	else if (input == "KEY61") //MINUS
	{
		pEditor->switchPrevMap();
	}

	else if (input == "F1")
	{
		pEditor->getHUD()->toggleHelp();
	}
}

//For Mouse
void EditorInputInterpretor::interpretInput(Vector2D mousePos, MouseClick click)
{
	Editor* pEditor = static_cast<Editor*>(gpGame);
	Grid* pGrid = static_cast<Editor*>(gpGame)->getMap()->getGrid();

	Vector2D transposedMousePos = transposeMousePos(mousePos);
	mMousePos = transposedMousePos;
	pEditor->getCursor()->setTilePos(Vector2D(int(mMousePos.getX() / pGrid->getSquareSize()), int(mMousePos.getY() / pGrid->getSquareSize())));

	if (click == LEFT_DOWN)
	{
		if (!mPortalClickedState)
		{
			if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) == CLEAR_VALUE)
				pEditor->addBlock(transposedMousePos);
			else if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) >= PORTAL_VALUE)
			{
				pEditor->handlePortalClick(transposedMousePos);
				mPortalClickedState = true;
			}
		}
	}

	else if (click == LEFT_UP && mPortalClickedState)
		mPortalClickedState = false;

	else if (click == RIGHT_DOWN)
	{
		if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) != CLEAR_VALUE)
			pEditor->clearBlock(transposedMousePos);
	}

	else if (click == MIDDLE_DOWN)
	{
		if (pGrid->getValueAtPixelXY(transposedMousePos.getX(), transposedMousePos.getY()) == CLEAR_VALUE)
		{
			Portal* p = inquireNewPortalData(transposedMousePos);
			if (p != NULL)
				pEditor->addPortal(p);
		}
	}

	else if (click == NO_CLICK)
	{
		pEditor->getMap()->setSelectedTileIndex(
			pGrid->getSquareIndexFromPixelXY(mMousePos.getX(),
			mMousePos.getY()));
	}
}

//The backbone for creating new portals along with new maps
Portal* EditorInputInterpretor::inquireNewPortalData(Vector2D pos)
{
	std::string name;
	std::string linkerID;
	Editor* pEditor = static_cast<Editor*>(gpGame);
	Map* mMap = static_cast<Editor*>(gpGame)->getMap();
	Grid* mGrid = static_cast<Editor*>(gpGame)->getMap()->getGrid();
	int tileIndex = mGrid->getSquareIndexFromPixelXY(pos.getX(), pos.getY());

	//Display header
	clearScreen();
	std::cout << "Creating a new Portal at (id = " + std::to_string(tileIndex) + ")" << std::endl;
	std::cout << "->Type \"-c\" at any time to cancel the process." << std::endl;
	std::cout << "------------------------------------------------" << std::endl;

	//Ask for map to link to
	std::cout << "Linked Map FILE (eg. dungeon.txt): ";
	std::getline (std::cin, name);
	space2underscore(name); //get rid of all spaces and replace them with underscores

	//Cancels the operation
	if (name == "-c")
	{
		clearScreen();
		std::cout << "Returning to editor" << std::endl;
		pEditor->clearBlock(pos);
		return NULL;
	}

	//Ask for the teleporters ID
	std::cout << "Teleporter ID for this map Connection:";
	std::getline(std::cin, linkerID);
	space2underscore(linkerID); //get rid of all spaces and replace them with underscores

	//Cancels the operation
	if (linkerID == "-c")
	{
		clearScreen();
		std::cout << "Returning to editor" << std::endl;
		pEditor->clearBlock(pos);
		return NULL;
	}

	clearScreen();

	//Create a new portal with the specified data
	Portal* portal = new Portal(mMap->getMapName(), name, linkerID, tileIndex);

	//Find number of portals with the same teleporter ID
	if (mMap->checkForIdenticalPortals(portal) >= 1) //if there is more than one portal with the same ID, cancel the operation
	{
		if (mMap->checkNumMapPortalConnections(portal) >= 2)
		{
			clearScreen();
			std::cout << "Portal of this type already exists!" << std::endl;
			std::cout << "Returning to editor" << std::endl;
			pEditor->clearBlock(pos);
			delete portal;
			return NULL;
		}
	}
	return portal;
}

std::string EditorInputInterpretor::space2underscore(std::string text)
{
	for (std::string::iterator it = text.begin(); it != text.end(); ++it) {
		if (*it == ' ') {
			*it = '_';
		}
	}
	return text;
}