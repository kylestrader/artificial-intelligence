#pragma once

#include "InputInterpretor.h"

class EditorInputInterpretor :public InputInterpretor
{
public:
	//constructor/destructor
	EditorInputInterpretor(){};
	~EditorInputInterpretor(){};

	//virtualized functions
	void interpretInput(std::string inputChar);
	void interpretInput(Vector2D mousePos, MouseClick click);

private:
	Portal* inquireNewPortalData(Vector2D pos);
	inline void clearScreen() { system("cls"); };
	std::string space2underscore(std::string text);

	Vector2D mMousePos;
	bool mPortalClickedState;
};